package gtunes.application.gtunes.ui.fragment.videos;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.adapter.VideoAdapter;
import gtunes.application.gtunes.model.DataVideo;
import gtunes.application.gtunes.model.ResponseGetVideo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideosFragment extends Fragment {

    List<DataVideo> listVideoAllData = new ArrayList<>();
    List<DataVideo> listVideoTemp = new ArrayList<>();

    View rootview;
    RecyclerView recyclerView;
    VideoAdapter myAdapter;
    Boolean loadMore = false;

    int jumlahItem = 0;
    int visibleItem = 10;
    int page = 1;
    int jumlahPage = 1;

    FirebaseAnalytics mFirebaseAnalytics;
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootview == null)
        {
            rootview = inflater.inflate(R.layout.fragment_videos, container, false);

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
            Bundle params = new Bundle();
            params.putString("View_Page", "User view list video");
            mFirebaseAnalytics.logEvent("Activity_Video_ViewListVideo", params);

            getVideoData();
            recyclerView = rootview.findViewById(R.id.rv_videos);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            myAdapter = new VideoAdapter(getActivity(),recyclerView,listVideoTemp);
            /*myAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    try {
                        loadMore = true;
                        listVideoTemp.add(null);
                        myAdapter.notifyItemInserted(listVideoTemp.size() - 1);

                        //Load more data for reyclerview
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                //Remove loading item
                                try {
                                    listVideoTemp.remove(listVideoTemp.size() - 1);
                                    myAdapter.notifyItemRemoved(listVideoTemp.size());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                int start = page * visibleItem;
                                int end = page * visibleItem + visibleItem;

                                if(page <= jumlahPage)
                                {
                                    for (int i=start ; i<end ; i++)
                                    {
                                        if(i < jumlahItem)
                                        {
                                            DataVideo dataVideo = new DataVideo( listVideoAllData.get(i).get_video_clip_id(),
                                                    listVideoAllData.get(i).get_video_clip_title(),
                                                    listVideoAllData.get(i).get_video_clip_image(),
                                                    listVideoAllData.get(i).get_video_clip_url(),
                                                    listVideoAllData.get(i).get_video_clip_desc()
                                            );
                                            listVideoTemp.add(dataVideo);
                                        }
                                    }

                                    page = page + 1;
                                }

                                myAdapter.notifyDataSetChanged();
                                myAdapter.setLoaded();
                                loadMore = false;


                            }
                        }, 2000);



                    } catch (Exception e) {

                    }
                }
            });*/
        }

        return rootview;
    }


    public void getVideoData()
    {
        /*if(loadMore == false)
        {*/
            listVideoAllData.clear();
            listVideoTemp.clear();
        /*}*/

        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseGetVideo> userCall = service.getVideo();
        userCall.enqueue(new Callback<ResponseGetVideo>() {
            @Override
            public void onResponse(Call<ResponseGetVideo> call, Response<ResponseGetVideo> response) {
                if(response.isSuccessful())
                {
                    try {
                        listVideoAllData = null;
                        listVideoAllData = response.body().getListVideo();

                        if(listVideoAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            //for (int i = 0; i < visibleItem; i++)
                            for (int i = 0; i < listVideoAllData.size(); i++)
                            {
                                DataVideo dataVideo = new DataVideo( listVideoAllData.get(i).get_video_clip_id(),
                                        listVideoAllData.get(i).get_video_clip_title(),
                                        listVideoAllData.get(i).get_video_clip_image(),
                                        listVideoAllData.get(i).get_video_clip_url(),
                                        listVideoAllData.get(i).get_video_clip_desc()
                                );
                                listVideoTemp.add(dataVideo);
                            }


                            /*if(loadMore == false)
                            {*/
                                jumlahItem = listVideoAllData.size();
                                jumlahPage = listVideoAllData.size() / visibleItem + 1;
                                recyclerView.setAdapter(myAdapter);
                            /*}*/

                            loadMore = false;
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getActivity(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getActivity(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetVideo> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
