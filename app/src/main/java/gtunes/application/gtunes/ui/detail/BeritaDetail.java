package gtunes.application.gtunes.ui.detail;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import gtunes.application.gtunes.MainActivity;
import gtunes.application.gtunes.R;

public class BeritaDetail extends AppCompatActivity implements View.OnClickListener {

    ImageView backButton,shareButton;
    ImageView imageView;
    FontAppCompatTextView t_title,t_date,t_desc;
    Bitmap shareBitmap;
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_berita_detail);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_detail_Berita", "User view detail berita");
        mFirebaseAnalytics.logEvent("Activity_Berita_ViewArtikel", params);

        backButton = findViewById(R.id.back);
        shareButton = findViewById(R.id.share);
        t_title = findViewById(R.id.title);
        t_date = findViewById(R.id.date);
        t_desc = findViewById(R.id.description);
        imageView = findViewById(R.id.image);

        backButton.setOnClickListener(this);
        shareButton.setOnClickListener(this);

        t_title.setText(""+getIntent().getStringExtra("title"));
        t_date.setText(""+getIntent().getStringExtra("date"));
        t_desc.setText(""+getIntent().getStringExtra("desc"));

        Picasso.with(getApplicationContext())
                .load("http://gtunes.co.id/" + getIntent().getStringExtra("image"))
                .noPlaceholder()
                .resize(600, 400)
                .centerCrop()
                .into(imageView);

        Picasso.with(getApplicationContext())
                .load("http://gtunes.co.id/" + getIntent().getStringExtra("image"))
                .noPlaceholder()
                .resize(600, 400)
                .centerCrop()
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        shareBitmap = bitmap;
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back:
                onBackPressed();
                break;

            case R.id.share:
                share();
                break;
        }
    }

    public void share()
    {
        String imgBitmapPath= MediaStore.Images.Media.insertImage(getContentResolver(),shareBitmap,"title",null);
        final Uri imgBitmapUri=Uri.parse(imgBitmapPath);
        Intent shareIntent=new Intent(Intent.ACTION_SEND);
        shareIntent.setType("*/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM,imgBitmapUri);
        shareIntent.putExtra(Intent.EXTRA_TEXT, ""+getIntent().getStringExtra("title"));
        startActivity(Intent.createChooser(shareIntent,"Bagikan Berita"));
    }
}
