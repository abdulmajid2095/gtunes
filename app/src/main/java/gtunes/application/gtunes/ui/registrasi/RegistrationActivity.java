package gtunes.application.gtunes.ui.registrasi;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.vistrav.ask.Ask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import gtunes.application.gtunes.Database.Data_Profile;
import gtunes.application.gtunes.MainActivity;
import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.model.ResponseLogin;
import gtunes.application.gtunes.model.SendLogin;
import gtunes.application.gtunes.model.SendRegister;
import gtunes.application.gtunes.ui.dashboard.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener{

    MaterialEditText editTextNama,editTextEmail,editTextHandphone,editTextPassword,editTextPasswordRetype;
    LinearLayout register;
    DialogPlus dialog,warningDialog;
    ImageView back;
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.register_activity);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_Page", "User view register page");
        mFirebaseAnalytics.logEvent("Activity_Register_ViewPage", params);

        editTextNama = findViewById(R.id.edit_nama);
        editTextEmail = findViewById(R.id.edit_email);
        editTextHandphone = findViewById(R.id.edit_handphone);
        editTextPassword = findViewById(R.id.edit_password);
        editTextPasswordRetype = findViewById(R.id.edit_retype);
        register = findViewById(R.id.register);
        register.setOnClickListener(this);

        back = findViewById(R.id.back);
        back.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.register:
                Bundle params = new Bundle();
                params.putString("Register", "User press register button");
                mFirebaseAnalytics.logEvent("Activity_Register_PressRegister", params);

                cekForm(""+editTextNama.getText().toString(),
                        ""+editTextEmail.getText().toString(),
                        ""+editTextHandphone.getText().toString(),
                        ""+editTextPassword.getText().toString(),
                        ""+editTextPasswordRetype.getText().toString());
            break;

            case R.id.back:
                Bundle params2 = new Bundle();
                params2.putString("Login", "User go to login page");
                mFirebaseAnalytics.logEvent("Activity_Register_BackToLogin", params2);
                onBackPressed();
                break;
        }
    }

    public void cekForm(String nama, String email, String handphone, String password, String retypePassword)
    {
        Boolean namaKosong = true, emailKosong = true, handphoneKosong = true, passKosong = true, retypeTidakSama = true;
        Boolean lebihDari6Karakter = false;

        //Pengecekan Form Nama
        if(nama.equals("") || nama == null)
        {
            namaKosong = true;
            editTextNama.setError("Nama tidak boleh kosong");
        }
        else
        {
            namaKosong = false;
            editTextNama.setError(null);
        }

        //Pengecekan Form Email
        if(email.equals("") || email == null)
        {
            emailKosong = true;
            editTextEmail.setError("Email tidak boleh kosong");
        }
        else
        {
            emailKosong = false;
            editTextEmail.setError(null);
        }

        //Pengecekan Form Telephone
        if(handphone.equals("") || handphone == null)
        {
            handphoneKosong = true;
            editTextHandphone.setError("Nomor Telepon tidak boleh kosong");
        }
        else
        {
            handphoneKosong = false;
            editTextHandphone.setError(null);
        }

        //Pengecekan Form Password
        if(password.equals("") || password == null)
        {
            passKosong = true;
            editTextPassword.setError("Password tidak boleh kosong");
        }
        else
        {
            passKosong = false;
            if(password.length() < 6)
            {
                editTextPassword.setError("Minimal pasword 6 karakter");
                lebihDari6Karakter = false;
            }
            else
            {
                editTextPassword.setError(null);
                lebihDari6Karakter = true;
            }
        }

        //Pengecekan Form Retype Password
        if(retypePassword.equals("") || retypePassword == null)
        {
            retypeTidakSama = true;
            editTextPasswordRetype.setError("Password tidak sama");
        }
        else
        {
            if(password.equals(retypePassword))
            {
                retypeTidakSama = false;
                editTextPasswordRetype.setError(null);
            }
            else
            {
                retypeTidakSama = true;
                editTextPasswordRetype.setError("Password tidak sama");
            }
        }

        if(namaKosong == false && emailKosong == false && handphoneKosong == false && passKosong == false && retypeTidakSama == false && lebihDari6Karakter == true)
        {
            registerUser(nama,email,handphone,password);
        }

    }

    public void registerUser(String nama, String email, String handphone, String password)
    {
        Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
        data_profile.loginUser = ""+email;
        data_profile.loginPassword = ""+password;
        data_profile.save();

        dialog_loading();
        SendRegister sendRegister = new SendRegister(""+email,""+password,""+nama,""+handphone);
        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseLogin> userCall = service.register(sendRegister);
        userCall.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if(response.isSuccessful()){
                    Bundle params = new Bundle();
                    params.putString("Register", "User success to register");
                    mFirebaseAnalytics.logEvent("Activity_Register_Success", params);
                    dialog.dismiss();
                    Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
                    data_profile.token = ""+response.body().get_dataData().get_token();
                    data_profile.user_id = ""+response.body().get_dataData().get_dataUser().get_user_id();
                    data_profile.phone = ""+response.body().get_dataData().get_dataUser().get_phone();
                    data_profile.telco = ""+response.body().get_dataData().get_dataUser().get_telco();
                    data_profile.email = ""+response.body().get_dataData().get_dataUser().get_email();
                    data_profile.active_until = ""+response.body().get_dataData().get_dataUser().get_active_until();
                    data_profile.paket_status = ""+response.body().get_dataData().get_dataUser().get_paket_status();
                    data_profile.login = true;
                    data_profile.nama = ""+nama;
                    data_profile.avatar = "default";
                    data_profile.save();
                    Toast.makeText(getApplicationContext(),"Register Berhasil",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(),DashboardActivity.class));
                    finish();
                } else{
                    dialog.dismiss();
                    JSONObject jObjError = null;
                    String message = "Gagal! Email atau nomor telepon sudah terdaftar.";
                    /*try {
                        jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjError2 = null;
                        jObjError2 = new JSONObject(jObjError.getString("error"));
                        message = ""+jObjError2.getString("userMsg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                    final String myMessage = message;
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            dialog_warning(""+myMessage);
                        }
                    };
                    handler.postDelayed(myRunnable,500);
                }

            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                dialog.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        // do something
                        dialog_warning("Terjadi kesalahan. Cek koneksi internet Anda.");
                    }
                };
                handler.postDelayed(myRunnable,500);

            }
        });
    }

    public void dialog_loading()
    {
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_loading))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = dialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        textMessage.setText("Memuat...");
        dialog.show();

    }

    public void dialog_warning(String message)
    {
        Bundle params = new Bundle();
        params.putString("Register", "User failed to register");
        mFirebaseAnalytics.logEvent("Activity_Register_Failed", params);

        warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();

    }
}
