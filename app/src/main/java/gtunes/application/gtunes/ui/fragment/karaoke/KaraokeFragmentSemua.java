package gtunes.application.gtunes.ui.fragment.karaoke;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.adapter.LaguAdapter;
import gtunes.application.gtunes.adapter.PaketAdapter;
import gtunes.application.gtunes.model.DataLagu;
import gtunes.application.gtunes.model.Lagu;
import gtunes.application.gtunes.model.Paket;
import gtunes.application.gtunes.model.ResponseGetLaguGenre;
import gtunes.application.gtunes.model.ResponseGetLaguGenre2;
import gtunes.application.gtunes.model.ResponseGetLaguKaraoke;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KaraokeFragmentSemua extends Fragment {

    List<DataLagu> listLaguAllData = new ArrayList<>();
    List<DataLagu> listLaguTemp = new ArrayList<>();

    RecyclerView recyclerView;
    LaguAdapter myAdapter;
    Boolean loadMore = false;

    int jumlahItem = 0;
    int visibleItem = 10;
    int page = 1;
    int jumlahPage = 1;

    BroadcastReceiver updateUIReciver;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lagu, container, false);

       /* recyclerViewLagu = (RecyclerView)v.findViewById(R.id.rv_lagu_list);
        laguAdapter = new LaguAdapter(laguList, getActivity());
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewLagu.setLayoutManager(horizontalLayoutManager);
        recyclerViewLagu.setAdapter(laguAdapter);
        populatepaketList();*/

        getAllLagu("http://api.gtunes.co.id/song/all?page=1&customer_code=");
        recyclerView = v.findViewById(R.id.rv_lagu_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myAdapter = new LaguAdapter(getActivity(),recyclerView,listLaguTemp);
        /*myAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                try {
                    loadMore = true;
                    listLaguTemp.add(null);
                    myAdapter.notifyItemInserted(listLaguTemp.size() - 1);

                    //Load more data for reyclerview
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            //Remove loading item
                            try {
                                listLaguTemp.remove(listLaguTemp.size() - 1);
                                myAdapter.notifyItemRemoved(listLaguTemp.size());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            int start = page * visibleItem;
                            int end = page * visibleItem + visibleItem;

                            if(page <= jumlahPage)
                            {
                                for (int i=start ; i<end ; i++)
                                {
                                    if(i < jumlahItem)
                                    {
                                        DataLagu dataLagu = new DataLagu( listLaguAllData.get(i).get_music_id(),
                                                listLaguAllData.get(i).get_music_title(),
                                                listLaguAllData.get(i).get_music_artist(),
                                                listLaguAllData.get(i).get_music_url(),
                                                listLaguAllData.get(i).get_music_artwork(),
                                                listLaguAllData.get(i).get_artist_name(),
                                                listLaguAllData.get(i).get_artist_id()
                                        );
                                        listLaguTemp.add(dataLagu);
                                    }
                                }

                                page = page + 1;
                            }

                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                            loadMore = false;


                        }
                    }, 2000);



                } catch (Exception e) {

                }
            }
        });*/



        //Receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction("service.to.activity.transfer");
        updateUIReciver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null)
                {
                    String typeBroadcast = ""+intent.getStringExtra("type").toString();
                    String textSearch = ""+intent.getStringExtra("search").toString();
                    //Toast.makeText(getActivity(),""+typeBroadcast,Toast.LENGTH_SHORT).show();
                    if(typeBroadcast.equals("all"))
                    {
                        //Toast.makeText(getActivity(),""+textSearch,Toast.LENGTH_SHORT).show();
                        getAllLagu("http://api.gtunes.co.id/song/search?type=all&word="+textSearch);
                    }
                }
            }
        };
        getActivity().registerReceiver(updateUIReciver, filter);


        return v;
    }

    public void getAllLagu(String url)
    {
        if(loadMore == false)
        {
            listLaguAllData.clear();
            listLaguTemp.clear();
        }

        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseGetLaguKaraoke> userCall = service.getSemuaLagu(url);
        userCall.enqueue(new Callback<ResponseGetLaguKaraoke>() {
            @Override
            public void onResponse(Call<ResponseGetLaguKaraoke> call, Response<ResponseGetLaguKaraoke> response) {
                if(response.isSuccessful())
                {
                    try {
                        listLaguAllData = null;
                        listLaguAllData = response.body().get_data();
                        //Toast.makeText(getApplicationContext(),""+listLaguAllData.get(0).get_artist_name().toString(),Toast.LENGTH_SHORT).show();
                        if(listLaguAllData.size()==0)
                        {
                            Toast.makeText(getActivity(), "Hasil tidak ditemukan", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            for (int i = 0; i < listLaguAllData.size(); i++)
                            {
                                DataLagu dataLagu = new DataLagu( listLaguAllData.get(i).get_music_id(),
                                        listLaguAllData.get(i).get_music_title(),
                                        listLaguAllData.get(i).get_music_artist(),
                                        listLaguAllData.get(i).get_music_url(),
                                        listLaguAllData.get(i).get_music_artwork(),
                                        listLaguAllData.get(i).get_artist_name(),
                                        listLaguAllData.get(i).get_artist_id(),
                                        listLaguAllData.get(i).get_music_singsong_url(),
                                        listLaguAllData.get(i).get_music_lyric(),
                                        listLaguAllData.get(i).get_music_free(),
                                        listLaguAllData.get(i).get_music_karaoke_available(),
                                        "no"
                                );
                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                                listLaguTemp.add(dataLagu);
                            }


                            if(loadMore == false)
                            {
                                jumlahItem = listLaguAllData.size();
                                jumlahPage = listLaguAllData.size() / visibleItem + 1;
                                recyclerView.setAdapter(myAdapter);
                            }

                            loadMore = false;
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getApplicationContext(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetLaguKaraoke> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }

}
