package gtunes.application.gtunes.ui.detail;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pixplicity.fontview.FontAppCompatTextView;

import gtunes.application.gtunes.R;

public class PaketWebview extends AppCompatActivity {

    WebView myWebView;
    LinearLayout lay_progress;
    ImageView back;

    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_paket_webview);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("Purchase", "User purchase with telcos");
        mFirebaseAnalytics.logEvent("Activity_Paket_TelcosPurchase", params);

        lay_progress = (LinearLayout) findViewById(R.id.lay_progress);
        Intent getdata = getIntent();
        String url = ""+getdata.getStringExtra("url");
        String title = ""+getdata.getStringExtra("title");

        FontAppCompatTextView myTitle = findViewById(R.id.title);
        myTitle.setText(""+title);

        myWebView = (WebView) findViewById(R.id.webView);
        myWebView.loadUrl(url);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        myWebView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                lay_progress.setVisibility(View.GONE);
                myWebView.setVisibility(View.VISIBLE);
            }
        });

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
