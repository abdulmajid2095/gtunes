package gtunes.application.gtunes.ui.splash;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.orm.SugarContext;

import gtunes.application.gtunes.Database.Data_Profile;
import gtunes.application.gtunes.MainActivity;
import gtunes.application.gtunes.R;
import gtunes.application.gtunes.ui.dashboard.DashboardActivity;
import gtunes.application.gtunes.ui.detail.BeritaDetail;
import gtunes.application.gtunes.ui.detail.MusicDetail;
import gtunes.application.gtunes.ui.detail.PaketWebview;
import gtunes.application.gtunes.ui.detail.StartKaraoke;
import gtunes.application.gtunes.ui.detail.VideosDetail;
import io.fabric.sdk.android.Fabric;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    FirebaseAnalytics mFirebaseAnalytics;
    Boolean getNotifor = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.splash_screen);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("Open_App", "User open app");
        mFirebaseAnalytics.logEvent("Activity_App_UserOpenApp", params);

        FirebaseMessaging.getInstance().subscribeToTopic("all")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //String msg = getString(R.string.msg_subscribed);
                        if (task.isSuccessful()) {
                            //msg = getString(R.string.msg_subscribe_failed);
                            //Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            //Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_SHORT).show();
                        }
                        //Log.d(TAG, msg);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        SugarContext.init(this);
        Data_Profile data = Data_Profile.findById(Data_Profile.class,1L);
        if((int)data.count(Data_Profile.class, "", null) == 0) {
            Data_Profile data2 = new Data_Profile("","","","","","","",false,"","default","","");
            data2.save();
        }

        if(getIntent().getExtras() !=null)
        {
            check_notif_type();
        }
        else
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashScreen.this, DashboardActivity.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }



    private void check_notif_type()
    {
        for(String notif : getIntent().getExtras().keySet())
        {
            if(notif.equals("notiFor"))
            {
                getNotifor = true;
                if(getIntent().getExtras().getString(notif).equals("news"))
                {
                    getNews();
                }
                else if(getIntent().getExtras().getString(notif).equals("newSong"))
                {
                    getSong();
                }
                else if(getIntent().getExtras().getString(notif).equals("newVideo"))
                {
                    getVideo();
                }
                else if(getIntent().getExtras().getString(notif).equals("newKaraoke"))
                {
                    getKaraoke();
                }
                else if(getIntent().getExtras().getString(notif).equals("webView"))
                {
                    getWebview();
                }
                else if(getIntent().getExtras().getString(notif).equals("general"))
                {
                    Intent intent = new Intent(this,DashboardActivity.class);
                    startActivity(intent);
                }
                else if(getIntent().getExtras().getString(notif).equals("updateApp"))
                {
                    Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.gtunes.gtunes");
                    Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                    likeIng.setPackage("com.android.vending");
                    likeIng.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(likeIng);
                }
                else
                {
                    Intent intent = new Intent(this,DashboardActivity.class);
                    startActivity(intent);
                }

            }

        }

        if(getNotifor == false)
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashScreen.this, DashboardActivity.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }


    public void getNews()
    {
        Intent intent = new Intent(this,BeritaDetail.class);
        for(String notif : getIntent().getExtras().keySet())
        {
            if(notif.equals("title"))
            {
                intent.putExtra("title", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("date"))
            {
                intent.putExtra("date", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("desc"))
            {
                intent.putExtra("desc", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("image"))
            {
                intent.putExtra("image", "" + getIntent().getExtras().getString(notif));
            }
        }
        startActivity(intent);
    }

    public void getVideo()
    {
        Intent intent = new Intent(this,VideosDetail.class);
        for(String notif : getIntent().getExtras().keySet())
        {
            if(notif.equals("title"))
            {
                intent.putExtra("title", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("url"))
            {
                intent.putExtra("url", "" + getIntent().getExtras().getString(notif));
            }
        }
        startActivity(intent);
    }

    public void getSong()
    {
        Intent intent = new Intent(this,MusicDetail.class);
        for(String notif : getIntent().getExtras().keySet())
        {
            if(notif.equals("musicId"))
            {
                intent.putExtra("musicId", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("title"))
            {
                intent.putExtra("title", "" + getIntent().getExtras().getString(notif));
            }
        }
        startActivity(intent);
    }

    public void getKaraoke()
    {
        Intent intent = new Intent(this,StartKaraoke.class);
        for(String notif : getIntent().getExtras().keySet())
        {
            if(notif.equals("tryme"))
            {
                intent.putExtra("tryme", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("musicId"))
            {
                intent.putExtra("musicId", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("title"))
            {
                intent.putExtra("title", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("band"))
            {
                intent.putExtra("band", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("url_karaoke"))
            {
                intent.putExtra("url_karaoke", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("gambar"))
            {
                intent.putExtra("gambar", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("lirik"))
            {
                intent.putExtra("lirik", "" + getIntent().getExtras().getString(notif));
            }
        }
        startActivity(intent);
    }

    public void getWebview()
    {
        Intent intent = new Intent(this,PaketWebview.class);
        for(String notif : getIntent().getExtras().keySet())
        {
            if(notif.equals("title"))
            {
                intent.putExtra("title", "" + getIntent().getExtras().getString(notif));
            }
            else if(notif.equals("url"))
            {
                intent.putExtra("url", "" + getIntent().getExtras().getString(notif));
            }
        }
        startActivity(intent);
    }
}
