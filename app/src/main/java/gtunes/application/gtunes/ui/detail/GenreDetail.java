package gtunes.application.gtunes.ui.detail;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.adapter.GenreAdapter;
import gtunes.application.gtunes.adapter.LaguAdapter;
import gtunes.application.gtunes.model.DataGenre;
import gtunes.application.gtunes.model.DataLagu;
import gtunes.application.gtunes.model.Lagu;
import gtunes.application.gtunes.model.ResponseGetLaguGenre;
import gtunes.application.gtunes.model.ResponseGetLaguGenre2;
import gtunes.application.gtunes.model.SearchGenre;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreDetail extends AppCompatActivity implements View.OnClickListener {

    FontAppCompatTextView judulGenre;
    ImageView back;
    String genreId = "";

    List<DataLagu> listLaguAllData = new ArrayList<>();
    List<DataLagu> listLaguTemp = new ArrayList<>();

    RecyclerView recyclerView;
    LaguAdapter myAdapter;
    Boolean loadMore = false;

    int jumlahItem = 0;
    int visibleItem = 10;
    int page = 1;
    int jumlahPage = 1;

    ImageView search;
    MaterialEditText editSearch;

    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_detail_genre);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_Page", "User view list song of genre");
        mFirebaseAnalytics.logEvent("Activity_Genre_ViewListSong", params);

        judulGenre = findViewById(R.id.judulGenre);
        judulGenre.setText(""+getIntent().getStringExtra("genre"));
        genreId = ""+getIntent().getStringExtra("id");
        back = findViewById(R.id.back);
        back.setOnClickListener(this);

        getGenreLagu("http://api.gtunes.co.id/genre/"+genreId);
        recyclerView = findViewById(R.id.rv_lagu_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        myAdapter = new LaguAdapter(getApplicationContext(),recyclerView,listLaguTemp);
        /*myAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                try {
                    loadMore = true;
                    listLaguTemp.add(null);
                    myAdapter.notifyItemInserted(listLaguTemp.size() - 1);

                    //Load more data for reyclerview
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            //Remove loading item
                            try {
                                listLaguTemp.remove(listLaguTemp.size() - 1);
                                myAdapter.notifyItemRemoved(listLaguTemp.size());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            int start = page * visibleItem;
                            int end = page * visibleItem + visibleItem;

                            if(page <= jumlahPage)
                            {
                                for (int i=start ; i<end ; i++)
                                {
                                    if(i < jumlahItem)
                                    {
                                        DataLagu dataLagu = new DataLagu( listLaguAllData.get(i).get_music_id(),
                                                listLaguAllData.get(i).get_music_title(),
                                                listLaguAllData.get(i).get_music_artist(),
                                                "http://gtunes.co.id/"+listLaguAllData.get(i).get_music_url(),
                                                "http://gtunes.co.id/"+listLaguAllData.get(i).get_music_artwork(),
                                                listLaguAllData.get(i).get_artist_name(),
                                                listLaguAllData.get(i).get_artist_id(),
                                                "-","-","1","0",
                                                "no"
                                        );
                                        listLaguTemp.add(dataLagu);
                                    }
                                }

                                page = page + 1;
                            }

                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                            loadMore = false;


                        }
                    }, 2000);



                } catch (Exception e) {

                }
            }
        });*/


        //Search
        editSearch = findViewById(R.id.editSearch);
        editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = ""+editSearch.getText().toString();
                    searchGenreLagu("http://api.gtunes.co.id/genre/search/"+genreId,search);
                    Bundle params = new Bundle();
                    params.putString("Search_Song", "User search song of genre");
                    mFirebaseAnalytics.logEvent("Activity_Genre_SearchSong", params);
                    return true;
                }
                return false;
            }
        });
        search = findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String search = ""+editSearch.getText().toString();
                searchGenreLagu("http://api.gtunes.co.id/genre/search/"+genreId,search);
                Bundle params = new Bundle();
                params.putString("Search_Song", "User search song of genre");
                mFirebaseAnalytics.logEvent("Activity_Genre_SearchSong", params);
            }
        });



    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back:
                onBackPressed();
                break;
        }
    }


    public void getGenreLagu(String url)
    {
        if(loadMore == false)
        {
            listLaguAllData.clear();
            listLaguTemp.clear();
        }

        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseGetLaguGenre> userCall = service. getGenreDetail(url);
        userCall.enqueue(new Callback<ResponseGetLaguGenre>() {
            @Override
            public void onResponse(Call<ResponseGetLaguGenre> call, Response<ResponseGetLaguGenre> response) {
                if(response.isSuccessful())
                {
                    try {
                        ResponseGetLaguGenre2 responseGetLaguGenre2 = response.body().get_data();
                        //Toast.makeText(getApplicationContext(),""+response.body().get_data().toString(),Toast.LENGTH_SHORT).show();
                        listLaguAllData = null;
                        listLaguAllData = responseGetLaguGenre2.getListSong();
                        //Toast.makeText(getApplicationContext(),""+listLaguAllData.get(0).get_artist_name().toString(),Toast.LENGTH_SHORT).show();
                        if(listLaguAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            for (int i = 0; i < listLaguAllData.size(); i++)
                            {
                                DataLagu dataLagu = new DataLagu( listLaguAllData.get(i).get_music_id(),
                                        listLaguAllData.get(i).get_music_title(),
                                        listLaguAllData.get(i).get_music_artist(),
                                        "http://gtunes.co.id/"+listLaguAllData.get(i).get_music_url(),
                                        "http://gtunes.co.id/"+listLaguAllData.get(i).get_music_artwork(),
                                        listLaguAllData.get(i).get_artist_name(),
                                        listLaguAllData.get(i).get_artist_id(),"-","-","1","0",
                                        "no"
                                );
                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                                listLaguTemp.add(dataLagu);
                            }


                            if(loadMore == false)
                            {
                                jumlahItem = listLaguAllData.size();
                                jumlahPage = listLaguAllData.size() / visibleItem + 1;
                                recyclerView.setAdapter(myAdapter);
                            }

                            loadMore = false;
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getApplicationContext(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetLaguGenre> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void searchGenreLagu(String url,String judul)
    {
        if(loadMore == false)
        {
            listLaguAllData.clear();
            listLaguTemp.clear();
        }

        SearchGenre searchGenre = new SearchGenre(""+judul);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseGetLaguGenre> userCall = service. searchGenreDetail(url,searchGenre);
        userCall.enqueue(new Callback<ResponseGetLaguGenre>() {
            @Override
            public void onResponse(Call<ResponseGetLaguGenre> call, Response<ResponseGetLaguGenre> response) {
                if(response.isSuccessful())
                {
                    try {
                        ResponseGetLaguGenre2 responseGetLaguGenre2 = response.body().get_data();
                        //Toast.makeText(getApplicationContext(),""+response.body().get_data().toString(),Toast.LENGTH_SHORT).show();
                        listLaguAllData = null;
                        listLaguAllData = responseGetLaguGenre2.getListSong();
                        //Toast.makeText(getApplicationContext(),""+listLaguAllData.get(0).get_artist_name().toString(),Toast.LENGTH_SHORT).show();
                        if(listLaguAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            for (int i = 0; i < listLaguAllData.size(); i++)
                            {
                                DataLagu dataLagu = new DataLagu( listLaguAllData.get(i).get_music_id(),
                                        listLaguAllData.get(i).get_music_title(),
                                        listLaguAllData.get(i).get_music_artist(),
                                        "http://gtunes.co.id/"+listLaguAllData.get(i).get_music_url(),
                                        "http://gtunes.co.id/"+listLaguAllData.get(i).get_music_artwork(),
                                        listLaguAllData.get(i).get_artist_name(),
                                        listLaguAllData.get(i).get_artist_id(),"-","-","1","0",
                                        "no"
                                );
                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                                listLaguTemp.add(dataLagu);
                            }


                            if(loadMore == false)
                            {
                                jumlahItem = listLaguAllData.size();
                                jumlahPage = listLaguAllData.size() / visibleItem + 1;
                                recyclerView.setAdapter(myAdapter);
                            }

                            loadMore = false;
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getApplicationContext(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetLaguGenre> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
