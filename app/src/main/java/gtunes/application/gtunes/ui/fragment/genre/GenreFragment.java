package gtunes.application.gtunes.ui.fragment.genre;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.adapter.ArtisAdapter;
import gtunes.application.gtunes.adapter.GenreAdapter;
import gtunes.application.gtunes.model.Artis;
import gtunes.application.gtunes.model.DataGenre;
import gtunes.application.gtunes.model.Genre;
import gtunes.application.gtunes.model.ResponseGetGenre;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreFragment extends Fragment {

    List<DataGenre> listGenreAllData = new ArrayList<>();
    List<DataGenre> listGenreTemp = new ArrayList<>();

    View rootview;
    RecyclerView recyclerView;
    GenreAdapter myAdapter;
    Boolean loadMore = false;

    int jumlahItem = 0;
    int visibleItem = 10;
    int page = 1;
    int jumlahPage = 1;

    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootview == null)
        {
            rootview = inflater.inflate(R.layout.fragment_artis, container, false);

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
            Bundle params = new Bundle();
            params.putString("View_Page", "User view genre page");
            mFirebaseAnalytics.logEvent("Activity_Genre_ViewListGenre", params);

            getGenreData();
            recyclerView = rootview.findViewById(R.id.rv_artis);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),2);
            recyclerView.setLayoutManager(gridLayoutManager);
            myAdapter = new GenreAdapter(getActivity(),recyclerView,listGenreTemp);
            /*myAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    try {
                        loadMore = true;
                        listGenreTemp.add(null);
                        myAdapter.notifyItemInserted(listGenreTemp.size() - 1);

                        //Load more data for reyclerview
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                //Remove loading item
                                try {
                                    listGenreTemp.remove(listGenreTemp.size() - 1);
                                    myAdapter.notifyItemRemoved(listGenreTemp.size());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                int start = page * visibleItem;
                                int end = page * visibleItem + visibleItem;

                                if(page <= jumlahPage)
                                {
                                    for (int i=start ; i<end ; i++)
                                    {
                                        if(i < jumlahItem)
                                        {
                                            DataGenre dataGenre = new DataGenre( listGenreAllData.get(i).get_genre_id(),
                                                    listGenreAllData.get(i).get_genre_name(),
                                                    listGenreAllData.get(i).get_genre_image()
                                            );
                                            listGenreTemp.add(dataGenre);
                                        }
                                    }

                                    page = page + 1;
                                }

                                myAdapter.notifyDataSetChanged();
                                myAdapter.setLoaded();
                                loadMore = false;


                            }
                        }, 2000);



                    } catch (Exception e) {

                    }
                }
            });*/
        }


        return rootview;
    }

    public void getGenreData()
    {
        /*if(loadMore == false)
        {*/
            listGenreAllData.clear();
            listGenreTemp.clear();
        /*}*/

        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseGetGenre> userCall = service.getGenre();
        userCall.enqueue(new Callback<ResponseGetGenre>() {
            @Override
            public void onResponse(Call<ResponseGetGenre> call, Response<ResponseGetGenre> response) {
                if(response.isSuccessful())
                {
                    try {
                        listGenreAllData = null;
                        listGenreAllData = response.body().getListGenre();

                        if(listGenreAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            //for (int i = 0; i < visibleItem; i++)
                            for (int i = 0; i < listGenreAllData.size(); i++)
                            {
                                DataGenre dataGenre = new DataGenre( listGenreAllData.get(i).get_genre_id(),
                                        listGenreAllData.get(i).get_genre_name(),
                                        listGenreAllData.get(i).get_genre_image()
                                );
                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                                listGenreTemp.add(dataGenre);
                            }


                            /*if(loadMore == false)
                            {*/
                                jumlahItem = listGenreAllData.size();
                                jumlahPage = listGenreAllData.size() / visibleItem + 1;
                                recyclerView.setAdapter(myAdapter);
                            /*}*/

                            loadMore = false;
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getActivity(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getActivity(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetGenre> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
