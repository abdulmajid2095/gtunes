package gtunes.application.gtunes.ui.fragment.tryme;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.adapter.LaguAdapter;
import gtunes.application.gtunes.adapter.PaketAdapter;
import gtunes.application.gtunes.model.DataLagu;
import gtunes.application.gtunes.model.Lagu;
import gtunes.application.gtunes.model.Paket;
import gtunes.application.gtunes.model.ResponseGetLaguGenre;
import gtunes.application.gtunes.model.ResponseGetLaguGenre2;
import gtunes.application.gtunes.model.ResponseGetLaguKaraoke;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TryMeFragment extends Fragment {

    List<DataLagu> listLaguAllData = new ArrayList<>();
    List<DataLagu> listLaguTemp = new ArrayList<>();

    RecyclerView recyclerView;
    LaguAdapter myAdapter;
    Boolean loadMore = false;

    int jumlahItem = 0;
    int visibleItem = 10;
    int page = 1;
    int jumlahPage = 1;

    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_try_me, container, false);

       /* recyclerViewLagu = (RecyclerView)v.findViewById(R.id.rv_lagu_list);
        laguAdapter = new LaguAdapter(laguList, getActivity());
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewLagu.setLayoutManager(horizontalLayoutManager);
        recyclerViewLagu.setAdapter(laguAdapter);
        populatepaketList();*/

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle params = new Bundle();
        params.putString("View_Page", "User view TryMe page");
        mFirebaseAnalytics.logEvent("Activity_TryMe_ViewListSong", params);

        getAllLagu();
        recyclerView = v.findViewById(R.id.rv_try_me);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myAdapter = new LaguAdapter(getActivity(),recyclerView,listLaguTemp);
        /*myAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                try {
                    loadMore = true;
                    listLaguTemp.add(null);
                    myAdapter.notifyItemInserted(listLaguTemp.size() - 1);

                    //Load more data for reyclerview
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            //Remove loading item
                            try {
                                listLaguTemp.remove(listLaguTemp.size() - 1);
                                myAdapter.notifyItemRemoved(listLaguTemp.size());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            int start = page * visibleItem;
                            int end = page * visibleItem + visibleItem;

                            if(page <= jumlahPage)
                            {
                                for (int i=start ; i<end ; i++)
                                {
                                    if(i < jumlahItem)
                                    {
                                        DataLagu dataLagu = new DataLagu( listLaguAllData.get(i).get_music_id(),
                                                listLaguAllData.get(i).get_music_title(),
                                                listLaguAllData.get(i).get_music_artist(),
                                                listLaguAllData.get(i).get_music_url(),
                                                listLaguAllData.get(i).get_music_artwork(),
                                                listLaguAllData.get(i).get_artist_name(),
                                                listLaguAllData.get(i).get_artist_id()
                                        );
                                        listLaguTemp.add(dataLagu);
                                    }
                                }

                                page = page + 1;
                            }

                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                            loadMore = false;


                        }
                    }, 2000);



                } catch (Exception e) {

                }
            }
        });*/

        return v;
    }

    public void getAllLagu()
    {
        if(loadMore == false)
        {
            listLaguAllData.clear();
            listLaguTemp.clear();
        }

        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseGetLaguKaraoke> userCall = service.getTryMe();
        userCall.enqueue(new Callback<ResponseGetLaguKaraoke>() {
            @Override
            public void onResponse(Call<ResponseGetLaguKaraoke> call, Response<ResponseGetLaguKaraoke> response) {
                if(response.isSuccessful())
                {
                    try {
                        listLaguAllData = null;
                        listLaguAllData = response.body().get_data();
                        //Toast.makeText(getApplicationContext(),""+listLaguAllData.get(0).get_artist_name().toString(),Toast.LENGTH_SHORT).show();
                        if(listLaguAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            for (int i = 0; i < listLaguAllData.size(); i++)
                            {
                                DataLagu dataLagu = new DataLagu( listLaguAllData.get(i).get_music_id(),
                                        listLaguAllData.get(i).get_music_title(),
                                        listLaguAllData.get(i).get_music_artist(),
                                        listLaguAllData.get(i).get_music_url(),
                                        listLaguAllData.get(i).get_music_artwork(),
                                        listLaguAllData.get(i).get_artist_name(),
                                        listLaguAllData.get(i).get_artist_id(),
                                        listLaguAllData.get(i).get_music_singsong_url(),
                                        listLaguAllData.get(i).get_music_lyric(),
                                        listLaguAllData.get(i).get_music_free(),
                                        listLaguAllData.get(i).get_music_karaoke_available(),
                                        "yes"
                                );
                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                                listLaguTemp.add(dataLagu);
                            }


                            if(loadMore == false)
                            {
                                jumlahItem = listLaguAllData.size();
                                jumlahPage = listLaguAllData.size() / visibleItem + 1;
                                recyclerView.setAdapter(myAdapter);
                            }

                            loadMore = false;
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getApplicationContext(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetLaguKaraoke> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
