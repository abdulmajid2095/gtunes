package gtunes.application.gtunes.ui.detail;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Formatter;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import gtunes.application.gtunes.R;

public class MusicDetail extends AppCompatActivity implements View.OnClickListener{

    Boolean autoplay = false;
    private SimpleExoPlayer exoPlayer;
    private  ExoPlayer.EventListener eventListener = new ExoPlayer.EventListener() {
        @Override
        public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
            Log.i(TAG,"onTimelineChanged");
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            Log.i(TAG,"onTracksChanged");
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
            Log.i(TAG,"onLoadingChanged");
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG,"onPlayerStateChanged: playWhenReady = "+String.valueOf(playWhenReady)
                    +" playbackState = "+playbackState);
            switch (playbackState){
                case ExoPlayer.STATE_ENDED:
                    Log.i(TAG,"Playback ended!");
                    //Stop playback and return to start position
                    setPlayPause(false);
                    exoPlayer.seekTo(0);
                    break;
                case ExoPlayer.STATE_READY:
                    Log.i(TAG,"ExoPlayer ready! pos: "+exoPlayer.getCurrentPosition()
                            +" max: "+stringForTime((int)exoPlayer.getDuration()));
                    setProgress();
                    try {
                        layLoading.setVisibility(View.GONE);
                        if(autoplay == false)
                        {
                            setPlayPause(true);
                            autoplay = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case ExoPlayer.STATE_BUFFERING:
                    Log.i(TAG,"Playback buffering!");
                    try {
                        layLoading.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case ExoPlayer.STATE_IDLE:
                    Log.i(TAG,"ExoPlayer idle!");
                    break;
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            Log.i(TAG,"onPlaybackError: "+error.getMessage());
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
            Log.i(TAG,"onPositionDiscontinuity");
        }
    };


    private SeekBar seekPlayerProgress;
    private Handler handler;
    private ImageView btnPlay;
    //private TextView txtCurrentTime, txtEndTime;
    private boolean isPlaying = false;
    private static final String TAG = "DetailLagu";

    ImageView back,gambar,rewind,forward;
    FontAppCompatTextView title,band;
    LinearLayout layLoading;
    int forwarRewindValue = 10000;

    ImageView buttonKaraoke;
    String musicId = "";
    FirebaseAnalytics mFirebaseAnalytics;

    String tryme="";
    Boolean fiturTry = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_detail_music);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_Page", "User view music page");
        mFirebaseAnalytics.logEvent("Activity_Music_ViewPage", params);

        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        title = findViewById(R.id.title);
        band = findViewById(R.id.band);
        gambar = findViewById(R.id.gambar);
        rewind = findViewById(R.id.rewind);
        rewind.setOnClickListener(this);
        forward = findViewById(R.id.forward);
        forward.setOnClickListener(this);
        layLoading = findViewById(R.id.layLoading);

        tryme = ""+getIntent().getStringExtra("tryme");
        if(tryme.equals("yes"))
        {
            limiterTryMe();
            fiturTry = true;
        }
        //Toast.makeText(getApplicationContext(),""+tryme,Toast.LENGTH_SHORT).show();

        musicId = ""+getIntent().getStringExtra("musicId");
        title.setText(""+getIntent().getStringExtra("title"));
        band.setText(""+getIntent().getStringExtra("band"));
        Picasso.with(getApplicationContext())
                .load(""+getIntent().getStringExtra("gambar"))
                .noPlaceholder()
                .resize(600, 600)
                .centerCrop()
                .into(gambar, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        layLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        layLoading.setVisibility(View.GONE);
                    }
                });

        prepareExoPlayerFromURL(Uri.parse(""+getIntent().getStringExtra("url")));

        buttonKaraoke = findViewById(R.id.buttonKaraoke);
        buttonKaraoke.setOnClickListener(this);
        String karaokeCek = ""+getIntent().getStringExtra("karaoke");
        if(karaokeCek.equals("0"))
        {
            buttonKaraoke.setVisibility(View.GONE);
        }
    }

    private void prepareExoPlayerFromURL(Uri uri){

        TrackSelector trackSelector = new DefaultTrackSelector();
        LoadControl loadControl = new DefaultLoadControl();
        exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "exoplayer2example"), null);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        MediaSource audioSource = new ExtractorMediaSource(uri, dataSourceFactory, extractorsFactory, null, null);
        exoPlayer.addListener(eventListener);
        exoPlayer.prepare(audioSource);
        initMediaControls();
    }

    private void initMediaControls() {
        initPlayButton();
        initSeekBar();
        //initTxtTime();
    }

    private void initPlayButton() {
        btnPlay = findViewById(R.id.btnPlay);
        btnPlay.requestFocus();
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlayPause(!isPlaying);
            }
        });
    }

    /**
     * Starts or stops playback. Also takes care of the Play/Pause button toggling
     * @param play True if playback should be started
     */
    private void setPlayPause(boolean play){
        isPlaying = play;
        exoPlayer.setPlayWhenReady(play);
        if(!isPlaying){
            Bundle params = new Bundle();
            params.putString("Stop", "User Stop playing music");
            mFirebaseAnalytics.logEvent("Activity_Music_StopMusic", params);
            btnPlay.setImageResource(R.drawable.musicplayer_play_button);
        }else{
            //setProgress();
            btnPlay.setImageResource(R.drawable.musicplayer_pause_button);
            Bundle params = new Bundle();
            params.putString("Play", "User play music");
            mFirebaseAnalytics.logEvent("Activity_Music_PlayMusic", params);
        }
    }

    private void initTxtTime() {
        /*txtCurrentTime = (TextView) findViewById(R.id.time_current);
        txtEndTime = (TextView) findViewById(R.id.player_end_time);*/
    }

    private String stringForTime(int timeMs) {
        StringBuilder mFormatBuilder;
        Formatter mFormatter;
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        int totalSeconds =  timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours   = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private void setProgress() {
        //seekPlayerProgress.setProgress(0);
        if(fiturTry == true)
        {
            seekPlayerProgress.setMax(30);
        }
        else
        {
            seekPlayerProgress.setMax((int) exoPlayer.getDuration()/1000);
            if(handler == null)handler = new Handler();
            //Make sure you update Seekbar on UI thread
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (exoPlayer != null) {
                        seekPlayerProgress.setMax((int) exoPlayer.getDuration()/1000);
                        int mCurrentPosition = (int) exoPlayer.getCurrentPosition() / 1000;
                        seekPlayerProgress.setProgress(mCurrentPosition);
                    /*txtCurrentTime.setText(stringForTime((int)exoPlayer.getCurrentPosition()));
                    txtEndTime.setText(stringForTime((int)exoPlayer.getDuration()));*/

                        handler.postDelayed(this, 1000);
                    }
                }
            });
        }

        /*txtCurrentTime.setText(stringForTime((int)exoPlayer.getCurrentPosition()));
        txtEndTime.setText(stringForTime((int)exoPlayer.getDuration()));*/
    }

    private void initSeekBar() {
        seekPlayerProgress = (SeekBar) findViewById(R.id.mediacontroller_progress);
        seekPlayerProgress.requestFocus();

        seekPlayerProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser) {
                    // We're not interested in programmatically generated changes to
                    // the progress bar's position.
                    return;
                }

                exoPlayer.seekTo(progress*1000);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekPlayerProgress.setMax(0);
        seekPlayerProgress.setMax((int) exoPlayer.getDuration()/1000);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.buttonKaraoke:
                Bundle params = new Bundle();
                params.putString("GoToKaraoke", "User go to karaoke page from music page");
                mFirebaseAnalytics.logEvent("Activity_Music_GoToKaraoke", params);
                Intent intent = new Intent(MusicDetail.this,StartKaraoke.class);
                intent.putExtra("title","" + getIntent().getStringExtra("title"));
                intent.putExtra("band","" + getIntent().getStringExtra("band"));
                intent.putExtra("gambar","" + getIntent().getStringExtra("gambar"));
                intent.putExtra("url_karaoke","" + getIntent().getStringExtra("url_karaoke"));
                intent.putExtra("lirik","" + getIntent().getStringExtra("lirik"));
                intent.putExtra("musicId","" + musicId);
                intent.putExtra("tryme","" + tryme);
                startActivity(intent);
                break;

            case R.id.back:
                Bundle params2 = new Bundle();
                params2.putString("Back", "User back from music page");
                mFirebaseAnalytics.logEvent("Activity_Music_GoBack", params2);
                onBackPressed();
                break;

            case R.id.rewind:
                int rewindPosition = (int) exoPlayer.getCurrentPosition() - forwarRewindValue;
                int rewindPositionForProgress = (int) exoPlayer.getCurrentPosition() / 1000;
                if(rewindPosition <= forwarRewindValue)
                {
                    exoPlayer.seekTo(0);
                    seekPlayerProgress.setProgress(0);
                }
                else
                {
                    exoPlayer.seekTo(rewindPosition);
                    seekPlayerProgress.setProgress(rewindPositionForProgress);
                }
                break;

            case R.id.forward:
                int forwardPosition = (int) exoPlayer.getCurrentPosition() + forwarRewindValue;
                int forwardPositionForProgress = (int) exoPlayer.getCurrentPosition() / 1000;
                if(forwardPosition >= exoPlayer.getDuration())
                {
                    exoPlayer.seekTo(0);
                    seekPlayerProgress.setProgress(0);
                }
                else
                {
                    exoPlayer.seekTo(forwardPosition);
                    seekPlayerProgress.setProgress(forwardPositionForProgress);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        releaseMusic();
    }

    public void releaseMusic()
    {
        exoPlayer.stop(true);
        exoPlayer.stop();
        exoPlayer.release();
    }


    public void limiterTryMe()
    {
        CountDownTimer countDownTimerLimiter;
        countDownTimerLimiter = new CountDownTimer(30000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {
                long x = 30000-millisUntilFinished;
                long z = x/1000;
                seekPlayerProgress.setProgress((int) z);
            }

            public void onFinish()
            {
                setPlayPause(false);
                dialog_tryMe("Anda hanya dapat mencoba lagu selama 30 detik. Silakan membeli paket untuk melanjutkan atau pilih lagu gratis");
            }
        };
        countDownTimerLimiter.start();
    }

    public void dialog_tryMe(String message)
    {
        DialogPlus warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
                onBackPressed();
            }
        });
        warningDialog.show();

    }

}