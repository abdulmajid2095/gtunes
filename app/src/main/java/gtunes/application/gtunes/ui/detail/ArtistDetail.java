package gtunes.application.gtunes.ui.detail;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.adapter.LaguAdapter;
import gtunes.application.gtunes.model.DataLagu;
import gtunes.application.gtunes.model.ResponseGetArtistDetail;
import gtunes.application.gtunes.model.ResponseGetLaguKaraoke;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArtistDetail extends AppCompatActivity implements View.OnClickListener{

    ImageView back;
    FontAppCompatTextView textArtis1,textArtis2;
    ImageView foto;

    List<DataLagu> listLaguAllData = new ArrayList<>();
    List<DataLagu> listLaguTemp = new ArrayList<>();
    RecyclerView recyclerView;
    LaguAdapter myAdapter;
    String idArtist="";
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_artist_detail);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_Detail_Artis", "User view list song of artis");
        mFirebaseAnalytics.logEvent("Activity_Artis_ViewListSong", params);

        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        textArtis1 = findViewById(R.id.tv_name_artis);
        textArtis1.setText(""+getIntent().getStringExtra("artis"));

        foto = findViewById(R.id.foto);
        Picasso.with(getApplicationContext())
                .load(""+getIntent().getStringExtra("gambar"))
                .noPlaceholder()
                .resize(600, 600)
                .centerCrop()
                .into(foto);

        textArtis2 = findViewById(R.id.artis2);
        textArtis2.setText(""+getIntent().getStringExtra("artis"));
        idArtist = ""+getIntent().getStringExtra("idArtis");
        getArtisLagu();
        recyclerView = findViewById(R.id.rv_lagu_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new LaguAdapter(getApplicationContext(),recyclerView,listLaguTemp);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    public void getArtisLagu()
    {
        listLaguAllData.clear();
        listLaguTemp.clear();

        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseGetArtistDetail> userCall = service.getLaguArtis("http://api.gtunes.co.id/artist/"+idArtist);
        userCall.enqueue(new Callback<ResponseGetArtistDetail>() {
        Response<ResponseGetArtistDetail> response) {
                if(response.isSuccessful())
                {
                    try {
                        listLaguAllData = null;
                        listLaguAllData = response.body().getData().getListSong();
                        //Toast.makeText(getApplicationContext(),""+listLaguAllData.get(0).get_artist_name().toString(),Toast.LENGTH_SHORT).show();
                        if(listLaguAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            for (int i = 0; i < listLaguAllData.size(); i++)
                            {
                                DataLagu dataLagu = new DataLagu( listLaguAllData.get(i).get_music_id(),
                                        listLaguAllData.get(i).get_music_title(),
                                        listLaguAllData.get(i).get_music_artist(),
                                        "http://gtunes.co.id/"+listLaguAllData.get(i).get_music_url(),
                                        "http://gtunes.co.id/"+listLaguAllData.get(i).get_music_artwork(),
                                        listLaguAllData.get(i).get_artist_name(),
                                        listLaguAllData.get(i).get_artist_id(),
                                        "-",
                                        "-",
                                        "1",
                                        "0",
                                        "no"
                                );
                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                                listLaguTemp.add(dataLagu);
                            }


                            recyclerView.setAdapter(myAdapter);
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getApplicationContext(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetArtistDetail> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
