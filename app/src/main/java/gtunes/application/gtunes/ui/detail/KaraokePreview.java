package gtunes.application.gtunes.ui.detail;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.coremedia.iso.boxes.Container;
import com.daasuu.mp4compose.FillMode;
import com.daasuu.mp4compose.Rotation;
import com.daasuu.mp4compose.composer.Mp4Composer;
import com.daasuu.mp4compose.filter.GlSepiaFilter;
import com.daasuu.mp4compose.filter.GlWatermarkFilter;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.pixplicity.fontview.FontAppCompatTextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Utilities.FfmpgProcessing;
import gtunes.application.gtunes.Utilities.ResultHolder;
import gtunes.application.gtunes.ui.dashboard.DashboardActivity;

public class KaraokePreview extends AppCompatActivity implements View.OnClickListener{

    final  String TAG = "FFMPG";
    String pathAudio = "";
    String pathKaraoke = "";
    String cameraChoice = "";
    String pathVideo = "";

    FontAppCompatTextView title,band;
    ImageView hapus,simpan,share,play,pause,image;
    LinearLayout layPlayPause;
    Boolean playing = false;

    VideoView videoView;
    MediaPlayer audioPlayer;
    MediaPlayer videoPlayer;
    MediaPlayer karaokePlayer;

    SeekBar seekBar;

    //AUDIO VIDEO PROCESSING
    DialogPlus warningDialog, dialog;
    String cachePathVideoMerge="";
    String cachePathAudioGet="";
    String cachePathAudioMerge="";
    String pathImage = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache/image.png";
    String outputMux = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "/gtunesVideo.mp4";
    CountDownTimer countDownTimer;
    Boolean first = true;
    Boolean saved = false;
    String finalAudio="";
    String finalVideo="";
    Boolean fromDelete = false;
    FirebaseAnalytics mFirebaseAnalytics;
    TextView textLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_karaoke_preview);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("Play_Preview", "User play preview recording");
        mFirebaseAnalytics.logEvent("Activity_Karaoke_PlayPreview", params);

        seekBar = findViewById(R.id.mediacontroller_progress);
        pathAudio = ""+getIntent().getStringExtra("audio");
        pathKaraoke = ""+getIntent().getStringExtra("karaoke");
        cameraChoice = ""+getIntent().getStringExtra("camera");

        hapus = findViewById(R.id.hapus);
        hapus.setOnClickListener(this);
        simpan = findViewById(R.id.simpan);
        simpan.setOnClickListener(this);
        share = findViewById(R.id.share);
        share.setOnClickListener(this);
        play = findViewById(R.id.play);
        play.setOnClickListener(this);
        pause = findViewById(R.id.share);
        pause.setOnClickListener(this);
        layPlayPause= findViewById(R.id.layPlay);
        layPlayPause.setOnClickListener(this);
        image= findViewById(R.id.gambar);
        image.setOnClickListener(this);


        title = findViewById(R.id.title);
        band = findViewById(R.id.band);
        title.setText(""+getIntent().getStringExtra("title"));
        band.setText(""+getIntent().getStringExtra("band"));

        videoView = findViewById(R.id.video);
        videoView.setOnClickListener(this);
        if(cameraChoice.equals("true"))
        {
            pathVideo = ""+getIntent().getStringExtra("video");
                videoView.setVisibility(View.VISIBLE);
                videoView.setVideoURI(Uri.parse(pathVideo));
                MediaController mediaController = new MediaController(this);
                mediaController.setVisibility(View.GONE);
                videoView.setMediaController(mediaController);
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        videoPlayer = mp;
                        videoPlayer.setLooping(false);
                        int max = videoPlayer.getDuration();
                        seekBar.setMax(max);
                        float multiplier = (float) videoView.getWidth() / (float) mp.getVideoWidth();
                        videoView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (mp.getVideoHeight() * multiplier)));
                        videoPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                play.setVisibility(View.VISIBLE);
                                karaokePlayer.pause();
                                karaokePlayer.seekTo(0);
                                play.setBackgroundResource(R.drawable.video_play);
                            }
                        });
                    }
                });

        }
            audioPlayer = new MediaPlayer();
            try {
                File fileAudio = new File(pathAudio);
                int file_size = Integer.parseInt(String.valueOf(fileAudio.length()/1024));
                int file_size2 = file_size/1024;
                //Toast.makeText(getApplicationContext(),"Ukuran Audio : "+file_size,Toast.LENGTH_SHORT).show();
                audioPlayer.setDataSource(""+pathAudio);
                audioPlayer.prepare();
                int max = audioPlayer.getDuration();
                Log.e("DURATION AUDIO",""+max);
                seekBar.setMax(max);
                audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        karaokePlayer.pause();
                        karaokePlayer.seekTo(0);
                        play.setBackgroundResource(R.drawable.video_play);
                    }
                });
            } catch (Exception e) {
                // make something
            }

        karaokePlayer = new MediaPlayer();
        try {
            int maxVolume = 100;
            float log1=(float)(Math.log(maxVolume-70)/Math.log(maxVolume));
            karaokePlayer.setVolume(1-log1,1-log1);
            karaokePlayer.setDataSource(""+pathKaraoke);
            karaokePlayer.prepare();
        } catch (Exception e) {
            // make something
        }

        countDownTimer = new CountDownTimer(300000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {
                if(cameraChoice.equals("true"))
                {

                    seekBar.setProgress(videoPlayer.getCurrentPosition());
                }
                else
                {
                    seekBar.setProgress(audioPlayer.getCurrentPosition());
                }
            }

            public void onFinish()
            {
                countDownTimer.start();
            }
        };

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.hapus:
                Bundle params = new Bundle();
                params.putString("Karaoke_Delete", "User delete recording");
                mFirebaseAnalytics.logEvent("Activity_Karaoke_Delete", params);
                fromDelete = true;
                onBackPressed();
                break;

            case R.id.simpan:
                Bundle params2 = new Bundle();
                params2.putString("Karaoke_Simpan", "User save recording");
                mFirebaseAnalytics.logEvent("Activity_Karaoke_Simpan", params2);
                if(cameraChoice.equals("true"))
                {
                    //getAudioFromVideo();
                    countdownSaveVideo();
                    mergeAudio();
                }
                else
                {
                    countdownSaveAudio();
                    mergeAudioRecord();
                }
                break;

            case R.id.share:
                Bundle params3 = new Bundle();
                params3.putString("Karaoke_Share", "User share recording");
                mFirebaseAnalytics.logEvent("Activity_Karaoke_Share", params3);
                if(saved == true)
                {
                    getDynamicLink();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Simpan terlebih dahulu",Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.play:
                if(playing == false)
                {
                    playing = true;
                    play.setBackgroundResource(R.drawable.video_pause);
                    if(cameraChoice.equals("true"))
                    {
                        countDownTimer.start();
                        videoPlayer.start();
                        audioPlayer.start();
                        karaokePlayer.start();
                    }
                    else
                    {
                        countDownTimer.start();
                        audioPlayer.start();
                        karaokePlayer.start();
                    }
                }
                else
                {
                    play.setBackgroundResource(R.drawable.video_play);
                    playing = false;
                    if(cameraChoice.equals("true"))
                    {
                        countDownTimer.cancel();
                        videoPlayer.pause();
                        karaokePlayer.pause();
                        audioPlayer.pause();
                    }
                    else
                    {
                        countDownTimer.cancel();
                        audioPlayer.pause();
                        karaokePlayer.pause();
                    }
                }

                break;

            case R.id.pause:

                play.setVisibility(View.VISIBLE);
                pause.setVisibility(View.GONE);

                break;

            case R.id.layPlay:
                //layPlayPause.setVisibility(View.GONE);
                break;

            case R.id.video:
                //layPlayPause.setVisibility(View.VISIBLE);
                break;

            case R.id.image:
                //layPlayPause.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(countDownTimer == null)
        {

        }
        else
        {
            countDownTimer.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        deleteCache();
        if(cameraChoice.equals("true"))
        {
            try {
                countDownTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if(videoPlayer == null)
                {

                }
                else
                {
                    videoPlayer.stop();
                    //videoPlayer.release();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }

            try {
                if(karaokePlayer == null)
                {

                }
                else
                {
                    karaokePlayer.stop();
                    //karaokePlayer.release();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }


        }
        else
        {
            try {
                countDownTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if(audioPlayer == null)
                {

                }
                else
                {
                    audioPlayer.stop();
                    //audioPlayer.release();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }

            try {
                if(karaokePlayer == null)
                {

                }
                else
                {
                    karaokePlayer.stop();
                    //karaokePlayer.release();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
        finish();
        Intent intent = new Intent(KaraokePreview.this, DashboardActivity.class);
        startActivity(intent);
    }

    public void loading()
    {
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_loading))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = dialog.getHolderView();
        textLoading = view.findViewById(R.id.message);
        textLoading.setText("Processing...");
        dialog.show();
    }

    public void dialog_warning(String message)
    {
        warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();


    }

    /*public void getAudioFromVideo()
    {
        cachePathAudioGet = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "/cacheGet.mp3";
        String cmd = String.format(FfmpgProcessing.GET_AUDIO_FROM_VIDEO, pathVideo, cachePathAudioGet);

        execFFmpegBinary(cmd,"getAudio");
        loading();
    }*/
    public void mergeAudio()
    {
        loading();
        cachePathAudioMerge = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "/gTunesAudioMerge.mp4";
        String cmd = String.format(FfmpgProcessing.MERGE_AUDIO, pathKaraoke, pathAudio, cachePathAudioMerge);
        execFFmpegBinary(cmd,"mergeAudio");
    }

    /*public void mergerAudioVideo()
    {
        cachePathVideoMerge = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "/gtunesVideo.mp4";
        String cmd = String.format(FfmpgProcessing.MERGE_AUDIO_AND_VIDEO, pathVideo, cachePathAudioMerge, cachePathVideoMerge);
        execFFmpegBinary(cmd,"mergeVideo");
    }*/

    public void addWatermark()
    {
        //Random
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        finalVideo = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/"+ "GTunes_VID_"+ts+".mp4";
        String cmd = String.format(FfmpgProcessing.ADD_WATERMARK, outputMux, pathImage, finalVideo);
        execFFmpegBinary(cmd,"watermark");
    }

    /*public void newProcessVideo()
    {
        loading();
        cachePathWatermak = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "/gtunesFinal.mp4";
        String cmd = String.format(FfmpgProcessing.NEW_PROCESS_VIDEO, pathVideo, pathKaraoke, cachePathWatermak);
        execFFmpegBinary(cmd,"newProcess");
    }*/

    public void mergeAudioRecord()
    {
        loading();
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        finalAudio = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/"+"GTunes_AUD_"+ts+".mp3";
        String cmd = String.format(FfmpgProcessing.MERGE_AUDIO, pathKaraoke, pathAudio, finalAudio);
        execFFmpegBinary(cmd,"mergeAudioRecord");
    }

    private void execFFmpegBinary(final String comd, final String cek) {
        String[] command = comd.split(",");
        Log.i(TAG, "execFFmpegBinary: " + comd);
        FFmpeg fFmpeg = FFmpeg.getInstance(this);
        PowerManager.WakeLock wakeLock = null;
        try {
            PowerManager powerManager = (PowerManager) this.getSystemService(Activity.POWER_SERVICE);
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "VK_LOCK");
            wakeLock.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            fFmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    dialog.dismiss();
                    Log.d(TAG, "onFailure: " + s);
//                    Toast.makeText(StartupScreen.this, "There is some problem inmerging", Toast.LENGTH_LONG).show();
                    if(cek.equals("getAudio"))
                    {
                        Toast.makeText(KaraokePreview.this, "Get Audio Failed", Toast.LENGTH_LONG).show();
                    }
                    else if(cek.equals("mergeAudio"))
                    {
                        Toast.makeText(KaraokePreview.this, "Failed. Please Try Again", Toast.LENGTH_LONG).show();
                    }
                    else if(cek.equals("mergeVideo"))
                    {
                        Toast.makeText(KaraokePreview.this, "Failed. Please Try Again", Toast.LENGTH_LONG).show();
                    }
                    else if(cek.equals("watermark"))
                    {
                        Toast.makeText(KaraokePreview.this, "Failed. Please Try Again", Toast.LENGTH_LONG).show();
                    }
                    else if(cek.equals("mergeAudioRecord"))
                    {
                        Toast.makeText(KaraokePreview.this, "Failed. Please Try Again", Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onSuccess(String s) {
                    if(cek.equals("getAudio"))
                    {
                        //Toast.makeText(KaraokePreview.this, "Get Audio Success", Toast.LENGTH_LONG).show();
                        mergeAudio();
                    }
                    else if(cek.equals("mergeAudio"))
                    {
                        //dialog.dismiss();
                        //Toast.makeText(KaraokePreview.this, "Merge Audio Success", Toast.LENGTH_LONG).show();
                        mux();
                    }
                    else if(cek.equals("mergeVideo"))
                    {
                        //Toast.makeText(KaraokePreview.this, "Merge Video Success", Toast.LENGTH_LONG).show();
                        addWatermark();
                    }
                    else if(cek.equals("watermark"))
                    {
                        deleteCache();
                        Toast.makeText(KaraokePreview.this, "Success", Toast.LENGTH_LONG).show();
                        saved = true;
                        dialog.dismiss();
                    }
                    else if(cek.equals("mergeAudioRecord"))
                    {
                        Toast.makeText(KaraokePreview.this, "Success", Toast.LENGTH_LONG).show();
                        saved = true;
                        dialog.dismiss();
                    }
                }

                @Override
                public void onProgress(String s) {
                    //progressBar.setMessage("Processing\n" + s);
                    Log.e("PROCESS",""+s);
                }

                @Override
                public void onStart() {
                    //handler.sendEmptyMessage(Utility.START_PROGRESS_MSG);
                }

                @Override
                public void onFinish() {
                    Log.d(TAG, "Finished command : ffmpeg " + comd);
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        } finally {
            wakeLock.release();
        }
    }


    public void shareVideo(String message)
    {
        Uri uriVideo = Uri.fromFile(new File(finalVideo));
        Intent shareIntent=new Intent(Intent.ACTION_SEND);
        shareIntent.setType("video/mp4");
        shareIntent.putExtra(Intent.EXTRA_STREAM,uriVideo);
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(shareIntent,"Bagikan Rekaman Video GTunes"));
    }

    public void shareAudio(String message)
    {
        Uri uriAudio = Uri.fromFile(new File(finalAudio));
        Intent shareIntent=new Intent(Intent.ACTION_SEND);
        shareIntent.setType("audio/mp3");
        shareIntent.putExtra(Intent.EXTRA_STREAM,uriAudio);
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(shareIntent,"Bagikan Rekaman Audio GTunes"));
    }

    public void deleteCache() {
        if (fromDelete == true) {
            Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_SHORT).show();
        }
            //Delete cache Karaoke
            File fdelete = new File(pathKaraoke);
            if (fdelete.exists()) {
                if (fdelete.delete()) {
                    Log.e("file Deleted :" , ""+pathKaraoke);
                } else {
                    Log.e("file Deleted :" , ""+pathKaraoke);
                }
            }

        //Delete cache Rekaman Audio
        File fdelete2 = new File(pathAudio);
        if (fdelete2.exists()) {
            if (fdelete2.delete()) {
                Log.e("file Deleted :" , ""+pathAudio);
            } else {
                Log.e("file Deleted :" , ""+pathAudio);
            }
        }

        //Delete cache Rekaman Video
        File fdelete3 = new File(pathVideo);
        if (fdelete3.exists()) {
            if (fdelete3.delete()) {
                Log.e("file Deleted :" , ""+pathAudio);
            } else {
                Log.e("file Deleted :" , ""+pathAudio);
            }
        }

            //Delete cache get Audio
            File fdelete4 = new File(cachePathAudioGet);
            if (fdelete4.exists()) {
                if (fdelete4.delete()) {
                    Log.e("file Deleted :", "" + cachePathAudioGet);
                } else {
                    Log.e("file Deleted :", "" + cachePathAudioGet);
                }
            }

            //Delete cache Merge Audio
            File fdelete5 = new File(cachePathAudioMerge);
            if (fdelete5.exists()) {
                if (fdelete5.delete()) {
                    Log.e("file Deleted :", "" + cachePathAudioMerge);
                } else {
                    Log.e("file Deleted :", "" + cachePathAudioMerge);
                }
            }

            //Delete cache Merge Video
            File fdelete6 = new File(cachePathVideoMerge);
            if (fdelete6.exists()) {
                if (fdelete6.delete()) {
                    Log.e("file Deleted :", "" + cachePathVideoMerge);
                } else {
                    Log.e("file Deleted :", "" + cachePathVideoMerge);
                }
            }

        //Delete cache mux Video
        File fdelete7 = new File(outputMux);
        if (fdelete7.exists()) {
            if (fdelete7.delete()) {
                Log.e("file Deleted :", "" + outputMux);
            } else {
                Log.e("file Deleted :", "" + outputMux);
            }
        }

    }


    public void mux() {

        //Toast.makeText(getApplicationContext(),"Masuk Muxer",Toast.LENGTH_SHORT).show();
        try {
            //Toast.makeText(getApplicationContext(),"Masuk Try",Toast.LENGTH_SHORT).show();
            //Toast.makeText(getApplicationContext(),"Path Audio mux : "+cachePathAudioMerge,Toast.LENGTH_SHORT).show();
            File file = new File(outputMux);
            file.createNewFile();

            MediaExtractor videoExtractor = new MediaExtractor();
            videoExtractor.setDataSource(pathVideo);

            MediaExtractor audioExtractor = new MediaExtractor();
            audioExtractor.setDataSource(cachePathAudioMerge);

            Log.e(TAG, "Video Extractor Track Count " + videoExtractor.getTrackCount() );
            Log.e(TAG, "Audio Extractor Track Count " + audioExtractor.getTrackCount() );

            //Toast.makeText(getApplicationContext(),"Video Track "+videoExtractor.getTrackCount(),Toast.LENGTH_SHORT).show();
            //Toast.makeText(getApplicationContext(),"Audio Track "+audioExtractor.getTrackCount(),Toast.LENGTH_SHORT).show();

            MediaMuxer muxer = new MediaMuxer(outputMux, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);

            videoExtractor.selectTrack(0);
            MediaFormat videoFormat = videoExtractor.getTrackFormat(0);
            int videoTrack = muxer.addTrack(videoFormat);

            //Toast.makeText(getApplicationContext(),"Add video Track Success ",Toast.LENGTH_SHORT).show();

            audioExtractor.selectTrack(0);
            MediaFormat audioFormat = audioExtractor.getTrackFormat(0);
            int audioTrack = muxer.addTrack(audioFormat);

            //Toast.makeText(getApplicationContext(),"Add Audio Track Success ",Toast.LENGTH_SHORT).show();


            Log.e(TAG, "Video Format " + videoFormat.toString() );
            Log.e(TAG, "Audio Format " + audioFormat.toString() );

            boolean sawEOS = false;
            int frameCount = 0;
            int offset = 100;
            int sampleSize = 256 * 1024;
            ByteBuffer videoBuf = ByteBuffer.allocate(sampleSize);
            ByteBuffer audioBuf = ByteBuffer.allocate(sampleSize);
            MediaCodec.BufferInfo videoBufferInfo = new MediaCodec.BufferInfo();
            MediaCodec.BufferInfo audioBufferInfo = new MediaCodec.BufferInfo();


            videoExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
            audioExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC);

            muxer.start();

            while (!sawEOS)
            {
                videoBufferInfo.offset = offset;
                videoBufferInfo.size = videoExtractor.readSampleData(videoBuf, offset);


                if (videoBufferInfo.size < 0 || audioBufferInfo.size < 0)
                {
                    Log.e(TAG, "saw input EOS.");
                    sawEOS = true;
                    videoBufferInfo.size = 0;

                }
                else
                {
                    videoBufferInfo.presentationTimeUs = videoExtractor.getSampleTime();
                    //videoBufferInfo.flags = videoExtractor.getSampleFlags();
                    videoBufferInfo.flags = MediaCodec.BUFFER_FLAG_SYNC_FRAME;
                    muxer.writeSampleData(videoTrack, videoBuf, videoBufferInfo);
                    videoExtractor.advance();


                    frameCount++;
                    Log.e(TAG, "Frame (" + frameCount + ") Video PresentationTimeUs:" + videoBufferInfo.presentationTimeUs +" Flags:" + videoBufferInfo.flags +" Size(KB) " + videoBufferInfo.size / 1024);
                    Log.e(TAG, "Frame (" + frameCount + ") Audio PresentationTimeUs:" + audioBufferInfo.presentationTimeUs +" Flags:" + audioBufferInfo.flags +" Size(KB) " + audioBufferInfo.size / 1024);

                }
            }

            //Toast.makeText(getApplicationContext() , "frame:" + frameCount , Toast.LENGTH_SHORT).show();



            boolean sawEOS2 = false;
            int frameCount2 =0;
            while (!sawEOS2)
            {
                frameCount2++;

                audioBufferInfo.offset = offset;
                audioBufferInfo.size = audioExtractor.readSampleData(audioBuf, offset);

                if (videoBufferInfo.size < 0 || audioBufferInfo.size < 0)
                {
                    Log.e(TAG, "saw input EOS.");
                    sawEOS2 = true;
                    audioBufferInfo.size = 0;
                }
                else
                {
                    audioBufferInfo.presentationTimeUs = audioExtractor.getSampleTime();
                    //audioBufferInfo.flags = audioExtractor.getSampleFlags();
                    audioBufferInfo.flags = MediaCodec.BUFFER_FLAG_SYNC_FRAME;
                    muxer.writeSampleData(audioTrack, audioBuf, audioBufferInfo);
                    audioExtractor.advance();


                    Log.e(TAG, "Frame (" + frameCount + ") Video PresentationTimeUs:" + videoBufferInfo.presentationTimeUs +" Flags:" + videoBufferInfo.flags +" Size(KB) " + videoBufferInfo.size / 1024);
                    Log.e(TAG, "Frame (" + frameCount + ") Audio PresentationTimeUs:" + audioBufferInfo.presentationTimeUs +" Flags:" + audioBufferInfo.flags +" Size(KB) " + audioBufferInfo.size / 1024);

                }
            }

            muxer.stop();
            muxer.release();
            newWatermark();
            //Toast.makeText(getApplicationContext() , "Mux Selesai" , Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Log.e(TAG, "Mixer Error 1 " + e.getMessage());
            Toast.makeText(KaraokePreview.this, "Failed. Please Try Again", Toast.LENGTH_LONG).show();
            dialog.dismiss();
        } catch (Exception e) {
            Log.e(TAG, "Mixer Error 2 " + e.getMessage());
            Toast.makeText(KaraokePreview.this, "Failed. Please Try Again", Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }
    }


    public void getDynamicLink()
    {
        loading();
        final Uri[] mInvitationUrl = new Uri[1];
        String link = "https://gtunes.co.id/?shareMedia=true";
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(link))
                .setDynamicLinkDomain("gtunesid.page.link")
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder("com.gtunes.gtunes")
                                .setMinimumVersion(18)
                                .build())

                .buildShortDynamicLink()
                .addOnCompleteListener(KaraokePreview.this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()){
                            dialog.dismiss();
                            mInvitationUrl[0] = task.getResult().getShortLink();
                            String invitationLink = mInvitationUrl[0].toString();
                            final String msg = "Coba GTunes sekarang juga! - " + invitationLink;

                            Handler handler =  new Handler();
                            Runnable myRunnable = new Runnable() {
                                public void run() {
                                    if (cameraChoice.equals("true"))
                                    {
                                        shareVideo(msg);
                                    }
                                    else
                                    {
                                        shareAudio(msg);
                                    }
                                }
                            };
                            handler.postDelayed(myRunnable,100);


                        }else {
                            dialog.dismiss();
                            String errorMessage = task.getException().getMessage();
                            Toast.makeText(getApplicationContext(), ""+errorMessage, Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    public void newWatermark()
    {
        File image = new File(pathImage);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);

        //Random
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        finalVideo = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/"+ "GTunes_VID_"+ts+".mp4";

        new Mp4Composer(outputMux, finalVideo)
                .filter(new GlWatermarkFilter(bitmap, GlWatermarkFilter.Position.LEFT_BOTTOM))
                .listener(new Mp4Composer.Listener() {
                    @Override
                    public void onProgress(double progress) {
                        Log.d(TAG, "onProgress = " + progress);
                    }

                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted()");
                        saved = true;
                        runOnUiThread(() -> {
                            dialog.dismiss();
                            Toast.makeText(KaraokePreview.this, "Success", Toast.LENGTH_LONG).show();
                        });

                    }

                    @Override
                    public void onCanceled() {
                        Log.d(TAG, "onCanceled");
                    }

                    @Override
                    public void onFailed(Exception exception) {
                        Log.e(TAG, "onFailed()", exception);
                        runOnUiThread(() -> {
                            dialog.dismiss();
                            Toast.makeText(KaraokePreview.this, "Failed. Please Try Again", Toast.LENGTH_LONG).show();
                        });
                    }
                })
                .start();
    }


    public void countdownSaveVideo()
    {
        int duration = audioPlayer.getDuration()+2000;
        CountDownTimer countDownTimerLimiter;
        countDownTimerLimiter = new CountDownTimer(duration, 100) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {
                int x = (int) (duration - millisUntilFinished);
                int percentage = x*100;
                int percentage2 = percentage/duration;
                Log.e("PERCENT X",""+x);
                Log.e("PERCENT Y",""+percentage);
                Log.e("PERCENT AKHIR",""+percentage2);
                if(textLoading == null)
                {

                }
                else
                {
                    textLoading.setText("Menyimpan Video "+percentage2+" % ...");
                }
            }

            public void onFinish()
            {

            }
        };
        countDownTimerLimiter.start();
    }

    public void countdownSaveAudio()
    {
        int durationAwal = audioPlayer.getDuration()+2000;
        int duration = durationAwal/2;
        CountDownTimer countDownTimerLimiter;
        countDownTimerLimiter = new CountDownTimer(duration, 100) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {
                int x = (int) (duration - millisUntilFinished);
                int percentage = x*100;
                int percentage2 = percentage/duration;
                Log.e("PERCENT X",""+x);
                Log.e("PERCENT Y",""+percentage);
                Log.e("PERCENT AKHIR",""+percentage2);
                if(textLoading == null)
                {

                }
                else
                {
                    textLoading.setText("Menyimpan Audio "+percentage2+" % ...");
                }
            }

            public void onFinish()
            {

            }
        };
        countDownTimerLimiter.start();
    }
}
