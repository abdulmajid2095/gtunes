package gtunes.application.gtunes.ui.detail;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.pixplicity.fontview.FontAppCompatTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.Database.Data_Profile;
import gtunes.application.gtunes.MainActivity;
import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.model.DataTelcos;
import gtunes.application.gtunes.model.GoogleBilling;
import gtunes.application.gtunes.model.ResponseGetPaketDetail;
import gtunes.application.gtunes.model.ResponseLogin;
import gtunes.application.gtunes.model.SendLogin;
import gtunes.application.gtunes.model.SendPaketId;
import gtunes.application.gtunes.ui.dashboard.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaketDetail extends AppCompatActivity implements BillingProcessor.IBillingHandler {

    List<DataTelcos> listPaketAllData = new ArrayList<>();
    List<DataTelcos> listPaketTemp = new ArrayList<>();
    RecyclerView recyclerView;
    PaketDetailAdapter myAdapter;
    
    String paket_id = "";
    ImageView back;
    String IN_APP_CODE = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo1yjP+hzwPxc1m49doO0XEt7b1W4R5NlvEbzXUkkyDnOe/x5AN53rKcKXV9IAaMdgGx/2IgST7fAXCt/LXxQqUk+QTFyLufv4He5oraDjCUo4sD2CA6s7KA3ZB75WcA0Et04CVWyIOan5tZqSIL415pUrpfIwlUJD4DeLS5mn2qepaxeQnA7gu2vcHjmvEosHNvgY7rHT32NLcnaNQlCYYXSOxvlJYAW7D1bqtu291Jp3KFqyiOoUbd1NRVePO5MX7e+8CbAPW0PS6OTqYjuI+UCjGxZBj7dBtinQ8BAyQgLYopuAKe2gCQaWhYiCrhc3d6IRAoyIq6vgaTuxR3hkQIDAQAB";
    BillingProcessor billingProcessor;
    DialogPlus dialog,warningDialog;
    Data_Profile data_profile;
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_paket_detail);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_Page", "User view paket page");
        mFirebaseAnalytics.logEvent("Activity_Paket_UserSelectPaket", params);

        data_profile = Data_Profile.findById(Data_Profile.class,1L);

        billingProcessor = new BillingProcessor(this, IN_APP_CODE, this);

        paket_id = ""+getIntent().getStringExtra("paket_id");
        //Toast.makeText(getApplicationContext(),""+paket_id,Toast.LENGTH_SHORT).show();

        getDetailPaket();
        recyclerView = findViewById(R.id.rv_paket);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new PaketDetailAdapter(this,recyclerView,listPaketTemp);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void getDetailPaket()
    {
        listPaketAllData.clear();
        listPaketTemp.clear();

        SendPaketId sendPaketId = new SendPaketId(""+paket_id);
        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseGetPaketDetail> userCall = service.PaketDetail(sendPaketId);
        userCall.enqueue(new Callback<ResponseGetPaketDetail>() {
            @Override
            public void onResponse(Call<ResponseGetPaketDetail> call, Response<ResponseGetPaketDetail> response) {
                if(response.isSuccessful())
                {
                    try {
                        listPaketAllData = null;
                        listPaketAllData = response.body().getDataPaketDetail();
                        //Toast.makeText(getApplicationContext(),""+listLaguAllData.get(0).get_artist_name().toString(),Toast.LENGTH_SHORT).show();
                        if(listPaketAllData.size()==0)
                        {
                            Toast.makeText(getApplicationContext(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            for (int i = 0; i < listPaketAllData.size(); i++)
                            {
                                if(data_profile.telco.equals("nonTelco"))
                                {
                                    DataTelcos dataTelcos = new DataTelcos( listPaketAllData.get(i).get_telco(),
                                            listPaketAllData.get(i).get_url(),
                                            listPaketAllData.get(i).get_type(),
                                            listPaketAllData.get(i).get_paket_code());
                                    listPaketTemp.add(dataTelcos);
                                }
                                else
                                {
                                    if(listPaketAllData.get(i).get_type().equals("google"))
                                    {

                                    }
                                    else
                                    {
                                        DataTelcos dataTelcos = new DataTelcos( listPaketAllData.get(i).get_telco(),
                                                listPaketAllData.get(i).get_url(),
                                                listPaketAllData.get(i).get_type(),
                                                listPaketAllData.get(i).get_paket_code());
                                        listPaketTemp.add(dataTelcos);
                                    }
                                }

                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                            }

                            recyclerView.setAdapter(myAdapter);
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    Toast.makeText(getApplicationContext(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetPaketDetail> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void googlePayment(String paket_code)
    {
        Bundle params = new Bundle();
        params.putString("Purchase", "User purchase with google");
        mFirebaseAnalytics.logEvent("Activity_Paket_GooglePurchase", params);

        if(!BillingProcessor.isIabServiceAvailable(this)) {
                    Toast.makeText(getApplicationContext(),"In-app billing service is unavailable",Toast.LENGTH_SHORT);
        }
        billingProcessor.purchase(PaketDetail.this,""+paket_code);
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        String purchaseToken = details.purchaseToken;
        String purchaseTime = details.purchaseTime.toString();
        String orderId = details.orderId;
        validateInAppBilling(productId,purchaseToken,purchaseTime,orderId);
        Bundle params = new Bundle();
        params.putString("Purchase", "User success purchase with google");
        mFirebaseAnalytics.logEvent("Activity_Paket_GoogleSuccess", params);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        Bundle params = new Bundle();
        params.putString("Purchase", "User success purchase with google");
        mFirebaseAnalytics.logEvent("Activity_Paket_GoogleError", params);
    }

    @Override
    public void onBillingInitialized() {

    }

    @Override
    protected void onDestroy() {
        if(billingProcessor != null)
        {
            billingProcessor.release();
        }
        super.onDestroy();
    }

    public void validateInAppBilling(String productId, String purchaseToken, String purchaseTime, String orderId)
    {
        dialog_loading();
        Log.e("Product ID",""+productId);
        Log.e("Purchase Token",""+purchaseToken);
        Log.e("Order Id",""+orderId);

        /*Toast.makeText(getApplicationContext(),"Product ID : "+productId,Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(),"Purchase Token : "+purchaseToken,Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(),"Order ID : "+orderId,Toast.LENGTH_SHORT).show();*/

        GoogleBilling googleBilling = new GoogleBilling(productId,purchaseToken, orderId);
        String tokenBearer = ""+data_profile.token;
        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseLogin> userCall = service.googlePurchase(tokenBearer, googleBilling);
        userCall.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if(response.isSuccessful()){
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Pembelian Paket Berhasil",Toast.LENGTH_SHORT).show();
                    finish();
                    /*Intent intent = new Intent(PaketDetail.this,DashboardActivity.class);
                    startActivity(intent);*/
                    goToLogin();

                } else{
                    dialog.dismiss();
                    JSONObject jObjError = null;
                    String message = "Gagal untuk validasi. Silakan coba lagi atau hubungi kami";
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjError2 = null;
                        jObjError2 = new JSONObject(jObjError.getString("error"));
                        message = ""+jObjError2.getString("userMsg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    final String myMessage = message;
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            dialog_warning(""+myMessage);
                        }
                    };
                    handler.postDelayed(myRunnable,500);
                }

            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                dialog.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        // do something
                        dialog_warning("Terjadi kesalahan. Cek koneksi internet Anda.");
                    }
                };
                handler.postDelayed(myRunnable,500);

            }
        });
    }


    public void dialog_loading()
    {
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_loading))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = dialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        textMessage.setText("Memuat...");
        dialog.show();

    }

    public void dialog_warning(String message)
    {
        warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();

    }








    public class PaketDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;

        private OnLoadMoreListener mOnLoadMoreListener;

        private boolean isLoading;
        private int visibleThreshold = 1;
        private int lastVisibleItem, totalItemCount;
        RecyclerView mRecyclerView;
        List<DataTelcos> myArray;
        Context c;

        public PaketDetailAdapter(Context c, RecyclerView mRecyclerView, List<DataTelcos> myArray) {

            this.mRecyclerView = mRecyclerView;
            this.myArray = myArray;
            this.c = c;

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        @Override
        public int getItemViewType(int position) {
            return myArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(c).inflate(R.layout.item_pilih_paket, parent, false);
                return new PaketDetailAdapter.UserViewHolder(view);
            } else if (viewType == VIEW_TYPE_LOADING) {
                View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
                return new PaketDetailAdapter.LoadingViewHolder(view);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof PaketDetailAdapter.UserViewHolder) {

                final DataTelcos dataTelcos = myArray.get(position);
                PaketDetailAdapter.UserViewHolder userViewHolder = (PaketDetailAdapter.UserViewHolder) holder;

                userViewHolder.t_judul.setText(""+dataTelcos.get_telco());
                userViewHolder.parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(dataTelcos.get_type().equals("google"))
                        {
                            if(data_profile.login == true)
                            {
                                googlePayment(""+dataTelcos.get_paket_code());
                            }
                            else
                            {
                                //dialog_warning("Anda harus login terlebih dahulu sebelum membeli paket");
                                Intent intent = new Intent(c,MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                c.startActivity(intent);
                            }

                        }
                        else
                        {
                            if(data_profile.login == true)
                            {
                                Intent intent = new Intent(c,PaketWebview.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("title","" + ""+dataTelcos.get_telco());
                                intent.putExtra("url","" + ""+dataTelcos.get_url());
                                c.startActivity(intent);
                            }
                            else
                            {
                                //dialog_warning("Anda harus login terlebih dahulu sebelum membeli paket");
                                Intent intent = new Intent(c,MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                c.startActivity(intent);
                            }

                        }

                    }
                });

            }

            else if (holder instanceof PaketDetailAdapter.LoadingViewHolder) {
                PaketDetailAdapter.LoadingViewHolder loadingViewHolder = (PaketDetailAdapter.LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }
        }

        @Override
        public int getItemCount() {
            return myArray == null ? 0 : myArray.size();
        }

        public void setLoaded() {
            isLoading = false;
        }


        public class UserViewHolder extends RecyclerView.ViewHolder {
            public FontAppCompatTextView t_judul;
            public LinearLayout parent;

            public UserViewHolder(View view) {
                super(view);
                c = itemView.getContext();

                parent = view.findViewById(R.id.parent);
                t_judul = view.findViewById(R.id.nama_paket);
            }
        }

        public class LoadingViewHolder extends RecyclerView.ViewHolder {
            public ProgressBar progressBar;

            public LoadingViewHolder(View itemView) {
                super(itemView);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
            }
        }

    }


    public void goToLogin()
    {
        Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
        String myPhone = ""+data_profile.loginUser;
        String myPass = ""+data_profile.loginPassword;

        SendLogin sendLogin = new SendLogin(myPhone,myPass);
        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseLogin> userCall = service.login(sendLogin);
        userCall.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if(response.isSuccessful()){
                    Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
                    data_profile.token = ""+response.body().get_dataData().get_token();
                    data_profile.user_id = ""+response.body().get_dataData().get_dataUser().get_user_id();
                    data_profile.phone = ""+response.body().get_dataData().get_dataUser().get_phone();
                    data_profile.telco = ""+response.body().get_dataData().get_dataUser().get_telco();
                    data_profile.email = ""+response.body().get_dataData().get_dataUser().get_email();
                    data_profile.active_until = ""+response.body().get_dataData().get_dataUser().get_active_until();
                    data_profile.paket_status = ""+response.body().get_dataData().get_dataUser().get_paket_status();
                    data_profile.login = true;
                    data_profile.nama = ""+response.body().get_dataData().get_dataUser().get_user_id();
                    data_profile.save();
                    startActivity(new Intent(getApplicationContext(),DashboardActivity.class));
                    finish();
                } else{
                    startActivity(new Intent(getApplicationContext(),DashboardActivity.class));
                    finish();
                    /*JSONObject jObjError = null;
                    String message = "Gagal! Silakan cek email/User Id dan password anda.";
                        *//*try {
                            jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjError2 = null;
                            jObjError2 = new JSONObject(jObjError.getString("error"));
                            message = ""+jObjError2.getString("userMsg");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*//*
                    final String myMessage = message;
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            dialog_warning(""+myMessage);
                        }
                    };
                    handler.postDelayed(myRunnable,500);*/
                }

            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                startActivity(new Intent(getApplicationContext(),DashboardActivity.class));
                finish();
                /*dialog.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        // do something
                        dialog_warning("Terjadi kesalahan. Cek koneksi internet Anda.");
                    }
                };
                handler.postDelayed(myRunnable,500);*/

            }
        });
    }
}




