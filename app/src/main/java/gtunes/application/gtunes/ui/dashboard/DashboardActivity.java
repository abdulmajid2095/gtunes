package gtunes.application.gtunes.ui.dashboard;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.BuildConfig;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.joooonho.SelectableRoundedImageView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.squareup.picasso.Picasso;
import com.vistrav.ask.Ask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import gtunes.application.gtunes.Database.Data_Profile;
import gtunes.application.gtunes.MainActivity;
import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.Utilities.ImageCircleTransform;
import gtunes.application.gtunes.model.ResponseGetVersionCode;
import gtunes.application.gtunes.model.ResponseLogin;
import gtunes.application.gtunes.ui.Profile.EditProfile;
import gtunes.application.gtunes.ui.fragment.artis.ArtisFragment;
import gtunes.application.gtunes.ui.fragment.berita.BeritaFragment;
import gtunes.application.gtunes.ui.fragment.genre.GenreFragment;
import gtunes.application.gtunes.ui.fragment.karaoke.KaraokeFragment;
import gtunes.application.gtunes.ui.fragment.pilihpaket.PilihPaketFragment;
import gtunes.application.gtunes.ui.fragment.tryme.TryMeFragment;
import gtunes.application.gtunes.ui.fragment.videos.VideosFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //Store the selected item menu
    private static final String SELECTED_ITEM_ID = "SELECTED_ITEM_ID";

    private final Handler mDrawerHandler = new Handler();
    private DrawerLayout mDrawerLayout;
    private int mPrevSelectedId;
    private NavigationView mNavigationView;
    private int mSelectedId;
    private Toolbar mToolbar;
    Boolean open = false;
    DialogPlus dialog,warningDialog;
    int press = 0;
    Fragment navFragment;

    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        //Activate the firebase analytics
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //get version of the app. It use for trigger to show dialog to update the app. Forcing update
        getVersionCode();

        //Creating directory GTunes in phone drive
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() , "GTunes");
        //If the directory not present, create it
        if (!f.exists()) {
            f.mkdirs();
        }
        File f1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "GTunes", "cache");
        if (!f1.exists()) {
            f1.mkdirs();
        }

        try {
            Bitmap bm = BitmapFactory.decodeResource(getResources(),R.drawable.mipimage);
            String pathku = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache";
            File file = new File(pathku,"image.png");
            FileOutputStream outputStream = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG,100,outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mNavigationView = findViewById(R.id.navigation_view);
        assert mNavigationView != null;
        mNavigationView.setNavigationItemSelectedListener(this);


        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout, mToolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                super.onDrawerSlide(drawerView, 0); // this disables the arrow @ completed state
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0); // this disables the animation
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mDrawerToggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.drawer_new); //set your own
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (open)
                {
                    mDrawerLayout.closeDrawers();
                }

                else
                {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }

            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        mSelectedId = mNavigationView.getMenu().getItem(prefs.getInt("default_view", 1)).getItemId();
        mSelectedId = savedInstanceState == null ? mSelectedId : savedInstanceState.getInt(SELECTED_ITEM_ID);
        mPrevSelectedId = mSelectedId;
        mNavigationView.getMenu().findItem(mSelectedId).setChecked(true);

        if (savedInstanceState == null) {
            mDrawerHandler.removeCallbacksAndMessages(null);
            mDrawerHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    navigate(mSelectedId);
                }
            }, 250);

            boolean openDrawer = prefs.getBoolean("open_drawer", false);

            if (openDrawer)
            {
                mDrawerLayout.openDrawer(GravityCompat.START);
                open = true;
            }

            else
            {
                mDrawerLayout.closeDrawers();
                open = false;
            }
        }

        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                    //Toast.makeText(getApplicationContext(),"FFMPEG Mulai",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure() {
                    //Toast.makeText(getApplicationContext(),"FFMPEG Tidak Mendukung",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess() {
                    //Toast.makeText(getApplicationContext(),"FFMPEG Success",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFinish() {
                    //Toast.makeText(getApplicationContext(),"FFMPEG Finish",Toast.LENGTH_SHORT).show();
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
            //Toast.makeText(getApplicationContext(),"Tidak Mendukung",Toast.LENGTH_SHORT).show();
        }

        setdata();
        check_permission();

        try {
            Log.e("Masuk","Try");
            String cek = ""+getIntent().getStringExtra("tab");
            Log.e("Masuk","Tab "+cek);
            if(cek.equals("paket"))
            {
                navFragment = null;
                Log.e("Masuk","Tab");
                Bundle params6 = new Bundle();
                params6.putString("Click_Paket", "User select page beli paket");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickPaket", params6);
                mPrevSelectedId = R.id.drawer_item_6;
                setTitle(R.string.nav_belipake);
                navFragment = new PilihPaketFragment();
                final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp(4));

                if (navFragment != null) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                    try {
                        transaction.replace(R.id.content_frame, navFragment).commit();
                    } catch (IllegalStateException ignored) {
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("catch",""+e);
        }
    }

    public void switchFragment(int itemId) {
        mSelectedId = mNavigationView.getMenu().getItem(itemId).getItemId();
        mNavigationView.getMenu().findItem(mSelectedId).setChecked(true);
        mDrawerHandler.removeCallbacksAndMessages(null);
        mDrawerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(mSelectedId);
            }
        }, 250);
        mDrawerLayout.closeDrawers();
    }

    private void navigate(final int itemId) {
        final View elevation = findViewById(R.id.elevation);
        navFragment = null;
        switch (itemId) {
            case R.id.drawer_item_1:
                Bundle params = new Bundle();
                params.putString("Click_Berita", "User select page berita");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickBerita", params);
                mPrevSelectedId = itemId;
                setTitle(R.string.nav_berita);
                navFragment = new BeritaFragment();
                break;
            case R.id.drawer_item_2:
                Bundle params2 = new Bundle();
                params2.putString("Click_Artist", "User select page artis");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickArtist", params2);
                mPrevSelectedId = itemId;
                setTitle(R.string.nav_artis);
                navFragment = new ArtisFragment();
                break;
            case R.id.drawer_item_3:
                Bundle params3 = new Bundle();
                params3.putString("Click_Genre", "User select page genre");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickGenre", params3);
                mPrevSelectedId = itemId;
                setTitle(R.string.nav_genre);
                navFragment = new GenreFragment();
                break;
            case R.id.drawer_item_4:
                Bundle params4 = new Bundle();
                params4.putString("Click_Video", "User select page video");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickVideo", params4);
                mPrevSelectedId = itemId;
                setTitle(R.string.nav_video);
                navFragment = new VideosFragment();
                break;
            case R.id.drawer_item_5:
                Bundle params5 = new Bundle();
                params5.putString("Click_Karaoke", "User select page karaoke");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickKaraoke", params5);
                mPrevSelectedId = itemId;
                setTitle(R.string.nav_karaoke);
                navFragment = new KaraokeFragment();
                break;
            case R.id.drawer_item_6:
                Bundle params6 = new Bundle();
                params6.putString("Click_Paket", "User select page beli paket");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickPaket", params6);
                mPrevSelectedId = itemId;
                setTitle(R.string.nav_belipake);
                navFragment = new PilihPaketFragment();
                break;
            case R.id.drawer_item_7:
                Bundle params7 = new Bundle();
                params7.putString("Click_TryMe", "User select page artis");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickTryMe", params7);
                mPrevSelectedId = itemId;
                setTitle(R.string.nav_tryme);
                navFragment = new TryMeFragment();
                break;
        }

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp(4));

        if (navFragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            try {
                transaction.replace(R.id.content_frame, navFragment).commit();

                if (elevation != null) {
                    //params.topMargin = navFragment instanceof AdvancedInstallerFragment ? dp(48) : 0;

                    Animation a = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
                            elevation.setLayoutParams(params);
                        }
                    };
                    a.setDuration(150);
                    elevation.startAnimation(a);
                }
            } catch (IllegalStateException ignored) {
            }
        }
    }

    public int dp(float value) {
        float density = getApplicationContext().getResources().getDisplayMetrics().density;

        if (value == 0) {
            return 0;
        }
        return (int) Math.ceil(density * value);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        mSelectedId = menuItem.getItemId();
        mDrawerHandler.removeCallbacksAndMessages(null);
        mDrawerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(mSelectedId);
            }
        }, 250);
        mDrawerLayout.closeDrawers();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED_ITEM_ID, mSelectedId);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            if(press==0)
            {
                if(mPrevSelectedId==0 || mPrevSelectedId >1)
                {
                    mPrevSelectedId = 1;
                    setTitle(R.string.nav_artis);
                    navFragment = new ArtisFragment();
                }
                Toast.makeText(getApplicationContext(), "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();
                press = 1;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        press = 0;
                    }
                },3000);
            }
            else {
                Bundle params = new Bundle();
                params.putString("Click_Exit", "User exit from application");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickExit", params);

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                press = 0;
            }
        }
    }

    public void setdata()
    {
        View header = mNavigationView.getHeaderView(0);
        ImageView fotoProfil = header.findViewById(R.id.foto);
        ImageView editProfil = header.findViewById(R.id.edit);
        ImageView logout = header.findViewById(R.id.logout);
        FontAppCompatTextView nama = header.findViewById(R.id.nama);
        FontAppCompatTextView jankie = header.findViewById(R.id.musicJankie);
        FontAppCompatTextView paket = header.findViewById(R.id.nama_paket);
        FontAppCompatTextView sisa = header.findViewById(R.id.sisa_paket);
        LinearLayout layPaket = header.findViewById(R.id.layPaket);
        Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
        if(data_profile.login)
        {
            //IF USER LOGIN
            logout.setVisibility(View.VISIBLE);
            layPaket.setVisibility(View.VISIBLE);
            nama.setVisibility(View.VISIBLE);
            jankie.setText("Music Jankie");
            nama.setText(""+data_profile.nama);
            //paket.setText(""+data_profile.customer_paket_name);
            sisa.setText(""+data_profile.active_until );
            //Toast.makeText(getApplicationContext(),"DISET "+data_profile.customer_avatar,Toast.LENGTH_SHORT).show();

            //Toast.makeText(getApplicationContext(),"AVATAR "+data_profile.avatar,Toast.LENGTH_SHORT).show();
            if(data_profile.avatar.equals("default"))
            {
                fotoProfil.setImageResource(R.drawable.profile);
            }
            else
            {
                try {
                    //Uri uriku = Uri.parse(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache/profile.png").toString());
                    /*Uri uriku = Uri.parse(new File(""+data_profile.avatar).toString());
                    Picasso.with(getApplicationContext())
                            .load(uriku)
                            .noPlaceholder()
                            .transform(new ImageCircleTransform())
                            .resize(300, 300)
                            .centerCrop()
                            .into(fotoProfil);*/
                    File imgFile = new  File(""+data_profile.avatar);
                    /*if(imgFile.exists()){
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        fotoProfil.setImageBitmap(myBitmap);
                        Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
                    }*/
                    Picasso.with(getApplicationContext())
                            .load(imgFile)
                            .noPlaceholder()
                            .transform(new ImageCircleTransform())
                            .resize(300, 300)
                            .centerCrop()
                            .into(fotoProfil);

                } catch (Exception e) {
                    e.printStackTrace();
                    fotoProfil.setImageResource(R.drawable.profile);
                    //Toast.makeText(getApplicationContext(),"CATCH "+e,Toast.LENGTH_SHORT).show();
                }
            }
           /* Picasso.with(getApplicationContext())
                    .load(""+data_profile.customer_avatar)
                    .noPlaceholder()
                    .resize(300, 300)
                    .centerCrop()
                    .transform(new ImageCircleTransform())
                    .into(fotoProfil);*/

            fotoProfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(DashboardActivity.this,EditProfile.class);
                    startActivityForResult(intent,78);
                }
            });
            editProfil.setVisibility(View.VISIBLE);
        }

        else
        {
            //IF USER NOT LOGIN
            logout.setVisibility(View.GONE);
            layPaket.setVisibility(View.GONE);
            nama.setVisibility(View.GONE);
            jankie.setText("Login");
            editProfil.setVisibility(View.GONE);
            fotoProfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle params = new Bundle();
                    params.putString("Click_Login", "User click login");
                    mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickLogin", params);
                    startActivity(new Intent(DashboardActivity.this, MainActivity.class));
                }
            });
        }


        editProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle params = new Bundle();
                params.putString("Click_Edit_Profile", "User click edit profile");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_EditProfile", params);

                Intent intent = new Intent(DashboardActivity.this,EditProfile.class);
                startActivityForResult(intent,78);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle params = new Bundle();
                params.putString("Click_Logout", "User click logout");
                mFirebaseAnalytics.logEvent("Activity_Dashboard_ClickLogout", params);
                logoutUser();
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //Toast.makeText(getApplicationContext(),"request "+requestCode,Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(),"result  "+resultCode,Toast.LENGTH_SHORT).show();
        if(requestCode == 78 && resultCode == 1) {
            //Toast.makeText(getApplicationContext(),"UPDATE HARUSE",Toast.LENGTH_SHORT).show();
            setdata();
        }
    }

    public void dialog_loading()
    {
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_loading))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = dialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        textMessage.setText("Logout ...");
        dialog.show();

    }

    public void dialog_warning(String message)
    {
        warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();

    }

    public void logoutUser()
    {
        Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
        dialog_loading();
        String tokenBearer = "Bearer" + " " + data_profile.token;
        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseLogin> userCall = service.logout(tokenBearer);
        userCall.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if(response.isSuccessful()){
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Logout Sukses",Toast.LENGTH_SHORT).show();
                    data_profile.token = "";
                    data_profile.login = false;
                    data_profile.save();
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                } else{
                    dialog.dismiss();
                    JSONObject jObjError = null;
                    String message = "Logout gagal !";
                    /*try {
                        jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjError2 = null;
                        jObjError2 = new JSONObject(jObjError.getString("error"));
                        message = ""+jObjError2.getString("userMsg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                    final String myMessage = message;
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            dialog_warning(""+myMessage);
                        }
                    };
                    handler.postDelayed(myRunnable,500);
                }

            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                dialog.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        // do something
                        dialog_warning("Terjadi kesalahan. Cek koneksi internet Anda.");
                    }
                };
                handler.postDelayed(myRunnable,500);

            }
        });
    }

    public void check_permission()
    {
        Ask.on(this)
                .id(1) // in case you are invoking multiple time Ask from same activity or fragment
                .forPermissions(Manifest.permission.CAMERA
                        , Manifest.permission.READ_EXTERNAL_STORAGE
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.RECORD_AUDIO
                        , Manifest.permission.INTERNET)
                /*.withRationales("Location permission need for map to work properly",
                        "In order to save file you will need to grant storage permission") *///optional
                .go();
    }



    public void getVersionCode()
    {
        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseGetVersionCode> userCall = service.getVersion();
        userCall.enqueue(new Callback<ResponseGetVersionCode>() {
            @Override
            public void onResponse(Call<ResponseGetVersionCode> call, Response<ResponseGetVersionCode> response) {
                if(response.isSuccessful())
                {
                    String version = "" + response.body().get_dataVersionCode().get_version_code();
                    int verCode = 0;
                    try {
                        PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
                        verCode = pInfo.versionCode;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int versionNet = 0;
                    try {
                        versionNet = Integer.parseInt("" + version);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //Toast.makeText(getApplicationContext(),"VersionCode : "+verCode,Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getApplicationContext(),"VersionNet : "+versionNet,Toast.LENGTH_SHORT).show();

                    if (verCode < versionNet) {
                        dialog_update();
                    }

                } else{

                }

            }

            @Override
            public void onFailure(Call<ResponseGetVersionCode> call, Throwable t) {

            }
        });
    }

    public void dialog_update()
    {
        DialogPlus warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_update))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.gtunes.gtunes");
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                likeIng.setPackage("com.android.vending");
                startActivity(likeIng);
            }
        });
        warningDialog.show();

    }
}
