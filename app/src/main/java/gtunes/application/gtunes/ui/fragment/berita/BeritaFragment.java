package gtunes.application.gtunes.ui.fragment.berita;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.adapter.BeritaAdapter;
import gtunes.application.gtunes.model.DataBerita;
import gtunes.application.gtunes.model.ResponseGetBerita;
import gtunes.application.gtunes.ui.detail.BeritaDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BeritaFragment extends Fragment implements View.OnClickListener {

    //Data Worker
    List<DataBerita> listNewsAllData = new ArrayList<>();
    List<DataBerita> listNewsTemp = new ArrayList<>();

    View rootview;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    BeritaAdapter myAdapter;
    Boolean loadMore = false;

    int jumlahItem = 0;
    int visibleItem = 8;
    int page = 1;
    int jumlahPage = 1;

    //BigNews
    ImageView newsImage;
    FontAppCompatTextView newsTitle,newsShortDesc,newsDate;
    LinearLayout headNews;
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (rootview == null)
        {
            rootview = inflater.inflate(R.layout.fragment_berita, container, false);

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
            Bundle params = new Bundle();
            params.putString("View_Page", "User view berita page");
            mFirebaseAnalytics.logEvent("Activity_Berita_ViewListBerita", params);

            newsImage = rootview.findViewById(R.id.image);
            newsTitle = rootview.findViewById(R.id.title);
            newsShortDesc = rootview.findViewById(R.id.shortDesc);
            newsDate = rootview.findViewById(R.id.date);
            headNews = rootview.findViewById(R.id.head);
            headNews.setOnClickListener(this);

            /*swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.refresh);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    swipeRefreshLayout.setRefreshing(true);
                    getNewsData();

                }
            });
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                    getNewsData();
                }
            });*/

            recyclerView = rootview.findViewById(R.id.rv_berita);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            myAdapter = new BeritaAdapter(getActivity(),recyclerView,listNewsTemp);
            /*myAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    try {
                        loadMore = true;
                        listNewsTemp.add(null);
                        myAdapter.notifyItemInserted(listNewsTemp.size() - 1);

                        //Load more data for reyclerview
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                //Remove loading item
                                try {
                                    listNewsTemp.remove(listNewsTemp.size() - 1);
                                    myAdapter.notifyItemRemoved(listNewsTemp.size());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                int start = page * visibleItem;
                                int end = page * visibleItem + visibleItem;

                                if(page <= jumlahPage)
                                {
                                    for (int i=start ; i<end ; i++)
                                    {
                                        if(i < jumlahItem)
                                        {
                                            DataBerita dataBerita = new DataBerita( listNewsAllData.get(i).get_whats_new_title(), listNewsAllData.get(i).get_whats_new_title(),
                                                    listNewsAllData.get(i).get_whats_new_short_desc(), listNewsAllData.get(i).get_whats_new_desc(), listNewsAllData.get(i).get_whats_new_image(),
                                                    listNewsAllData.get(i).get_whats_new_active_status(), listNewsAllData.get(i).get_whats_new_meta_description(),
                                                    listNewsAllData.get(i).get_whats_new_meta_keywords(), listNewsAllData.get(i).get_whats_new_hits(), listNewsAllData.get(i).get_whats_new_ticket(),
                                                    listNewsAllData.get(i).get_whats_new_create_date(), listNewsAllData.get(i).get_whats_new_create_by(), listNewsAllData.get(i).get_whats_new_update_date(),
                                                    listNewsAllData.get(i).get_whats_new_update_by(), listNewsAllData.get(i).get_dateHuman());
                                            listNewsTemp.add(dataBerita);
                                        }
                                    }

                                    page = page + 1;
                                }

                                myAdapter.notifyDataSetChanged();
                                myAdapter.setLoaded();
                                loadMore = false;


                            }
                        }, 2000);



                    } catch (Exception e) {

                    }
                }
            });*/

        }
        getNewsData();
        return rootview;
    }


    public void getNewsData()
    {
        /*if(loadMore == false)
        {*/
            listNewsAllData.clear();
            listNewsTemp.clear();
        /*}*/
        
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseGetBerita> userCall = service.getNews();
        userCall.enqueue(new Callback<ResponseGetBerita>() {
            @Override
            public void onResponse(Call<ResponseGetBerita> call, Response<ResponseGetBerita> response) {
                //swipeRefreshLayout.setRefreshing(false);
                if(response.isSuccessful())
                {
                    try {
                        listNewsAllData = null;
                        listNewsAllData = response.body().getListNews();

                        setUpBigNews();

                        Log.e("jumlah Data",""+listNewsAllData.size());
                        if(listNewsAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            //for (int i = 1; i < visibleItem; i++)
                            for (int i = 1; i < listNewsAllData.size(); i++)
                            {
                                DataBerita dataBerita = new DataBerita( listNewsAllData.get(i).get_whats_new_title(), listNewsAllData.get(i).get_whats_new_title(),
                                        listNewsAllData.get(i).get_whats_new_short_desc(), listNewsAllData.get(i).get_whats_new_desc(), listNewsAllData.get(i).get_whats_new_image(),
                                        listNewsAllData.get(i).get_whats_new_active_status(), listNewsAllData.get(i).get_whats_new_meta_description(),
                                        listNewsAllData.get(i).get_whats_new_meta_keywords(), listNewsAllData.get(i).get_whats_new_hits(), listNewsAllData.get(i).get_whats_new_ticket(),
                                        listNewsAllData.get(i).get_whats_new_create_date(), listNewsAllData.get(i).get_whats_new_create_by(), listNewsAllData.get(i).get_whats_new_update_date(),
                                        listNewsAllData.get(i).get_whats_new_update_by(), listNewsAllData.get(i).get_dateHuman());
                                listNewsTemp.add(dataBerita);
                            }


                            /*if(loadMore == false)
                            {*/
                                jumlahItem = listNewsAllData.size();
                                jumlahPage = listNewsAllData.size() / visibleItem + 1;
                                recyclerView.setAdapter(myAdapter);
                            /*}*/

                            loadMore = false;
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }
                        
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getActivity(),"catch",Toast.LENGTH_SHORT).show();
                    }
                    
                    
                }
                else
                {
                    //Toast.makeText(getActivity(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetBerita> call, Throwable t) {
                //swipeRefreshLayout.setRefreshing(false);
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void setUpBigNews()
    {
        newsTitle.setText(""+listNewsAllData.get(0).get_whats_new_title());
        newsShortDesc.setText(""+listNewsAllData.get(0).get_whats_new_short_desc());
        newsDate.setText(""+listNewsAllData.get(0).get_dateHuman());

        Picasso.with(getActivity())
                .load("http://gtunes.co.id/" + listNewsAllData.get(0).get_whats_new_image())
                .noPlaceholder()
                .resize(810, 453)
                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(newsImage);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.head:
                if(listNewsAllData.size()<=0)
                {

                }
                else
                {
                    Intent intent = new Intent(getActivity(), BeritaDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("title",""+ listNewsAllData.get(0).get_whats_new_title());
                    intent.putExtra("date",""+ listNewsAllData.get(0).get_dateHuman());
                    intent.putExtra("desc",""+ listNewsAllData.get(0).get_whats_new_desc());
                    intent.putExtra("image",""+ listNewsAllData.get(0).get_whats_new_image());
                    startActivity(intent);
                }

                break;
        }
    }
}