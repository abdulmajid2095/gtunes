package gtunes.application.gtunes.ui.Profile;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import gtunes.application.gtunes.Database.Data_Profile;
import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.Utilities.ImageCircleTransform;
import gtunes.application.gtunes.model.ResponseLogin;
import gtunes.application.gtunes.model.ResponseUploadImage;
import gtunes.application.gtunes.model.SendEditProfile;
import gtunes.application.gtunes.model.SendRegister;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile extends AppCompatActivity implements View.OnClickListener{

    LinearLayout simpan;
    ImageView back,foto,editFoto;
    MaterialEditText editNama,editEmail,editTelepon;
    Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
    DialogPlus dialog;
    Uri uriImage;
    DialogPlus warningDialog;
    Boolean imageUpdated = false;
    ImageView deleteNama,deleteEmail,deleteTelepon;
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_edit_profile);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_Page", "User view edit profile page");
        mFirebaseAnalytics.logEvent("Activity_Profile_ViewEditPage", params);

        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        foto = findViewById(R.id.foto);
        foto.setOnClickListener(this);
        editFoto = findViewById(R.id.edit_foto);
        editFoto.setOnClickListener(this);
        editNama = findViewById(R.id.edit_nama);
        editEmail = findViewById(R.id.edit_email);
        editTelepon = findViewById(R.id.edit_telepon);
        simpan = findViewById(R.id.simpan);
        simpan.setOnClickListener(this);
        deleteNama = findViewById(R.id.deleteNama);
        deleteNama.setOnClickListener(this);
        deleteEmail = findViewById(R.id.deleteEmail);
        deleteEmail.setOnClickListener(this);
        deleteTelepon = findViewById(R.id.deleteTelepon);
        deleteTelepon.setOnClickListener(this);

        Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
        if(data_profile.avatar.equals("default"))
        {
            foto.setImageResource(R.drawable.profile);
        }
        else
        {
            try {
                File imgFile = new  File(""+data_profile.avatar);
                Picasso.with(getApplicationContext())
                        .load(imgFile)
                        .noPlaceholder()
                        .transform(new ImageCircleTransform())
                        .resize(300, 300)
                        .centerCrop()
                        .into(foto);
            } catch (Exception e) {
                e.printStackTrace();
                foto.setImageResource(R.drawable.profile);
            }
        }

        editNama.setText(""+data_profile.nama);
        /*editEmail.setText(""+data_profile.customer_email);
        editTelepon.setText(""+data_profile.customer_phone);*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.edit_foto:
                Bundle params = new Bundle();
                params.putString("Pick_Foto", "User pickFoto");
                mFirebaseAnalytics.logEvent("Activity_Profile_PickFoto", params);

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .setFixAspectRatio(true)
                        .start(this);
                break;
            case R.id.simpan:
                Bundle params2 = new Bundle();
                params2.putString("Simpan", "User press simpan");
                mFirebaseAnalytics.logEvent("Activity_Profile_PressSimpan", params2);

                cekForm(""+editNama.getText().toString(),""+editEmail.getText().toString(),""+editTelepon.getText().toString());
                break;

            case R.id.deleteNama:
                Bundle params3 = new Bundle();
                params3.putString("Delete", "User delete nama");
                mFirebaseAnalytics.logEvent("Activity_Profile_DeleteNama", params3);
                editNama.setText("");
                break;

            case R.id.deleteEmail:
                Bundle params4 = new Bundle();
                params4.putString("Delete", "User delete email");
                mFirebaseAnalytics.logEvent("Activity_Profile_DeleteEmail", params4);
                editEmail.setText("");
                break;

            case R.id.deleteTelepon:
                Bundle params5 = new Bundle();
                params5.putString("Delete", "User delete telepon");
                mFirebaseAnalytics.logEvent("Activity_Profile_DeleteTelepon", params5);
                editTelepon.setText("");
                break;

            case R.id.foto:
                Bundle params6 = new Bundle();
                params6.putString("Pick_Foto", "User pickFoto");
                mFirebaseAnalytics.logEvent("Activity_Profile_PickFoto", params6);
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .setFixAspectRatio(true)
                        .start(this);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriImage = result.getUri();
                //uploadFile();
                //File file = new File(uriImage.getPath());
                try {
                    //Bitmap bm = BitmapFactory.decodeResource(getResources(),R.drawable.mipimage);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),uriImage);
                    String pathku = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache";
                    File file = new File(pathku,"profile.png");
                    FileOutputStream outputStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,outputStream);
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
                data_profile.avatar = ""+uriImage.getPath();
                data_profile.save();
                Picasso.with(getApplicationContext())
                        .load(uriImage)
                        .noPlaceholder()
                        .transform(new ImageCircleTransform())
                        .resize(300, 300)
                        .centerCrop()
                        .into(foto);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    // Uploading Image/Video
    private void uploadFile() {
        loading();
        File file = new File(uriImage.getPath());
        String tokenBearer = "Bearer" + " " + data_profile.token;
        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("**/*//*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("avatar", file.getName(), requestBody);
       
        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseUploadImage> userCall = service.uploadImage(tokenBearer, fileToUpload);
        userCall.enqueue(new Callback<ResponseUploadImage>() {
            @Override
            public void onResponse(Call<ResponseUploadImage> call, Response<ResponseUploadImage> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    Data_Profile data = Data_Profile.findById(Data_Profile.class, 1L);
                    //data.customer_avatar = ""+response.body().get_dataData().get_customer_avatar();
                    data.save();
                    //Toast.makeText(getApplicationContext(), ""+response.body().get_dataData().get_customer_avatar(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Upload sukses", Toast.LENGTH_SHORT).show();
                    Picasso.with(getApplicationContext())
                            .load(uriImage)
                            .noPlaceholder()
                            .transform(new ImageCircleTransform())
                            .resize(300, 300)
                            .centerCrop()
                            .into(foto);
                    imageUpdated = true;
                    Bundle params = new Bundle();
                    params.putString("View_Page", "User success change photo");
                    mFirebaseAnalytics.logEvent("Activity_Profile_ChangePhotoOK", params);
                }
                else
                {
                    Bundle params = new Bundle();
                    params.putString("View_Page", "User success change photo");
                    mFirebaseAnalytics.logEvent("Activity_Profile_ChangePhotoFail", params);

                    JSONObject jObjError = null;
                    String message = "Failed";
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjError2 = null;
                        jObjError2 = new JSONObject(jObjError.getString("error"));
                        message = ""+jObjError2.getString("userMsg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    final String myMessage = message;
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            dialog_warning(""+myMessage);
                        }
                    };
                    handler.postDelayed(myRunnable,500);

                }

            }

            @Override
            public void onFailure(Call<ResponseUploadImage> call, Throwable t) {
                dialog.dismiss();
                Bundle params = new Bundle();
                params.putString("View_Page", "User success change photo");
                mFirebaseAnalytics.logEvent("Activity_Profile_ChangePhotoFail", params);
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        // do something
                        dialog_warning("Terjadi kesalahan. Cek koneksi internet Anda.");
                    }
                };
                handler.postDelayed(myRunnable,500);
            }
        });
    }

    public void cekForm(String nama, String email, String handphone)
    {
        Boolean namaKosong = true, emailKosong = true, handphoneKosong = true;

        //Pengecekan Form Nama
        if(nama.equals("") || nama == null)
        {
            namaKosong = true;
            editNama.setError("Nama tidak boleh kosong");
        }
        else
        {
            namaKosong = false;
            editNama.setError(null);
        }

        /*//Pengecekan Form Email
        if(email.equals("") || email == null)
        {
            emailKosong = true;
            editEmail.setError("Email tidak boleh kosong");
        }
        else
        {
            emailKosong = false;
            editEmail.setError(null);
        }

        //Pengecekan Form Telephone
        if(handphone.equals("") || handphone == null)
        {
            handphoneKosong = true;
            editTelepon.setError("Nomor Telepon tidak boleh kosong");
        }
        else
        {
            handphoneKosong = false;
            editTelepon.setError(null);
        }*/

        if(namaKosong == false /*&& emailKosong == false && handphoneKosong == false*/)
        {
            //updateUser(nama,email,handphone);
            Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
            data_profile.nama = ""+editNama.getText().toString();
            data_profile.save();
            Intent intent = new Intent();
            setResult(1,intent);
            finish();
        }
    }

    public void updateUser(String nama, String email, String handphone)
    {
        loading();
        String tokenBearer = "Bearer" + " " + data_profile.token;
        SendEditProfile sendEditProfile = new SendEditProfile(""+email,""+nama,""+handphone);
        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseLogin> userCall = service.editProfile(tokenBearer,sendEditProfile);
        userCall.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if(response.isSuccessful()){
                    Bundle params = new Bundle();
                    params.putString("Edit", "User success edit");
                    mFirebaseAnalytics.logEvent("Activity_Profile_EditSuccess", params);

                    dialog.dismiss();
                    Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
                    /*data_profile.customer_name = ""+nama;
                    data_profile.customer_email = ""+email;
                    data_profile.customer_phone = ""+handphone;*/
                    data_profile.save();
                    Toast.makeText(getApplicationContext(),"Profil Diperbarui",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    setResult(1,intent);
                    finish();
                } else{
                    Bundle params = new Bundle();
                    params.putString("Edit", "User failed edit");
                    mFirebaseAnalytics.logEvent("Activity_Profile_EditFailed", params);
                    dialog.dismiss();
                    JSONObject jObjError = null;
                    String message = "Failed";
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjError2 = null;
                        jObjError2 = new JSONObject(jObjError.getString("error"));
                        message = ""+jObjError2.getString("userMsg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    final String myMessage = message;
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            dialog_warning(""+myMessage);
                        }
                    };
                    handler.postDelayed(myRunnable,500);
                }

            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                Bundle params = new Bundle();
                params.putString("Edit", "User failed edit");
                mFirebaseAnalytics.logEvent("Activity_Profile_EditFailed", params);
                dialog.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        // do something
                        dialog_warning("Terjadi kesalahan. Cek koneksi internet Anda.");
                    }
                };
                handler.postDelayed(myRunnable,500);

            }
        });
    }

    public void loading()
    {
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_loading))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = dialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        textMessage.setText("Updating...");
        dialog.show();
    }

    public void dialog_warning(String message)
    {
        warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();

    }

    @Override
    public void onBackPressed() {
        if(imageUpdated == true)
        {
            Intent intent = new Intent();
            setResult(1,intent);
            finish();
        }
        else
        {
            Intent intent = new Intent();
            setResult(0,intent);
            finish();
        }

    }
}
