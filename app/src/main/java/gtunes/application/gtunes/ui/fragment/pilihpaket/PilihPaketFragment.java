package gtunes.application.gtunes.ui.fragment.pilihpaket;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.adapter.PaketAdapter;
import gtunes.application.gtunes.model.DataPaket;
import gtunes.application.gtunes.model.Paket;
import gtunes.application.gtunes.model.ResponseGetPaket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PilihPaketFragment extends Fragment {

    List<DataPaket> listPaketAllData = new ArrayList<>();
    List<DataPaket> listPaketTemp = new ArrayList<>();
    RecyclerView recyclerView;
    PaketAdapter myAdapter;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pilih_paket, container, false);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle params = new Bundle();
        params.putString("View_Page", "User view paket page");
        mFirebaseAnalytics.logEvent("Activity_Paket_ViewListPaket", params);

        getAllPaket();
        recyclerView = v.findViewById(R.id.rv_paket);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myAdapter = new PaketAdapter(getActivity(),recyclerView,listPaketTemp);

        return v;
    }
    public void getAllPaket()
    {
        listPaketAllData.clear();
        listPaketTemp.clear();

        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseGetPaket> userCall = service.getPaket();
        userCall.enqueue(new Callback<ResponseGetPaket>() {
            @Override
            public void onResponse(Call<ResponseGetPaket> call, Response<ResponseGetPaket> response) {
                if(response.isSuccessful())
                {
                    try {
                        listPaketAllData = null;
                        listPaketAllData = response.body().getDataPaket();
                        //Toast.makeText(getApplicationContext(),""+listLaguAllData.get(0).get_artist_name().toString(),Toast.LENGTH_SHORT).show();
                        if(listPaketAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            for (int i = 0; i < listPaketAllData.size(); i++)
                            {
                                DataPaket dataPaket = new DataPaket( listPaketAllData.get(i).get_paket_id(),
                                        listPaketAllData.get(i).get_paket_code(),
                                        listPaketAllData.get(i).get_paket_tarif(),
                                        listPaketAllData.get(i).get_paket_hari()

                                );
                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                                listPaketTemp.add(dataPaket);
                            }



                            recyclerView.setAdapter(myAdapter);
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getApplicationContext(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetPaket> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
