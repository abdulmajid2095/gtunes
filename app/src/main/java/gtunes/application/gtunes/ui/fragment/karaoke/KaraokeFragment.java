package gtunes.application.gtunes.ui.fragment.karaoke;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;

public class KaraokeFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    ImageView search;
    MaterialEditText editSearch;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_karaoke, container, false);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle params = new Bundle();
        params.putString("View_Page", "User view karaoke page");
        mFirebaseAnalytics.logEvent("Activity_Karaoke_ViewListSong", params);

        viewPager = (ViewPager)v.findViewById(R.id.htab_viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout)v.findViewById(R.id.htab_tabs);
        tabLayout.setupWithViewPager(viewPager);

        editSearch = v.findViewById(R.id.editSearch);
        editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if(viewPager.getCurrentItem() == 0)
                    {
                        //Toast.makeText(getActivity(),"A1",Toast.LENGTH_SHORT).show();
                        Intent local = new Intent();
                        local.setAction("service.to.activity.transfer");
                        local.putExtra("type","all");
                        local.putExtra("search",""+editSearch.getText().toString());
                        getActivity().sendBroadcast(local);
                        Bundle params = new Bundle();
                        params.putString("Search_Song", "User search music");
                        mFirebaseAnalytics.logEvent("Activity_Karaoke_SearchLagu", params);
                    }
                    else
                    {
                        //Toast.makeText(getActivity(),"A2",Toast.LENGTH_SHORT).show();
                        Intent local = new Intent();
                        local.setAction("service.to.activity.transfer");
                        local.putExtra("type","free");
                        local.putExtra("search",""+editSearch.getText().toString());
                        getActivity().sendBroadcast(local);
                        Bundle params = new Bundle();
                        params.putString("Search_Song", "User search music");
                        mFirebaseAnalytics.logEvent("Activity_Karaoke_SearchLagu", params);
                    }
                    return true;
                }
                return false;
            }
        });
        search = v.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem() == 0)
                {
                    //Toast.makeText(getActivity(),"B1",Toast.LENGTH_SHORT).show();
                    Intent local = new Intent();
                    local.setAction("service.to.activity.transfer");
                    local.putExtra("type","all");
                    local.putExtra("search",""+editSearch.getText().toString());
                    getActivity().sendBroadcast(local);
                }
                else
                {
                    //Toast.makeText(getActivity(),"B2",Toast.LENGTH_SHORT).show();
                    Intent local = new Intent();
                    local.setAction("service.to.activity.transfer");
                    local.putExtra("type","free");
                    local.putExtra("search",""+editSearch.getText().toString());
                    getActivity().sendBroadcast(local);
                }

            }
        });
        return v;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new KaraokeFragmentSemua(), "Semua Lagu");
        adapter.addFragment(new KaraokeFragmentFree(), "Gratis");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
