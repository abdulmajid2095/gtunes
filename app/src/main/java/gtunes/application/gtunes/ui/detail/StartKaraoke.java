package gtunes.application.gtunes.ui.detail;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.opengl.EGL14;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.SoundEffectConstants;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.github.republicofgavin.pauseresumeaudiorecorder.PauseResumeAudioRecorder;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lauzy.freedom.library.Lrc;
import com.lauzy.freedom.library.LrcHelper;
import com.lauzy.freedom.library.LrcView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.pixplicity.fontview.FontAppCompatTextView;
import com.squareup.picasso.Picasso;
import com.vistrav.ask.Ask;
import com.wonderkiln.camerakit.CameraListener;
import com.wonderkiln.camerakit.CameraView;
import com.wonderkiln.camerakit.Facing;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import gtunes.application.gtunes.Database.Data_Profile;
import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.Utilities.AMRAudioRecorder;
import gtunes.application.gtunes.Utilities.CameraPreview;
import gtunes.application.gtunes.Utilities.ImageCircleTransform;
import gtunes.application.gtunes.Utilities.PreviewActivity;
import gtunes.application.gtunes.Utilities.ResultHolder;
import gtunes.application.gtunes.Utilities.SquareCameraPreview;
import gtunes.application.gtunes.Utilities.TextureMovieEncoder;
import gtunes.application.gtunes.Utilities.gles.FullFrameRect;
import gtunes.application.gtunes.Utilities.gles.Texture2dProgram;
import gtunes.application.gtunes.model.ResponseUploadImage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.wonderkiln.camerakit.CameraKit;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class StartKaraoke extends AppCompatActivity implements View.OnClickListener, SurfaceTexture.OnFrameAvailableListener {

    Boolean firstRecord = false;
    Boolean isResuming = false;
    CountDownTimer countDownTimer;
    private boolean isPlaying = false;
    private SimpleExoPlayer exoPlayer;
    private static final String TAG = "EXO_KARAOKE";
    private  ExoPlayer.EventListener eventListener = new ExoPlayer.EventListener() {
        @Override
        public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
            Log.i(TAG,"onTimelineChanged");
            //Toast.makeText(getApplicationContext(),"EXO TIMELINE CHANGE",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            Log.i(TAG,"onTracksChanged");
            //Toast.makeText(getApplicationContext(),"EXO TRACK CHANGE",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
            Log.i(TAG,"onLoadingChanged");
            //Toast.makeText(getApplicationContext(),"EXO LOADING END",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG,"onPlayerStateChanged: playWhenReady = "+String.valueOf(playWhenReady)
                    +" playbackState = "+playbackState);
            switch (playbackState){
                case ExoPlayer.STATE_ENDED:
                    //Toast.makeText(getApplicationContext(),"EXO END",Toast.LENGTH_SHORT).show();
                    Bundle params = new Bundle();
                    params.putString("Finish_Karaoke", "User finish karaoke");
                    mFirebaseAnalytics.logEvent("Activity_Karaoke_FinishKaraoke", params);
                    setPlayPause(false);
                    exoPlayer.seekTo(0);
                    if(camActive == true)
                    {
                        mGLView.queueEvent(new Runnable() {
                            @Override public void run() {
                                mRenderer.changeRecordingState(false);
                            }
                        });
                        amrAudioRecorder.stop();
                    }
                    else
                    {
                        amrAudioRecorder.stop();
                    }
                    musicIsPlaying = false;
                    karaokeFinish = true;
                    goToPreview();
                    break;
                case ExoPlayer.STATE_READY:
                    /*Toast.makeText(getApplicationContext(),"EXO START",Toast.LENGTH_SHORT).show();*/
                    try {
                        layLoading.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mediaPlayerDuration = exoPlayer.getDuration();
                    //Toast.makeText(getApplicationContext(),"DURATION "+mediaPlayerDuration,Toast.LENGTH_SHORT).show();
                    countDownTimer = new CountDownTimer(300000, 1000) { // adjust the milli seconds here

                        public void onTick(long millisUntilFinished)
                        {
                            //Toast.makeText(getApplicationContext(),"DURATION "+millisUntilFinished,Toast.LENGTH_SHORT).show();
                            long myUpdate = mediaPlayerDuration - exoPlayer.getCurrentPosition();
                            mLrcView.updateTime(exoPlayer.getCurrentPosition());
                            timerMinute.setText(""+String.format("%d", TimeUnit.MILLISECONDS.toMinutes( myUpdate)));
                            timerSecond.setText(""+String.format("%d", TimeUnit.MILLISECONDS.toSeconds(myUpdate) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(myUpdate))));
                        }

                        public void onFinish()
                        {
                            countDownTimer.start();
                        }
                    };
                    countDownTimer.start();

                    if(firstRecord == true)
                    {
                        firstRecord = false;
                        if(camActive == true)
                        {
                            mGLView.queueEvent(new Runnable() {
                                @Override public void run() {
                                    mRenderer.changeRecordingState(true);
                                }
                            });
                            amrAudioRecorder.start();
                        }
                        else
                        {
                            amrAudioRecorder.start();
                        }
                    }

                    if(isResuming == true)
                    {
                        isResuming = false;
                        if(camActive == true)
                        {
                            mGLView.queueEvent(new Runnable() {
                                @Override public void run() {
                                    // notify the renderer that we want to change the encoder's state
                                    mRenderer.changePauseState(false);
                                }
                            });
                            amrAudioRecorder.resume();
                        }
                        else
                        {
                            amrAudioRecorder.resume();
                        }
                    }


                    break;
                case ExoPlayer.STATE_BUFFERING:


                    break;
                case ExoPlayer.STATE_IDLE:
                    //Toast.makeText(getApplicationContext(),"EXO IDDLE",Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            Log.i(TAG,"onPlaybackError: "+error.getMessage());
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
            Log.i(TAG,"onPositionDiscontinuity");
        }
    };

    ImageView back, preview_clip;
    Boolean camActive = false;
    private Context myContext;

    FontAppCompatTextView title,band;
    LinearLayout layLoading;
    RelativeLayout laySwitch,layImage,layPreview;
    ImageView imageSwitch,start,switchCamera;
    int widthScreen;

    //CAMERA
    //private static final String TAG = StartKaraoke.class.getSimpleName();
    // Camera filters; must match up with cameraFilterNames in strings.xml
    static final int FILTER_NONE = 0;
    static final int FILTER_BLACK_WHITE = 1;
    static final int FILTER_BLUR = 2;
    static final int FILTER_SHARPEN = 3;
    static final int FILTER_EDGE_DETECT = 4;
    static final int FILTER_EMBOSS = 5;
    static final int PREVIEW_SIZE_MAX_WIDTH = 640;
    private SquareCameraPreview mGLView;
    private CameraSurfaceRenderer mRenderer;
    private static TextureMovieEncoder sVideoEncoder = new TextureMovieEncoder();
    private StartKaraoke.CameraHandler mCameraHandler;
    File outputFile;
    private Camera mCamera;
    private static final boolean VERBOSE = false;
    private int mCameraPreviewWidth, mCameraPreviewHeight;
    int myCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
    boolean isFront = true;

    private LrcView mLrcView;
    long mediaPlayerDuration;

    LinearLayout layToolbar,layLyric,layPlay,layStart;
    ImageView playPause;
    FontAppCompatTextView title2,band2,timerMinute,timerSecond;
    Boolean musicIsPlaying = false;

    //AUDIO RECORDER
    //String outputAudioFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "/gtunesCache.wav";
    String outputAudioFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "";
    AMRAudioRecorder amrAudioRecorder;
    //MediaRecorder myAudioRecorder;
    //PauseResumeAudioRecorder newAudioRecorder;

    String downloaderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "";

    DialogPlus warningDialog, dialog;
    String musicId = "";
    String myUploadId = "";
    Boolean downloadComplete = false;
    Boolean karaokeFinish = false;
    Boolean exoFirstTime = true;
    FirebaseAnalytics mFirebaseAnalytics;
    String tryme = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_start_karaoke);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_Page", "User view karaoke page");
        mFirebaseAnalytics.logEvent("Activity_Karaoke_ViewPage", params);

        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        preview_clip = findViewById(R.id.preview_clip);

        myContext = this;
        check_permission();

        tryme = ""+getIntent().getStringExtra("tryme");
        if(tryme.equals("yes"))
        {
            limiterTryMe();
        }
        //Toast.makeText(getApplicationContext(),""+tryme,Toast.LENGTH_SHORT).show();

        musicId = ""+getIntent().getStringExtra("musicId");

        layLoading = findViewById(R.id.layLoading);
        title = findViewById(R.id.title);
        band = findViewById(R.id.band);

        laySwitch = findViewById(R.id.laySwitch);
        laySwitch.setOnClickListener(this);
        imageSwitch = findViewById(R.id.imageSwitch);
        start = findViewById(R.id.start);
        start.setOnClickListener(this);
        switchCamera = findViewById(R.id.switchCamera);
        switchCamera.setOnClickListener(this);
        layImage = findViewById(R.id.layImage);
        layPreview = findViewById(R.id.layPreview);
        layToolbar = findViewById(R.id.toolbar);
        layLyric = findViewById(R.id.layLyric);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        widthScreen = displayMetrics.widthPixels;

        layPlay = findViewById(R.id.layPlay);
        layStart = findViewById(R.id.layStart);
        title2 = findViewById(R.id.title2);
        band2 = findViewById(R.id.band2);
        timerMinute = findViewById(R.id.timerMinute);
        timerSecond = findViewById(R.id.timerSecond);
        playPause = findViewById(R.id.playPause);
        playPause.setOnClickListener(this);

        title2.setText(""+getIntent().getStringExtra("title"));
        band2.setText(""+getIntent().getStringExtra("band"));
        title.setText(""+getIntent().getStringExtra("title"));
        band.setText(""+getIntent().getStringExtra("band"));

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        prepareExoPlayerFromURL(Uri.parse(""+getIntent().getStringExtra("url_karaoke")));

        Picasso.with(getApplicationContext())
                .load(""+getIntent().getStringExtra("gambar"))
                .noPlaceholder()
                .resize(600, 600)
                .centerCrop()
                .into(preview_clip, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        layLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        layLoading.setVisibility(View.GONE);
                    }
                });

        outputFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache","test.mp4");
        mCameraHandler = new StartKaraoke.CameraHandler(this);
        mGLView = (SquareCameraPreview) findViewById(R.id.camera_preview_view);
        mGLView.setEGLContextClientVersion(2);     // select GLES 2.0
        mRenderer = new CameraSurfaceRenderer(mCameraHandler, sVideoEncoder, outputFile);
        mGLView.setRenderer(mRenderer);
        mGLView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        PRDownloader.initialize(getApplicationContext());
        downloadAudio();
    }

    @Override
    protected void onResume() {
        super.onResume();
        openCamera(myCameraId);
        mGLView.onResume();
        mGLView.queueEvent(new Runnable() {
            @Override public void run() {
                mRenderer.setCameraPreviewSize(mCameraPreviewWidth, mCameraPreviewHeight);
            }
        });
        Log.d(TAG, "onResume complete: " + this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
        mGLView.queueEvent(new Runnable() {
            @Override public void run() {
                // Tell the renderer that it's about to be paused so it can clean up.
                //TODO Stop MediaCodec? users may click home button and never come back again
                mRenderer.notifyPausing();
            }
        });
        mGLView.onPause();
        Log.d(TAG, "onPause complete");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCameraHandler.invalidateHandler();     // paranoia
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back:
                onBackPressed();
                break;

            case R.id.laySwitch:
                float dip = 30f;
                Resources r = getResources();
                float px = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        dip,
                        r.getDisplayMetrics()
                );

                if(camActive == false)
                {
                    //start_camera();
                    showCamera();
                    camActive = true;
                    Animation mAnimation = new TranslateAnimation(0, px, 0, 0);
                    mAnimation.setDuration(300);
                    mAnimation.setFillAfter(true);
                    mAnimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            laySwitch.setBackgroundResource(R.drawable.switch_background);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    imageSwitch.startAnimation(mAnimation);
                }
                else
                {
                    //stop_camera();
                    hideCamera();
                    camActive = false;
                    Animation mAnimation = new TranslateAnimation(px, 0, 0, 0);
                    mAnimation.setDuration(300);
                    mAnimation.setFillAfter(true);
                    mAnimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            laySwitch.setBackgroundResource(R.drawable.switch_background_off);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    imageSwitch.startAnimation(mAnimation);
                }
                break;

            case R.id.start:
                dialog_preparation();
                break;
            case R.id.switchCamera:
                if(isFront == true)
                {
                    myCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                    switchCamera();
                }
                else
                {
                    myCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                    switchCamera();
                }
                isFront = !isFront;
                break;
            case R.id.playPause:
                if(musicIsPlaying == true)
                {
                    if(camActive == true)
                    {
                        mGLView.queueEvent(new Runnable() {
                            @Override public void run() {
                                // notify the renderer that we want to change the encoder's state
                                mRenderer.changePauseState(true);
                            }
                        });
                        amrAudioRecorder.pause();
                    }
                    else
                    {
                        amrAudioRecorder.pause();
                    }
                    setPlayPause(false);
                    Bundle params = new Bundle();
                    params.putString("Pause_Karaoke", "User pause karaoke");
                    mFirebaseAnalytics.logEvent("Activity_Karaoke_PauseKaraoke", params);
                    dialog_option();
                }
                else
                {
                    Bundle params = new Bundle();
                    params.putString("Start_Karaoke", "User start karaoke");
                    mFirebaseAnalytics.logEvent("Activity_Karaoke_StartKaraoke", params);

                    if(camActive == true)
                    {
                        firstRecord = true;
                        setPlayPause(true);
                    }
                    else
                    {
                        firstRecord = true;
                        setPlayPause(true);
                    }
                    karaokeFinish = false;
                }
                break;
        }
    }

    public void check_permission()
    {
        Ask.on(this)
                .id(1) // in case you are invoking multiple time Ask from same activity or fragment
                .forPermissions(Manifest.permission.CAMERA
                        , Manifest.permission.READ_EXTERNAL_STORAGE
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE)
                /*.withRationales("Location permission need for map to work properly",
                        "In order to save file you will need to grant storage permission") *///optional
                .go();
    }

    public void showCamera()
    {
        //Toast.makeText(getApplicationContext(),"SHOW",Toast.LENGTH_SHORT).show();
        Animation mAnimation2 = new TranslateAnimation(0, widthScreen, 0, 0);
        mAnimation2.setDuration(300);
        mAnimation2.setFillAfter(true);
        layImage.startAnimation(mAnimation2);
    }

    public void hideCamera()
    {
        //Toast.makeText(getApplicationContext(),"HIDE",Toast.LENGTH_SHORT).show();
        Animation mAnimation2 = new TranslateAnimation(widthScreen, 0, 0, 0);
        mAnimation2.setDuration(300);
        mAnimation2.setFillAfter(true);
        layImage.startAnimation(mAnimation2);
    }

    public void dialog_preparation()
    {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_preparation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);

        LinearLayout ok = dialog.findViewById(R.id.oke);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                layToolbar.setVisibility(View.GONE);
                layLyric.setVisibility(View.VISIBLE);
                layPlay.setVisibility(View.VISIBLE);
                layStart.setVisibility(View.GONE);
                if(camActive == true)
                {
                    Bundle params = new Bundle();
                    params.putString("Video_Karaoke", "User select video karaoke");
                    mFirebaseAnalytics.logEvent("Activity_Karaoke_VideoKaraoke", params);

                    //initVideoRecorder();
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            switchCamera();
                        }
                    };
                    handler.postDelayed(myRunnable,50);
                    initAudioRecorder();
                }
                else
                {
                    Bundle params = new Bundle();
                    params.putString("Audio_Karaoke", "User select audio karaoke");
                    mFirebaseAnalytics.logEvent("Activity_Karaoke_AudioKaraoke", params);
                    initAudioRecorder();
                }
            }
        });

        dialog.show();
    }

    public void dialog_option()
    {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_option_karaoke);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        LinearLayout lanjutkan = dialog.findViewById(R.id.opsi1);
        lanjutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle params = new Bundle();
                params.putString("Resume_Karaoke", "User resume karaoke");
                mFirebaseAnalytics.logEvent("Activity_Karaoke_ResumeKaraoke", params);

                isResuming = true;
                musicIsPlaying = true;
                setPlayPause(true);
                dialog.dismiss();
            }
        });

        LinearLayout awal = dialog.findViewById(R.id.opsi2);
        awal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle params = new Bundle();
                params.putString("Restart_Karaoke", "User restart karaoke");
                mFirebaseAnalytics.logEvent("Activity_Karaoke_RestartKaraoke", params);
                if(camActive == false)
                {
                    amrAudioRecorder.stop();
                    initAudioRecorder();
                }
                else
                {
                    mGLView.queueEvent(new Runnable() {
                        @Override public void run() {
                            mRenderer.changeRecordingState(false);
                        }
                    });
                    amrAudioRecorder.stop();
                    initAudioRecorder();
                }
                dialog.dismiss();
                exoPlayer.seekTo(0);
                musicIsPlaying = true;
                playPause.setBackgroundResource(R.drawable.ic_pause_white);
                setPlayPause(true);
            }
        });

        LinearLayout baru = dialog.findViewById(R.id.opsi3);
        baru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle params = new Bundle();
                params.putString("NewSong", "User choose new song");
                mFirebaseAnalytics.logEvent("Activity_Karaoke_NewSong", params);
                dialog.dismiss();
                releaseMusic();
                onBackPressed();
            }
        });

        LinearLayout simpan = dialog.findViewById(R.id.opsi4);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle params = new Bundle();
                params.putString("Finish_Karaoke", "User finish karaoke");
                mFirebaseAnalytics.logEvent("Activity_Karaoke_FinishKaraoke", params);
                dialog.dismiss();
                karaokeFinish = true;
                if(camActive == false)
                {
                    amrAudioRecorder.stop();
                }
                else
                {
                    mGLView.queueEvent(new Runnable() {
                        @Override public void run() {
                            mRenderer.changeRecordingState(false);
                        }
                    });
                    amrAudioRecorder.stop();
                }
                releaseMusic();
                goToPreview();
            }
        });

        dialog.show();
    }

    private void prepareExoPlayerFromURL(Uri uri){

        TrackSelector trackSelector = new DefaultTrackSelector();
        LoadControl loadControl = new DefaultLoadControl();
        exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "exoplayer2example"), null);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        MediaSource audioSource = new ExtractorMediaSource(uri, dataSourceFactory, extractorsFactory, null, null);
        exoPlayer.addListener(eventListener);
        exoPlayer.prepare(audioSource);

        URL url = null;
        File lyricFile = null;
        try {
            url = new URL(""+getIntent().getStringExtra("lirik"));
            String tDir = System.getProperty("java.io.tmpdir");
            String path = tDir + "tmp" + ".pdf";
            lyricFile = new File(path);
            lyricFile.deleteOnExit();
            FileUtils.copyURLToFile(url, lyricFile);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(),"Masuk catch 1",Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            //Toast.makeText(getApplicationContext(),"Masuk catch 2",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


        List<Lrc> lrcs = LrcHelper.parseLrcFromFile(lyricFile);
        mLrcView = findViewById(R.id.lrc_view);
        mLrcView.setLrcData(lrcs);
        mLrcView.setEmptyContent("no data");
        mLrcView.setOnPlayIndicatorLineListener(new LrcView.OnPlayIndicatorLineListener() {
            @Override
            public void onPlay(long time, String content) {

            }
        });

    }

    private void setPlayPause(boolean play){
        isPlaying = play;
        exoPlayer.setPlayWhenReady(play);
        if(!isPlaying){
            musicIsPlaying = false;
            playPause.setBackgroundResource(R.drawable.ic_play_white);
        }else{
            musicIsPlaying = true;
            playPause.setBackgroundResource(R.drawable.ic_pause_white);
        }
    }

    private String stringForTime(int timeMs) {
        StringBuilder mFormatBuilder;
        Formatter mFormatter;
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        int totalSeconds =  timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours   = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    @Override
    public void onBackPressed() {
        if (musicIsPlaying == true)
        {
            setPlayPause(false);
            musicIsPlaying = false;
            playPause.setBackgroundResource(R.drawable.ic_play_white);
            if(camActive == true)
            {

            }
            else
            {

            }
            dialog_option();
        }
        else
        {
            if(countDownTimer == null)
            {

            }
            else
            {
                countDownTimer.cancel();
            }
            super.onBackPressed();
        }
    }

    public void releaseMusic()
    {
        exoPlayer.stop(true);
        exoPlayer.stop();
        exoPlayer.release();
    }

    public void initAudioRecorder()
    {
        amrAudioRecorder = new AMRAudioRecorder(outputAudioFile);
        /*newAudioRecorder=new PauseResumeAudioRecorder();
        newAudioRecorder.setAudioFile(outputAudioFile);*/

        /*myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        myAudioRecorder.setAudioEncodingBitRate(128000);
        myAudioRecorder.setAudioSamplingRate(44100);
        myAudioRecorder.setOutputFile(outputAudioFile);*/
    }

    public void downloadAudio()
    {
        int downloadId = PRDownloader.download(""+getIntent().getStringExtra("url_karaoke"), downloaderPath, "gtunesMusic.mp3")
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                        //Toast.makeText(getApplicationContext(),"START DOWNLOAD",Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                        Toast.makeText(getApplicationContext(),"PAUSE DOWNLOAD",Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        Toast.makeText(getApplicationContext(),"CANCEL DOWNLOAD",Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        Log.e("DONWLOAD","PROGRESS");
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                       downloadComplete = true;
                       if(karaokeFinish == true)
                       {
                           if(dialog == null)
                           {

                           }
                           else {
                               dialog.dismiss();
                           }
                           goToPreview2();
                       }
                    }
                    @Override
                    public void onError(Error error) {
                        //Toast.makeText(getApplicationContext(),"ERROR DOWNLOAD",Toast.LENGTH_SHORT).show();
                        downloadAudio();
                    }
                });
    }


    public void goToPreview()
    {
        //uploadFile();
        goToPreview2();
    }

    public void goToPreview2()
    {
        //downloadComplete = true;
        if(downloadComplete == true)
        {
            Intent intent = new Intent(StartKaraoke.this, KaraokePreview.class);
            intent.putExtra("karaoke",""+downloaderPath+"/gtunesMusic.mp3");
            intent.putExtra("camera",""+camActive);
            intent.putExtra("band",""+getIntent().getStringExtra("band"));
            intent.putExtra("title",""+getIntent().getStringExtra("title"));
            intent.putExtra("uploadId",""+myUploadId);
            intent.putExtra("audio",""+amrAudioRecorder.getAudioFilePath());
            intent.putExtra("video",""+Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache/test.mp4");
            startActivity(intent);
        }
        else
        {
            loading();
        }

    }

    // Uploading Image/Video
    private void uploadFile() {
        loading();
        File file;
        String fileTypeString = "";
        if(camActive == true)
        {
            file = ResultHolder.getVideo();
            fileTypeString = "video";
        }
        else
        {
            fileTypeString = "audio";
            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/GTunes/cache"+ "/gtunesCache.amr");
        }
        Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
        //Random
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        myUploadId = ts+""+output;
        Toast.makeText(getApplicationContext(),"Upload Id = "+myUploadId,Toast.LENGTH_SHORT).show();
        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("**/*//*"), file);
        MultipartBody.Part fileRecord = MultipartBody.Part.createFormData("file_record", file.getName(), requestBody);
        MultipartBody.Part customerId = MultipartBody.Part.createFormData("customer_code", ""+data_profile.user_id);
        MultipartBody.Part fileType = MultipartBody.Part.createFormData("file_type", ""+fileTypeString);
        MultipartBody.Part musicIdFile = MultipartBody.Part.createFormData("music_id", ""+musicId);
        MultipartBody.Part uploadId = MultipartBody.Part.createFormData("upload_id", ""+myUploadId);

        APIService service = ApiClientDevelopment.getClient().create(APIService.class);
        Call<ResponseUploadImage> userCall = service.uploadRecord(customerId,fileRecord,fileType,musicIdFile,uploadId);
        userCall.enqueue(new Callback<ResponseUploadImage>() {
            @Override
            public void onResponse(Call<ResponseUploadImage> call, Response<ResponseUploadImage> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    //Toast.makeText(getApplicationContext(), "Upload sukses", Toast.LENGTH_SHORT).show();
                    goToPreview2();
                }
                else
                {
                    JSONObject jObjError = null;
                    String message = "Failed";
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        JSONObject jObjError2 = null;
                        jObjError2 = new JSONObject(jObjError.getString("error"));
                        message = ""+jObjError2.getString("userMsg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    final String myMessage = message;
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            //dialog_warning(""+myMessage);
                        }
                    };
                    handler.postDelayed(myRunnable,500);
                    goToPreview2();
                }

            }

            @Override
            public void onFailure(Call<ResponseUploadImage> call, Throwable t) {
                dialog.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        // do something
                        //dialog_warning(""+t.getMessage());
                        goToPreview2();
                    }
                };
                handler.postDelayed(myRunnable,500);
            }
        });
    }

    public void loading()
    {
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_loading))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = dialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        textMessage.setText("Processing...");
        dialog.show();
    }

    public void dialog_warning(String message)
    {
        warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();

    }




    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        if (VERBOSE) Log.d(TAG, "ST onFrameAvailable");
        mGLView.requestRender();
    }

    static class CameraSurfaceRenderer implements GLSurfaceView.Renderer {
        private static final String TAG = StartKaraoke.CameraSurfaceRenderer.class.getSimpleName();
        private static final boolean VERBOSE = false;

        private static final int RECORDING_OFF = 0;
        private static final int RECORDING_ON = 1;
        private static final int RECORDING_RESUMED = 2;
        private static final int RECORDING_PAUSE = 3;

        private CameraHandler mCameraHandler;
        private TextureMovieEncoder mVideoEncoder;
        private File mOutputFile;

        private FullFrameRect mFullScreen;

        private final float[] mSTMatrix = new float[16];
        private int mTextureId;

        private SurfaceTexture mSurfaceTexture;
        private long mLastTimeStamp;
        private boolean mRecordingEnabled;
        private volatile boolean mPauseEnabled;
        private int mRecordingStatus;
        private int mFrameCount;

        // width/height of the incoming camera preview frames
        private boolean mIncomingSizeUpdated;
        private int mIncomingWidth;
        private int mIncomingHeight;

        private int mCurrentFilter;
        private int mNewFilter;
        private int mDisplayOrientation;


        /**
         * Constructs CameraSurfaceRenderer.
         * <p>
         * @param cameraHandler Handler for communicating with UI thread
         * @param movieEncoder video encoder object
         * @param outputFile output file for encoded video; forwarded to movieEncoder
         */
        public CameraSurfaceRenderer(StartKaraoke.CameraHandler cameraHandler,
                                     TextureMovieEncoder movieEncoder, File outputFile) {
            mCameraHandler = cameraHandler;
            mVideoEncoder = movieEncoder;
            mOutputFile = outputFile;

            mTextureId = -1;

            mRecordingStatus = -1;
            mRecordingEnabled = false;
            mPauseEnabled = false;
            mFrameCount = -1;
            mLastTimeStamp = 0;

            mIncomingSizeUpdated = false;
            mIncomingWidth = mIncomingHeight = -1;

            // We could preserve the old filter mode, but currently not bothering.
            mCurrentFilter = -1;
            mNewFilter = StartKaraoke.FILTER_NONE;
        }

        /**
         * Notifies the renderer thread that the activity is pausing.
         * <p>
         * For best results, call this *after* disabling Camera preview.
         */
        public void notifyPausing() {
            if (mSurfaceTexture != null) {
                Log.d(TAG, "renderer pausing -- releasing SurfaceTexture");
                mSurfaceTexture.release();
                mSurfaceTexture = null;
            }
            if (mFullScreen != null) {
                mFullScreen.release(false);     // assume the GLSurfaceView EGL context is about
                mFullScreen = null;             //  to be destroyed
            }
            mIncomingWidth = mIncomingHeight = -1;
        }

        /**
         * Notifies the renderer that we want to stop or start recording.
         */
        public void changeRecordingState(boolean isRecording) {
            Log.d(TAG, "changeRecordingState: was " + mRecordingEnabled + " now " + isRecording);
            mRecordingEnabled = isRecording;
        }

        public void changePauseState(boolean isPause) {
            mPauseEnabled = isPause;
        }

        public void setDisplayOrientation(int orientation) {
            mDisplayOrientation = orientation;
        }

        /**
         * Changes the filter that we're applying to the camera preview.
         */
        public void changeFilterMode(int filter) {
            mNewFilter = filter;
        }

        /**
         * Updates the filter program.
         */
        public void updateFilter() {
            Texture2dProgram.ProgramType programType;
            float[] kernel = null;
            float colorAdj = 0.0f;

            Log.d(TAG, "Updating filter to " + mNewFilter);
            switch (mNewFilter) {
                case StartKaraoke.FILTER_NONE:
                    programType = Texture2dProgram.ProgramType.TEXTURE_EXT;
                    break;
                case StartKaraoke.FILTER_BLACK_WHITE:
                    // (In a previous version the TEXTURE_EXT_BW variant was enabled by a flag called
                    // ROSE_COLORED_GLASSES, because the shader set the red channel to the B&W color
                    // and green/blue to zero.)
                    programType = Texture2dProgram.ProgramType.TEXTURE_EXT_BW;
                    break;
                case StartKaraoke.FILTER_BLUR:
                    programType = Texture2dProgram.ProgramType.TEXTURE_EXT_FILT;
                    kernel = new float[] {
                            1f/16f, 2f/16f, 1f/16f,
                            2f/16f, 4f/16f, 2f/16f,
                            1f/16f, 2f/16f, 1f/16f };
                    break;
                case StartKaraoke.FILTER_SHARPEN:
                    programType = Texture2dProgram.ProgramType.TEXTURE_EXT_FILT;
                    kernel = new float[] {
                            0f, -1f, 0f,
                            -1f, 5f, -1f,
                            0f, -1f, 0f };
                    break;
                case StartKaraoke.FILTER_EDGE_DETECT:
                    programType = Texture2dProgram.ProgramType.TEXTURE_EXT_FILT;
                    kernel = new float[] {
                            -1f, -1f, -1f,
                            -1f, 8f, -1f,
                            -1f, -1f, -1f };
                    break;
                case StartKaraoke.FILTER_EMBOSS:
                    programType = Texture2dProgram.ProgramType.TEXTURE_EXT_FILT;
                    kernel = new float[] {
                            2f, 0f, 0f,
                            0f, -1f, 0f,
                            0f, 0f, -1f };
                    colorAdj = 0.5f;
                    break;
                default:
                    throw new RuntimeException("Unknown filter mode " + mNewFilter);
            }

            // Do we need a whole new program?  (We want to avoid doing this if we don't have
            // too -- compiling a program could be expensive.)
            if (programType != mFullScreen.getProgram().getProgramType()) {
                mFullScreen.changeProgram(new Texture2dProgram(programType));
                // If we created a new program, we need to initialize the texture width/height.
                mIncomingSizeUpdated = true;
            }

            // Update the filter kernel (if any).
            if (kernel != null) {
                mFullScreen.getProgram().setKernel(kernel, colorAdj);
            }

            mCurrentFilter = mNewFilter;
        }

        /**
         * Records the size of the incoming camera preview frames.
         * <p>
         * It's not clear whether this is guaranteed to execute before or after onSurfaceCreated(),
         * so we assume it could go either way.  (Fortunately they both run on the same thread,
         * so we at least know that they won't execute concurrently.)
         */
        public void setCameraPreviewSize(int width, int height) {
            Log.d(TAG, "setCameraPreviewSize");
            mIncomingWidth = width;
            mIncomingHeight = height;
            mIncomingSizeUpdated = true;
        }

        @Override
        public void onSurfaceCreated(GL10 unused, EGLConfig config) {
            Log.d(TAG, "onSurfaceCreated");

            // We're starting up or coming back.  Either way we've got a new EGLContext that will
            // need to be shared with the video encoder, so figure out if a recording is already
            // in progress.
            mRecordingEnabled = mVideoEncoder.isRecording();
            if (mRecordingEnabled) {
                mRecordingStatus = RECORDING_RESUMED;
            } else {
                mRecordingStatus = RECORDING_OFF;
            }

            // Set up the texture blitter that will be used for on-screen display.  This
            // is *not* applied to the recording, because that uses a separate shader.
            mFullScreen = new FullFrameRect(
                    new Texture2dProgram(Texture2dProgram.ProgramType.TEXTURE_EXT));

            mTextureId = mFullScreen.createTextureObject();

            // Create a SurfaceTexture, with an external texture, in this EGL context.  We don't
            // have a Looper in this thread -- GLSurfaceView doesn't create one -- so the frame
            // available messages will arrive on the main thread.
            mSurfaceTexture = new SurfaceTexture(mTextureId);

            // Tell the UI thread to enable the camera preview.
            mCameraHandler.sendMessage(mCameraHandler.obtainMessage(
                    StartKaraoke.CameraHandler.MSG_SET_SURFACE_TEXTURE, mSurfaceTexture));
        }

        @Override
        public void onSurfaceChanged(GL10 unused, int width, int height) {
            Log.d(TAG, "onSurfaceChanged " + width + "x" + height);
        }

        @Override
        public void onDrawFrame(GL10 unused) {
            if (VERBOSE) Log.d(TAG, "onDrawFrame tex=" + mTextureId);
            boolean showBox = false;

            // Latch the latest frame.  If there isn't anything new, we'll just re-use whatever
            // was there before.
            mSurfaceTexture.updateTexImage();

            // If the recording state is changing, take care of it here.  Ideally we wouldn't
            // be doing all this in onDrawFrame(), but the EGLContext sharing with GLSurfaceView
            // makes it hard to do elsewhere.
            if (mRecordingEnabled) {
                switch (mRecordingStatus) {
                    case RECORDING_OFF:
                        Log.d(TAG, "START recording");
                        // start recording
                        if (mDisplayOrientation == 90 || mDisplayOrientation == 270) {
                            int temp = mIncomingWidth;
                            mIncomingWidth = mIncomingHeight;
                            mIncomingHeight = temp;
                        }
                        mVideoEncoder.startRecording(new TextureMovieEncoder.EncoderConfig(
                                mOutputFile, mIncomingWidth, mIncomingHeight, 1000000, EGL14.eglGetCurrentContext()));
                        mRecordingStatus = RECORDING_ON;
                        break;
                    case RECORDING_RESUMED:
                        Log.d(TAG, "RESUME recording");
                        mVideoEncoder.updateSharedContext(EGL14.eglGetCurrentContext());
                        mRecordingStatus = RECORDING_ON;
                        break;
                    case RECORDING_ON:
                        // yay
                        break;
                    default:
                        throw new RuntimeException("unknown status " + mRecordingStatus);
                }
            } else {
                switch (mRecordingStatus) {
                    case RECORDING_ON:
                    case RECORDING_RESUMED:
                        // stop recording
                        Log.d(TAG, "STOP recording");
                        mVideoEncoder.stopRecording();
                        mRecordingStatus = RECORDING_OFF;
                        mCameraHandler.sendMessage(mCameraHandler.obtainMessage(
                                StartKaraoke.CameraHandler.MSG_STOP_RECORDING));
                        break;
                    case RECORDING_OFF:
                        // yay
                        break;
                    default:
                        throw new RuntimeException("unknown status " + mRecordingStatus);
                }
            }

            // Set the video encoder's texture name.  We only need to do this once, but in the
            // current implementation it has to happen after the video encoder is started, so
            // we just do it here.
            //
            // TODO: be less lame.
            mVideoEncoder.setTextureId(mTextureId);

            // Tell the video encoder thread that a new frame is available.
            // This will be ignored if we're not actually recording.
            if (!mPauseEnabled) {
                mVideoEncoder.frameAvailable(mSurfaceTexture);
            } else {
                mVideoEncoder.pause();
            }

            if (mIncomingWidth <= 0 || mIncomingHeight <= 0) {
                // Texture size isn't set yet.  This is only used for the filters, but to be
                // safe we can just skip drawing while we wait for the various races to resolve.
                // (This seems to happen if you toggle the screen off/on with power button.)
                Log.i(TAG, "Drawing before incoming texture size set; skipping");
                return;
            }
            // Update the filter, if necessary.
            if (mCurrentFilter != mNewFilter) {
                updateFilter();
            }
            if (mIncomingSizeUpdated) {
                mFullScreen.getProgram().setTexSize(mIncomingWidth, mIncomingHeight);
                mIncomingSizeUpdated = false;
            }

            // Draw the video frame.
            mSurfaceTexture.getTransformMatrix(mSTMatrix);
            mFullScreen.drawFrame(mTextureId, mSTMatrix);

            // Draw a flashing box if we're recording.  This only appears on screen.
            showBox = (mRecordingStatus == RECORDING_ON);
            if (showBox && (++mFrameCount & 0x04) == 0) {
//            drawBox();
            }
        }

        /**
         * Draws a red box in the corner.
         */
        private void drawBox() {
            GLES20.glEnable(GLES20.GL_SCISSOR_TEST);
            GLES20.glScissor(0, 0, 100, 100);
            GLES20.glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
            GLES20.glDisable(GLES20.GL_SCISSOR_TEST);
        }
    }

    static class CameraHandler extends Handler {
        public static final int MSG_SET_SURFACE_TEXTURE = 0;
        public static final int MSG_STOP_RECORDING = 1;

        // Weak reference to the Activity; only access this from the UI thread.
        private WeakReference<StartKaraoke> mWeakActivity;

        public CameraHandler(StartKaraoke activity) {
            mWeakActivity = new WeakReference<StartKaraoke>(activity);
        }

        /**
         * Drop the reference to the activity.  Useful as a paranoid measure to ensure that
         * attempts to access a stale Activity through a handler are caught.
         */
        public void invalidateHandler() {
            mWeakActivity.clear();
        }

        @Override  // runs on UI thread
        public void handleMessage(Message inputMessage) {
            int what = inputMessage.what;
            Log.d(TAG, "CameraHandler [" + this + "]: what=" + what);

            StartKaraoke activity = mWeakActivity.get();
            if (activity == null) {
                Log.w(TAG, "CameraHandler.handleMessage: activity is null");
                return;
            }

            switch (what) {
                case MSG_SET_SURFACE_TEXTURE:
                    activity.handleSetSurfaceTexture((SurfaceTexture) inputMessage.obj);
                    break;
                case MSG_STOP_RECORDING:
                    /*Intent intent = new Intent();
                    intent.putExtra("video", Environment.getExternalStorageDirectory() + "/test.mp4");
                    intent.setClass(activity, PlayMovieSurfaceActivity.class);
                    activity.startActivity(intent);*/
                    break;
                default:
                    throw new RuntimeException("unknown msg " + what);
            }
        }
    }

    private void handleSetSurfaceTexture(SurfaceTexture st) {
        st.setOnFrameAvailableListener(this);
        try {
            mCamera.setPreviewTexture(st);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        mCamera.startPreview();
    }

    private void openCamera(int CameraId) {//int desiredWidth, int desiredHeight) {
        if (mCamera != null) {
            throw new RuntimeException("camera already initialized");
        }

        Camera.CameraInfo info = new Camera.CameraInfo();

        // Try to find a front-facing camera (e.g. for videoconferencing).
        int numCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numCameras; i++) {
            Camera.getCameraInfo(i, info);
            if (info.facing == CameraId) {
                mCamera = Camera.open(i);
                break;
            }
        }
        if (mCamera == null) {
            Log.d(TAG, "No front-facing camera found; opening default");
            mCamera = Camera.open();    // opens first back-facing camera
        }
        mGLView.setCamera(mCamera);
        if (mCamera == null) {
            throw new RuntimeException("Unable to open camera");
        }

        Camera.Parameters parms = mCamera.getParameters();

        Camera.Size bestPreviewSize = determineBestPreviewSize(parms);

        parms.setPreviewSize(bestPreviewSize.width, bestPreviewSize.height);

//        CameraUtils.choosePreviewSize(parms, desiredWidth, desiredHeight);

        // Give the camera a hint that we're recording video.  This can have a big
        // impact on frame rate.
        parms.setRecordingHint(true);
//        parms.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);

        // leave the frame rate set to default
        mCamera.setParameters(parms);
//        setCameraDisplayOrientation(Camera.CameraInfo.CAMERA_FACING_BACK, mCamera);
        determineDisplayOrientation();

        int[] fpsRange = new int[2];
        Camera.Size mCameraPreviewSize = parms.getPreviewSize();
        parms.getPreviewFpsRange(fpsRange);
        String previewFacts = mCameraPreviewSize.width + "x" + mCameraPreviewSize.height;
        if (fpsRange[0] == fpsRange[1]) {
            previewFacts += " @" + (fpsRange[0] / 1000.0) + "fps";
        } else {
            previewFacts += " @[" + (fpsRange[0] / 1000.0) +
                    " - " + (fpsRange[1] / 1000.0) + "] fps";
        }

        mCameraPreviewWidth = mCameraPreviewSize.width;
        mCameraPreviewHeight = mCameraPreviewSize.height;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mGLView.setCamera(null);
//            mCamera.setPreviewTexture(null);
            mCamera.release();
            mCamera = null;
            Log.d(TAG, "releaseCamera -- done");
        }
    }

    private Camera.Size determineBestPreviewSize(Camera.Parameters parameters) {
        return determineBestSize(parameters.getSupportedPreviewSizes(), PREVIEW_SIZE_MAX_WIDTH);
    }

    private Camera.Size determineBestSize(List<Camera.Size> sizes, int widthThreshold) {
        Camera.Size bestSize = null;
        Camera.Size size;
        int numOfSizes = sizes.size();
        for (int i = 0; i < numOfSizes; i++) {
            size = sizes.get(i);
            boolean isDesireRatio = (size.width / 4) == (size.height / 3);
            boolean isBetterSize = (bestSize == null) || size.width > bestSize.width;

            if (isDesireRatio && isBetterSize) {
                bestSize = size;
            }
        }

        if (bestSize == null) {
            Log.d(TAG, "cannot find the best camera size");
            return sizes.get(sizes.size() - 1);
        }

        return bestSize;
    }

    private void determineDisplayOrientation() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_FRONT, cameraInfo);

        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: {
                degrees = 0;
                break;
            }
            case Surface.ROTATION_90: {
                degrees = 90;
                break;
            }
            case Surface.ROTATION_180: {
                degrees = 180;
                break;
            }
            case Surface.ROTATION_270: {
                degrees = 270;
                break;
            }
        }

        final int displayOrientation;

        // Camera direction
        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // Orientation is angle of rotation when facing the camera for
            // the camera image to match the natural orientation of the device
            int tmpDisplayOrientation = (cameraInfo.orientation + degrees) % 360;
            displayOrientation = (360 - tmpDisplayOrientation) % 360;
        } else {
            displayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
        }

        mCamera.setDisplayOrientation(displayOrientation);
        mGLView.queueEvent(new Runnable() {
            @Override public void run() {
                // notify the renderer that we want to change the encoder's state
                mRenderer.setDisplayOrientation(displayOrientation);
            }
        });
    }

    public void switchCamera()
    {
        releaseCamera();
        mGLView.queueEvent(new Runnable() {
            @Override public void run() {
                // Tell the renderer that it's about to be paused so it can clean up.
                //TODO Stop MediaCodec? users may click home button and never come back again
                mRenderer.notifyPausing();
            }
        });
        mGLView.onPause();
        openCamera(myCameraId);
        mGLView.onResume();
        mGLView.queueEvent(new Runnable() {
            @Override public void run() {
                mRenderer.setCameraPreviewSize(mCameraPreviewWidth, mCameraPreviewHeight);
            }
        });
    }




    public void limiterTryMe()
    {
        CountDownTimer countDownTimerLimiter;
        countDownTimerLimiter = new CountDownTimer(30000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {

            }

            public void onFinish()
            {
                if(musicIsPlaying == true)
                {
                    setPlayPause(false);
                    exoPlayer.seekTo(0);
                    if(camActive == true)
                    {
                        mGLView.queueEvent(new Runnable() {
                            @Override public void run() {
                                mRenderer.changeRecordingState(false);
                            }
                        });
                        amrAudioRecorder.stop();
                    }
                    else
                    {
                        amrAudioRecorder.stop();
                    }
                    musicIsPlaying = false;
                    karaokeFinish = true;
                }
                dialog_tryMe("Anda hanya dapat mencoba lagu selama 30 detik. Silakan membeli paket untuk melanjutkan atau pilih lagu gratis");
            }
        };
        countDownTimerLimiter.start();
    }

    public void dialog_tryMe(String message)
    {
        DialogPlus warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
                goToPreview();
            }
        });
        warningDialog.show();

    }
}