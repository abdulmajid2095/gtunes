package gtunes.application.gtunes.ui.fragment.artis;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.adapter.ArtisAdapter;
import gtunes.application.gtunes.model.DataArtis;
import gtunes.application.gtunes.model.ResponseGetArtis;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArtisFragment extends Fragment {

    List<DataArtis> listArtisAllData = new ArrayList<>();
    List<DataArtis> listArtisTemp = new ArrayList<>();

    View rootview;
    RecyclerView recyclerView;
    ArtisAdapter myAdapter;
    Boolean loadMore = false;

    int jumlahItem = 0;
    int visibleItem = 10;
    int page = 1;
    int jumlahPage = 1;
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootview == null)
        {
            rootview = inflater.inflate(R.layout.fragment_artis, container, false);
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
            Bundle params = new Bundle();
            params.putString("View_Page", "User view artis page");
            mFirebaseAnalytics.logEvent("Activity_Artis_ViewListArtis", params);

            getArtisData();
            recyclerView = rootview.findViewById(R.id.rv_artis);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),2);
            recyclerView.setLayoutManager(gridLayoutManager);
            myAdapter = new ArtisAdapter(getActivity(),recyclerView,listArtisTemp);
            /*myAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    try {
                        loadMore = true;
                        listArtisTemp.add(null);
                        myAdapter.notifyItemInserted(listArtisTemp.size() - 1);

                        //Load more data for reyclerview
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                //Remove loading item
                                try {
                                    listArtisTemp.remove(listArtisTemp.size() - 1);
                                    myAdapter.notifyItemRemoved(listArtisTemp.size());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                int start = page * visibleItem;
                                int end = page * visibleItem + visibleItem;

                                if(page <= jumlahPage)
                                {
                                    for (int i=start ; i<end ; i++)
                                    {
                                        if(i < jumlahItem)
                                        {
                                            DataArtis dataArtis = new DataArtis( listArtisAllData.get(i).get_artist_id(),
                                                    listArtisAllData.get(i).get_artist_name(),
                                                    listArtisAllData.get(i).get_artist_image()
                                                    );
                                            listArtisTemp.add(dataArtis);
                                        }
                                    }

                                    page = page + 1;
                                }

                                myAdapter.notifyDataSetChanged();
                                myAdapter.setLoaded();
                                loadMore = false;


                            }
                        }, 2000);



                    } catch (Exception e) {

                    }
                }
            });*/
        }


        return rootview;
    }


    public void getArtisData()
    {
        /*if(loadMore == false)
        {*/
            listArtisAllData.clear();
            listArtisTemp.clear();
        /*}*/

        APIService service = ApiClient.getClient().create(APIService.class);
        Call<ResponseGetArtis> userCall = service.getArtis();
        userCall.enqueue(new Callback<ResponseGetArtis>() {
            @Override
            public void onResponse(Call<ResponseGetArtis> call, Response<ResponseGetArtis> response) {
                if(response.isSuccessful())
                {
                    try {
                        listArtisAllData = null;
                        listArtisAllData = response.body().getListArtis();

                        if(listArtisAllData.size()==0)
                        {
                            //Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            //for (int i = 0; i < visibleItem; i++)
                            for (int i = 0; i < listArtisAllData.size(); i++)
                            {
                                DataArtis dataArtis = new DataArtis( listArtisAllData.get(i).get_artist_id(),
                                        listArtisAllData.get(i).get_artist_name(),
                                        listArtisAllData.get(i).get_artist_image()
                                );
                                //Toast.makeText(getContext(),""+listArtisAllData.get(i).get_artist_name(),Toast.LENGTH_SHORT).show();
                                listArtisTemp.add(dataArtis);
                            }


                            /*if(loadMore == false)
                            {*/
                                jumlahItem = listArtisAllData.size();
                                jumlahPage = listArtisAllData.size() / visibleItem + 1;
                                recyclerView.setAdapter(myAdapter);
                            /*}*/

                            loadMore = false;
                            myAdapter.notifyDataSetChanged();
                            myAdapter.setLoaded();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getActivity(),"catch",Toast.LENGTH_SHORT).show();
                    }


                }
                else
                {
                    //Toast.makeText(getActivity(),"404",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseGetArtis> call, Throwable t) {
                //Toast.makeText(getActivity(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
