package gtunes.application.gtunes.Service;

import gtunes.application.gtunes.model.GoogleBilling;
import gtunes.application.gtunes.model.ResponseGetArtis;
import gtunes.application.gtunes.model.ResponseGetArtistDetail;
import gtunes.application.gtunes.model.ResponseGetBerita;
import gtunes.application.gtunes.model.ResponseGetGenre;
import gtunes.application.gtunes.model.ResponseGetLaguGenre;
import gtunes.application.gtunes.model.ResponseGetLaguKaraoke;
import gtunes.application.gtunes.model.ResponseGetPaket;
import gtunes.application.gtunes.model.ResponseGetPaketDetail;
import gtunes.application.gtunes.model.ResponseGetVersionCode;
import gtunes.application.gtunes.model.ResponseGetVideo;
import gtunes.application.gtunes.model.ResponseLogin;
import gtunes.application.gtunes.model.ResponseUploadImage;
import gtunes.application.gtunes.model.SearchGenre;
import gtunes.application.gtunes.model.SendEditProfile;
import gtunes.application.gtunes.model.SendLogin;
import gtunes.application.gtunes.model.SendPaketId;
import gtunes.application.gtunes.model.SendRegister;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface APIService {

    //API DEVELOPMENT

    //API for login
    @POST("auth/login")
    Call<ResponseLogin> login (@Body SendLogin sendLogin);

    //Api for register
    @POST("auth/register")
    Call<ResponseLogin> register (@Body SendRegister sendRegister);

    //Api for logout
    @POST("auth/logout")
    Call<ResponseLogin> logout (@Header("Authorization")String token);

    //Api for changing the avatar
    @Multipart
    @POST("profile/avatar")
    Call<ResponseUploadImage> uploadImage(@Header("Authorization")String token,
                                          @Part MultipartBody.Part file);

    //Api for edit the profile
    @POST("profile/edit")
    Call<ResponseLogin> editProfile (@Header("Authorization")String token,
                                     @Body SendEditProfile sendEditProfile);

    //Get all the karaoke songs
    @GET
    Call<ResponseGetLaguKaraoke> getSemuaLagu(@Url String url);

    //Get all the free karaoke songs
    @GET
    Call<ResponseGetLaguKaraoke> getFreeLagu(@Url String url);

    //Trying the paid version of the song
    @POST("song/try")
    Call<ResponseGetLaguKaraoke> getTryMe();

    //Api for send recording, but not use it
    @Multipart
    @POST("record")
    Call<ResponseUploadImage> uploadRecord (@Part MultipartBody.Part customerId,
                                            @Part MultipartBody.Part file,
                                            @Part MultipartBody.Part fileType,
                                            @Part MultipartBody.Part musicId,
                                            @Part MultipartBody.Part uploadId);

    //Showing the available package
    @GET("package")
    Call<ResponseGetPaket> getPaket();

    //Get the detail of the package
    @POST("package/detail")
    Call<ResponseGetPaketDetail> PaketDetail(@Body SendPaketId sendPaketId);

    //Purchase from google play
    @POST("google/purchase")
    Call<ResponseLogin> googlePurchase(@Header("Authorization")String token,
                                              @Body GoogleBilling body);

    //Get the version of the app
    @GET("app/version")
    Call<ResponseGetVersionCode> getVersion();

    /*@POST("package/detail")
    Call<ResponseGetPaket> getPaket();*/

    //API GTUNES

    //Get all news
    @GET("news")
    Call<ResponseGetBerita> getNews();

    //get all artist
    @GET("artist")
    Call<ResponseGetArtis> getArtis();

    //get all available genre
    @GET("genre")
    Call<ResponseGetGenre> getGenre();

    //get detail of the genre. Song list
    @GET
    Call<ResponseGetLaguGenre> getGenreDetail(@Url String url);

    //Searching the song based on selected genre
    @POST
    Call<ResponseGetLaguGenre> searchGenreDetail(@Url String url,
                                              @Body SearchGenre body);
    //Get all the video list
    @GET("video")
    Call<ResponseGetVideo> getVideo();

    //Get song of the artist
    @GET
    Call<ResponseGetArtistDetail> getLaguArtis(@Url String url);
}