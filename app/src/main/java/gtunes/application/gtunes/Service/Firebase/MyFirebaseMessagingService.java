package gtunes.application.gtunes.Service.Firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.ui.dashboard.DashboardActivity;
import gtunes.application.gtunes.ui.detail.BeritaDetail;
import gtunes.application.gtunes.ui.detail.MusicDetail;
import gtunes.application.gtunes.ui.detail.PaketWebview;
import gtunes.application.gtunes.ui.detail.StartKaraoke;
import gtunes.application.gtunes.ui.detail.VideosDetail;
import gtunes.application.gtunes.ui.splash.SplashScreen;

//This file is very important for push notification. It will handle the notification from firebase when phone
//In foreground, not background.


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //Just want to know the data is
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());

            //It will call function to determine what kind of notification is.
            handleNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(), remoteMessage);
            //showNotification(remoteMessage.getNotification().getBody());
        }
        else
        {
            Log.e("payload", "Data Payload: " + remoteMessage.getData().toString());
        }
    }

    private void showNotification(String messageBody) {
        Intent intent = new Intent(this, SplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notif)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private void handleNotification(String title, String message, RemoteMessage remoteMessage) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            Log.e("payload", "Data Payload: " + remoteMessage.getData().toString());
            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.e("payload", "Data Payload: " + remoteMessage.getData().toString());

                try {

                    //Get the data from notification payloads
                    Map<String, String> params = remoteMessage.getData();
                    JSONObject my_json = new JSONObject(params);

                    //The variable notiFor is used to separate between notification. First, we get the value from variabel notiFor
                    String notiFor = my_json.getString("notiFor");

                    //The, it will  check, what kind of notifiation like news, new song, new video, etc. And call different function
                    //based on kind of notification
                    //new news
                    if (notiFor.equals("news"))
                    {
                        news(title, message, my_json);
                    }
                    //there is a new song
                    else if (notiFor.equals("newSong"))
                    {
                        song(title, message, my_json);
                    }
                    //there is new video available
                    else if (notiFor.equals("newVideo"))
                    {
                        video(title, message, my_json);
                    }
                    //there is new karaoke song
                    else if (notiFor.equals("newKaraoke"))
                    {
                        karaoke(title, message, my_json);
                    }
                    //it will handle everything using webview
                    else if (notiFor.equals("webView"))
                    {
                        webview(title, message, my_json);
                    }
                    //general means any notifiation that doesnt need handling, like greetings, etc
                    else if (notiFor.equals("general"))
                    {
                        general(title, message, my_json);
                    }
                    //it can be used when we have new update for application
                    else if (notiFor.equals("updateApp")){
                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(600);
                        Log.e("Masuk Update App","Done");
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.gtunes.gtunes");
                        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
                        likeIng.setPackage("com.android.vending");
                        likeIng.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,likeIng, PendingIntent.FLAG_ONE_SHOT);
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
                        notificationBuilder.setContentTitle(""+title);
                        notificationBuilder.setContentText(""+message);
                        notificationBuilder.setAutoCancel(true);
                        notificationBuilder.setSmallIcon(R.drawable.ic_notif);
                        notificationBuilder.setContentIntent(pendingIntent);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            String channelId = getApplication().getString(R.string.default_notification_channel_id);
                            NotificationChannel channel = new NotificationChannel(channelId,   title, NotificationManager.IMPORTANCE_HIGH);
                            channel.setDescription(message);
                            notificationManager.createNotificationChannel(channel);
                            notificationBuilder.setChannelId(channelId);
                        }
                        Uri uri2= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        notificationBuilder.setSound(uri2);
                        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(""+message));
                        Bitmap bigImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notif);
                        notificationBuilder.setLargeIcon(bigImage);
                        notificationManager.notify(167,notificationBuilder.build());
                    }


                } catch (Exception e)
                {

                }
            }
            else {
                /*Intent intent = new Intent(getApplicationContext(), Offer_ReceiveJob.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
                notificationBuilder.setContentTitle("Payload Kosong");
                notificationBuilder.setContentText("Payloadmu mungkin ono sing salah");
                notificationBuilder.setAutoCancel(true);
                notificationBuilder.setSmallIcon(R.drawable.ic_notif);
                notificationBuilder.setContentIntent(pendingIntent);
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(0,notificationBuilder.build());*/
            }

        }else{
            // If the app is in background, firebase itself handles the notification
            /*Intent intent = new Intent(getApplicationContext(), Offer_ReceiveJob.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);*/
        }
    }

    public void general(String title, String message, JSONObject my_json)
    {
        //Make phone vibrate
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(600);

        //creating an intent and redirect to dashboard menu
        Intent intent = new Intent(MyFirebaseMessagingService.this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Random r = new Random();
        int my_random = r.nextInt(80 - 1) + 1;

        //Create pending intent to show notification
        PendingIntent pendingIntent2 = PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(message);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSmallIcon(R.drawable.ic_notif);
        notificationBuilder.setContentIntent(pendingIntent2);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = getApplication().getString(R.string.default_notification_channel_id);
            NotificationChannel channel = new NotificationChannel(channelId,   title, NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(message);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(uri);
        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(""+message));
        Bitmap bigImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notif);
        notificationBuilder.setLargeIcon(bigImage);
        notificationManager.notify(my_random,notificationBuilder.build());
        Log.e("Masuk", "Berhasil create notifikasi");


    }

    public void news(String title, String message, JSONObject my_json)
    {
        try {
            //vibrate the phone
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(600);

            //creating intent and redirect to detail of news when notification clicked
            Intent intent = new Intent(MyFirebaseMessagingService.this, BeritaDetail.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //getting some data from notifivation, and pass it to next page
            intent.putExtra("title",""+my_json.getString("title"));
            intent.putExtra("date",""+my_json.getString("date"));
            intent.putExtra("desc",""+my_json.getString("desc"));
            intent.putExtra("image",""+my_json.getString("image"));

            Random r = new Random();
            int my_random = r.nextInt(80 - 1) + 1;

            //creating pending intent and show the notification
            PendingIntent pendingIntent2 = PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(message);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSmallIcon(R.drawable.ic_notif);
            notificationBuilder.setContentIntent(pendingIntent2);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = getApplication().getString(R.string.default_notification_channel_id);
                NotificationChannel channel = new NotificationChannel(channelId,   title, NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription(message);
                notificationManager.createNotificationChannel(channel);
                notificationBuilder.setChannelId(channelId);
            }
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBuilder.setSound(uri);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(""+message));
            Bitmap bigImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notif);
            notificationBuilder.setLargeIcon(bigImage);
            notificationManager.notify(my_random,notificationBuilder.build());
            Log.e("Masuk", "Berhasil create notifikasi");


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Masuk", "Gagal create notifikasi");
        }
    }

    public void video(String title, String message, JSONObject my_json)
    {
        try {
            //vibrate the phone
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(600);

            //creating an intent and redirect it to detail of the video page
            Intent intent = new Intent(MyFirebaseMessagingService.this, VideosDetail.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //get some data from the data payloads from notification
            intent.putExtra("title",""+my_json.getString("title"));
            intent.putExtra("url",""+my_json.getString("url"));

            Random r = new Random();
            int my_random = r.nextInt(80 - 1) + 1;

            //creating pending intent and show the notification
            PendingIntent pendingIntent2 = PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(message);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSmallIcon(R.drawable.ic_notif);
            notificationBuilder.setContentIntent(pendingIntent2);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = getApplication().getString(R.string.default_notification_channel_id);
                NotificationChannel channel = new NotificationChannel(channelId,   title, NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription(message);
                notificationManager.createNotificationChannel(channel);
                notificationBuilder.setChannelId(channelId);
            }
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBuilder.setSound(uri);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(""+message));
            Bitmap bigImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notif);
            notificationBuilder.setLargeIcon(bigImage);
            notificationManager.notify(my_random,notificationBuilder.build());
            Log.e("Masuk", "Berhasil create notifikasi");


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Masuk", "Gagal create notifikasi");
        }
    }

    public void song(String title, String message, JSONObject my_json)
    {
        try {
            //vibrate the phone
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(600);

            //creating intent and redorect it to music player when the notification clicked
            Intent intent = new Intent(MyFirebaseMessagingService.this, MusicDetail.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //get some data from the notif, and pass it to music player page
            intent.putExtra("musicId",""+my_json.getString("musicId"));
            intent.putExtra("title",""+my_json.getString("title"));
            intent.putExtra("band",""+my_json.getString("band"));
            intent.putExtra("gambar",""+my_json.getString("gambar"));
            intent.putExtra("tryme",""+my_json.getString("tryme"));
            intent.putExtra("url",""+my_json.getString("url"));
            intent.putExtra("karaoke",""+my_json.getString("karaoke"));
            intent.putExtra("url_karaoke",""+my_json.getString("url_karaoke"));
            intent.putExtra("lirik",""+my_json.getString("lirik"));

            Random r = new Random();
            int my_random = r.nextInt(80 - 1) + 1;

            //create pending intent and show notification
            PendingIntent pendingIntent2 = PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(message);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSmallIcon(R.drawable.ic_notif);
            notificationBuilder.setContentIntent(pendingIntent2);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = getApplication().getString(R.string.default_notification_channel_id);
                NotificationChannel channel = new NotificationChannel(channelId,   title, NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription(message);
                notificationManager.createNotificationChannel(channel);
                notificationBuilder.setChannelId(channelId);
            }
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBuilder.setSound(uri);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(""+message));
            Bitmap bigImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notif);
            notificationBuilder.setLargeIcon(bigImage);
            notificationManager.notify(my_random,notificationBuilder.build());
            Log.e("Masuk", "Berhasil create notifikasi");


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Masuk", "Gagal create notifikasi");
        }
    }


    public void karaoke(String title, String message, JSONObject my_json)
    {
        try {
            //vibrate the phone
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(600);

            //creating an intent, and redirect it to karaoke page when clicked
            Intent intent = new Intent(MyFirebaseMessagingService.this, StartKaraoke.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //Get some data from notification, and pass it to next page
            intent.putExtra("tryme",""+my_json.getString("tryme"));
            intent.putExtra("musicId",""+my_json.getString("musicId"));
            intent.putExtra("title",""+my_json.getString("title"));
            intent.putExtra("band",""+my_json.getString("band"));
            intent.putExtra("url_karaoke",""+my_json.getString("url_karaoke"));
            intent.putExtra("gambar",""+my_json.getString("gambar"));
            intent.putExtra("lirik",""+my_json.getString("lirik"));
            Random r = new Random();
            int my_random = r.nextInt(80 - 1) + 1;
            //create pending intent and show the notification
            PendingIntent pendingIntent2 = PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(message);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSmallIcon(R.drawable.ic_notif);
            notificationBuilder.setContentIntent(pendingIntent2);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = getApplication().getString(R.string.default_notification_channel_id);
                NotificationChannel channel = new NotificationChannel(channelId,   title, NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription(message);
                notificationManager.createNotificationChannel(channel);
                notificationBuilder.setChannelId(channelId);
            }
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBuilder.setSound(uri);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(""+message));
            Bitmap bigImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notif);
            notificationBuilder.setLargeIcon(bigImage);
            notificationManager.notify(my_random,notificationBuilder.build());
            Log.e("Masuk", "Berhasil create notifikasi");


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Masuk", "Gagal create notifikasi");
        }
    }


    public void webview(String title, String message, JSONObject my_json)
    {
        try {
            //vibrate the phone
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(600);

            /*Intent intent = new Intent(getApplicationContext(), Offer_ReceiveJob.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
            //creating an intent and redorect to webview when clicked
            Intent intent = new Intent(MyFirebaseMessagingService.this, PaketWebview.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //get data from notification and pass it to the next page
            intent.putExtra("title",""+my_json.getString("title"));
            intent.putExtra("url",""+my_json.getString("url"));

            Random r = new Random();
            int my_random = r.nextInt(80 - 1) + 1;

            //startActivity(intent);
            //create pending intent and show the notification
            PendingIntent pendingIntent2 = PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(message);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSmallIcon(R.drawable.ic_notif);
            notificationBuilder.setContentIntent(pendingIntent2);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = getApplication().getString(R.string.default_notification_channel_id);
                NotificationChannel channel = new NotificationChannel(channelId,   title, NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription(message);
                notificationManager.createNotificationChannel(channel);
                notificationBuilder.setChannelId(channelId);
            }
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notificationBuilder.setSound(uri);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(""+message));
            Bitmap bigImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notif);
            notificationBuilder.setLargeIcon(bigImage);
            notificationManager.notify(my_random,notificationBuilder.build());
            Log.e("Masuk", "Berhasil create notifikasi");


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("Masuk", "Gagal create notifikasi");
        }

    }



}
