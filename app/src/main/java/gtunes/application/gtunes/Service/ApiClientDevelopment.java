package gtunes.application.gtunes.Service;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientDevelopment {

    //BASE URL OF the API
    //first it using for development, cause gtunes doesnt allow to using theirs
    //Intinya, karna satu app ini ngakses ke 2 base api yg berberda, jadi ada file ini.
    //setelah di merger alamatnya disamakan


    //public static final String BASE_URL = "https://av.mimzy.xyz/";
    public static final String BASE_URL = "http://api.gtunes.co.id/";
    public static final String BASE_URL_DWNLD = "";
    private static Retrofit retrofit = null;


    private static Context context;

    public static Retrofit getClient(){
        if (retrofit == null){

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getDownload(){
        if (retrofit == null){
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_DWNLD)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }

        return retrofit;
    }
}
