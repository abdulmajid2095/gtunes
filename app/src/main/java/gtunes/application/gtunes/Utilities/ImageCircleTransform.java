package gtunes.application.gtunes.Utilities;

/**
 * Created by AbdulMajid on 30/01/2019.
 */


import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

public class ImageCircleTransform implements Transformation {
    @Override
    public Bitmap transform(Bitmap source) {
        return ImageUtilities.getCircularBitmapImage(source);
    }
    @Override
    public String key() {
        return "circle-image";
    }
}