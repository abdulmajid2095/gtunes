package gtunes.application.gtunes;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.vistrav.ask.Ask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import gtunes.application.gtunes.Database.Data_Profile;
import gtunes.application.gtunes.Service.APIService;
import gtunes.application.gtunes.Service.ApiClient;
import gtunes.application.gtunes.Service.ApiClientDevelopment;
import gtunes.application.gtunes.model.ResponseLogin;
import gtunes.application.gtunes.model.SendLogin;
import gtunes.application.gtunes.ui.dashboard.DashboardActivity;
import gtunes.application.gtunes.ui.registrasi.RegistrationActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout login_btn;
    TextView regis_btn;
    MaterialEditText editTextHandphone,editTextPassword;
    DialogPlus dialog,warningDialog;
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.login_activity);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("View_Page", "User view login page");
        mFirebaseAnalytics.logEvent("Activity_Login_ViewPage", params);

        //Crashlytics.getInstance().crash(); // Force a crash

        regis_btn = findViewById(R.id.regis_btn);
        regis_btn.setOnClickListener(this);
        login_btn = findViewById(R.id.login_btn);
        login_btn.setOnClickListener(this);

        editTextHandphone = findViewById(R.id.phone);
        editTextPassword = findViewById(R.id.password);

        check_permission();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),DashboardActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.login_btn:
                Bundle params = new Bundle();
                params.putString("Login", "User Press login");
                mFirebaseAnalytics.logEvent("Activity_Login_PressLogin", params);
                String myPhone = ""+editTextHandphone.getText().toString();
                String myPass = ""+editTextPassword.getText().toString();
                cekForm(myPhone,myPass);
                /*Intent intent = new Intent(MainActivity.this,DashboardActivity.class);
                startActivity(intent);*/
                break;

            case R.id.regis_btn:
                Bundle params2 = new Bundle();
                params2.putString("Register", "User Press Register");
                mFirebaseAnalytics.logEvent("Activity_Login_PressRegister", params2);
                Intent intent2 = new Intent(MainActivity.this,RegistrationActivity.class);
                startActivity(intent2);
                break;
        }
    }

    public void cekForm(String myPhone, String myPass)
    {
        Boolean phoneKosong = true, passKosong = true;
        if(myPhone.equals("") || myPhone == null)
        {
            phoneKosong = true;
            editTextHandphone.setError("Username tidak boleh kosong");
        }
        else
        {
            editTextHandphone.setError(null);
            phoneKosong = false;
        }

        if(myPass.equals("") || myPass == null)
        {
            passKosong = true;
            editTextPassword.setError("Password tidak boleh kosong");
        }
        else
        {
            editTextPassword.setError(null);
            passKosong = false;
        }

        if(passKosong == false && phoneKosong == false)
        {
            sendData(myPhone,myPass);
            //dialog_warning("ljlkjljlk");
        }
    }


    public void sendData(String myPhone, String myPass)
    {
            Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
            data_profile.loginUser = ""+myPhone;
            data_profile.loginPassword = ""+myPass;
            data_profile.save();

            dialog_loading();
            SendLogin sendLogin = new SendLogin(myPhone,myPass);
            APIService service = ApiClientDevelopment.getClient().create(APIService.class);
            Call<ResponseLogin> userCall = service.login(sendLogin);
            userCall.enqueue(new Callback<ResponseLogin>() {
                @Override
                public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                    if(response.isSuccessful()){
                        Bundle params2 = new Bundle();
                        params2.putString("Login", "User Success to Login");
                        mFirebaseAnalytics.logEvent("Activity_Login_Success", params2);
                        dialog.dismiss();
                        Data_Profile data_profile = Data_Profile.findById(Data_Profile.class,1L);
                        data_profile.token = ""+response.body().get_dataData().get_token();
                        data_profile.user_id = ""+response.body().get_dataData().get_dataUser().get_user_id();
                        data_profile.phone = ""+response.body().get_dataData().get_dataUser().get_phone();
                        data_profile.telco = ""+response.body().get_dataData().get_dataUser().get_telco();
                        data_profile.email = ""+response.body().get_dataData().get_dataUser().get_email();
                        data_profile.active_until = ""+response.body().get_dataData().get_dataUser().get_active_until();
                        data_profile.paket_status = ""+response.body().get_dataData().get_dataUser().get_paket_status();
                        data_profile.login = true;
                        data_profile.avatar = "default";
                        data_profile.nama = ""+response.body().get_dataData().get_dataUser().get_user_id();
                        data_profile.save();
                        Toast.makeText(getApplicationContext(),"Login Berhasil",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),DashboardActivity.class));
                        finish();
                    } else{
                        dialog.dismiss();
                        JSONObject jObjError = null;
                        String message = "Gagal! Silakan cek email/User Id dan password anda.";
                        /*try {
                            jObjError = new JSONObject(response.errorBody().string());
                            JSONObject jObjError2 = null;
                            jObjError2 = new JSONObject(jObjError.getString("error"));
                            message = ""+jObjError2.getString("userMsg");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/
                        final String myMessage = message;
                        Handler handler =  new Handler();
                        Runnable myRunnable = new Runnable() {
                            public void run() {
                                // do something
                                dialog_warning(""+myMessage);
                            }
                        };
                        handler.postDelayed(myRunnable,500);
                    }

                }

                @Override
                public void onFailure(Call<ResponseLogin> call, Throwable t) {
                    dialog.dismiss();
                    Handler handler =  new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            // do something
                            dialog_warning("Terjadi kesalahan. Cek koneksi internet Anda.");
                        }
                    };
                    handler.postDelayed(myRunnable,500);

                }
            });
    }

    public void dialog_loading()
    {
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_loading))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = dialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        textMessage.setText("Memuat...");
        dialog.show();

    }

    public void dialog_warning(String message)
    {
        Bundle params2 = new Bundle();
        params2.putString("Login", "User failed to Login");
        mFirebaseAnalytics.logEvent("Activity_Login_Failed", params2);

        warningDialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.dialog_warning))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();

        View view = warningDialog.getHolderView();
        TextView textMessage = view.findViewById(R.id.message);
        TextView textTitle = view.findViewById(R.id.title);
        textTitle.setText("Oops !");
        textMessage.setText(""+message);

        TextView oke = view.findViewById(R.id.oke);
        oke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warningDialog.dismiss();
            }
        });
        warningDialog.show();

    }


    public void check_permission()
    {
        Ask.on(this)
                .id(1) // in case you are invoking multiple time Ask from same activity or fragment
                .forPermissions(Manifest.permission.CAMERA
                        , Manifest.permission.READ_EXTERNAL_STORAGE
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE)
                /*.withRationales("Location permission need for map to work properly",
                        "In order to save file you will need to grant storage permission") *///optional
                .go();
    }
}
