package gtunes.application.gtunes.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.pixplicity.fontview.FontAppCompatTextView;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.model.DataVideo;
import gtunes.application.gtunes.ui.detail.VideosDetail;

//This is adapter for showing listing of video
public class    VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener mOnLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    RecyclerView mRecyclerView;
    List<DataVideo> myArray;
    Context c;

    public VideoAdapter(Context c, RecyclerView mRecyclerView, List<DataVideo> myArray) {

        this.mRecyclerView = mRecyclerView;
        this.myArray = myArray;
        this.c = c;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                //using for loadmore function
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return myArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(c).inflate(R.layout.item_videos, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserViewHolder) {
            //Storing the array data into Video Model
            final DataVideo dataVideo = myArray.get(position);
            UserViewHolder userViewHolder = (UserViewHolder) holder;

            //Set the data into View
            String myTitle = "" + dataVideo.get_video_clip_title();
            if(!myTitle.equals("")) {
                List<String> listTag = Arrays.asList(myTitle.split(" - "));
                try {
                    userViewHolder.t_artis.setText(""+listTag.get(0));
                    userViewHolder.t_title.setText(""+listTag.get(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //Piscasso used for showing image
            Picasso.with(c)
                    .load("http://gtunes.co.id/" + dataVideo.get_video_clip_image())
                    .noPlaceholder()
                    .resize(600, 400)
                    .centerCrop()
                    .into(userViewHolder.image);

            userViewHolder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(c,VideosDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("url","" + dataVideo.get_video_clip_url());
                    intent.putExtra("title","" +dataVideo.get_video_clip_title());
                    c.startActivity(intent);
                }
            });
        }


        else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return myArray == null ? 0 : myArray.size();
    }

    public void setLoaded() {
        isLoading = false;
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        public FontAppCompatTextView t_artis,t_title;
        public ImageView image;
        public RelativeLayout parent;

        public UserViewHolder(View view) {
            super(view);
            c = itemView.getContext();

            t_artis = view.findViewById(R.id.artis);
            t_title = view.findViewById(R.id.title);
            image = view.findViewById(R.id.img_videos);
            parent = view.findViewById(R.id.parent);

        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            //showing progressbar when using loadmore
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }



}