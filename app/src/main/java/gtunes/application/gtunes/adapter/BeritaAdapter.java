package gtunes.application.gtunes.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.pixplicity.fontview.FontAppCompatTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.model.DataBerita;
import gtunes.application.gtunes.ui.detail.BeritaDetail;

//This is adapter for showing listing of news
public class BeritaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener mOnLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    RecyclerView mRecyclerView;
    List<DataBerita> myArray;
    Context c;

    public BeritaAdapter(Context c, RecyclerView mRecyclerView, List<DataBerita> myArray) {

        this.mRecyclerView = mRecyclerView;
        this.myArray = myArray;
        this.c = c;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                //using for loadmore function
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return myArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(c).inflate(R.layout.item_berita, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserViewHolder) {

            //Storing the array data into Berita Model
            final DataBerita dataBerita = myArray.get(position);
            UserViewHolder userViewHolder = (UserViewHolder) holder;

            //Set the data into View
            userViewHolder.t_title.setText("" + dataBerita.get_whats_new_title());
            userViewHolder.t_shordDesc.setText("" + dataBerita.get_whats_new_short_desc());
            userViewHolder.t_date.setText("" + dataBerita.get_dateHuman());

            //Piscasso used for showing image
            Picasso.with(c)
                    .load("http://gtunes.co.id/" + dataBerita.get_whats_new_image())
                    .noPlaceholder()
                    .resize(150, 115)
                    .centerCrop()
                    .into(userViewHolder.image);

            //if user press the list, it will redirrect to Detail of detail of news page and bring data like
            //news title, news date, news image, and description of the news
            userViewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(c, BeritaDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("title",""+ dataBerita.get_whats_new_title());
                    intent.putExtra("date",""+ dataBerita.get_dateHuman());
                    intent.putExtra("desc",""+ dataBerita.get_whats_new_desc());
                    intent.putExtra("image",""+ dataBerita.get_whats_new_image());
                    c.startActivity(intent);
                }
            });
        }


        else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return myArray == null ? 0 : myArray.size();
    }

    public void setLoaded() {
        isLoading = false;
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        public FontAppCompatTextView t_title,t_shordDesc,t_date;
        public ImageView image;
        public RelativeLayout parentLayout;

        public UserViewHolder(View view) {
            super(view);
            c = itemView.getContext();

            //Initializing view
            t_title = view.findViewById(R.id.title);
            t_shordDesc = view.findViewById(R.id.shortDesc);
            t_date = view.findViewById(R.id.date);
            image = view.findViewById(R.id.image);
            parentLayout = view.findViewById(R.id.parent);

        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            //showing progressbar when using loadmore
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }



}