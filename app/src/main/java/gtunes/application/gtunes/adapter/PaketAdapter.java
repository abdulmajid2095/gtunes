package gtunes.application.gtunes.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pixplicity.fontview.FontAppCompatTextView;

import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.model.DataLagu;
import gtunes.application.gtunes.model.DataPaket;
import gtunes.application.gtunes.ui.detail.MusicDetail;
import gtunes.application.gtunes.ui.detail.PaketDetail;
import gtunes.application.gtunes.ui.detail.PaketWebview;
import gtunes.application.gtunes.ui.detail.StartKaraoke;

//This is adapter for showing listing of paket
public class PaketAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener mOnLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    RecyclerView mRecyclerView;
    List<DataPaket> myArray;
    Context c;

    public PaketAdapter(Context c, RecyclerView mRecyclerView, List<DataPaket> myArray) {

        this.mRecyclerView = mRecyclerView;
        this.myArray = myArray;
        this.c = c;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //using for loadmore function
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return myArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(c).inflate(R.layout.item_pilih_paket, parent, false);
            return new gtunes.application.gtunes.adapter.PaketAdapter.UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
            return new gtunes.application.gtunes.adapter.PaketAdapter.LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof gtunes.application.gtunes.adapter.PaketAdapter.UserViewHolder) {

            //Storing the array data into DataPaket Model
            final DataPaket dataPaket = myArray.get(position);
            gtunes.application.gtunes.adapter.PaketAdapter.UserViewHolder userViewHolder = (gtunes.application.gtunes.adapter.PaketAdapter.UserViewHolder) holder;

            //Set the data into View
            userViewHolder.t_judul.setText(""+dataPaket.get_paket_hari()+" Hari");

            //if user press the list, it will redirrect to Detail of detail of paket data page and bring the id of the paket data
            userViewHolder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(c,PaketDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("paket_id","" + ""+dataPaket.get_paket_id());
                    c.startActivity(intent);
                }
            });

        }

        else if (holder instanceof gtunes.application.gtunes.adapter.PaketAdapter.LoadingViewHolder) {
            gtunes.application.gtunes.adapter.PaketAdapter.LoadingViewHolder loadingViewHolder = (gtunes.application.gtunes.adapter.PaketAdapter.LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return myArray == null ? 0 : myArray.size();
    }

    public void setLoaded() {
        isLoading = false;
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        public FontAppCompatTextView t_judul;
        public LinearLayout parent;

        public UserViewHolder(View view) {
            super(view);
            c = itemView.getContext();

            //Initializing view
            parent = view.findViewById(R.id.parent);
            t_judul = view.findViewById(R.id.nama_paket);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            //showing progressbar when using loadmore
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

}
