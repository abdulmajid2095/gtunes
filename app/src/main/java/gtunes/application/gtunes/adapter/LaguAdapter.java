package gtunes.application.gtunes.adapter;
/*
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import gtunes.application.gtunes.MainActivity;
import gtunes.application.gtunes.R;
import gtunes.application.gtunes.model.Artis;
import gtunes.application.gtunes.model.Lagu;
import gtunes.application.gtunes.ui.dashboard.DashboardActivity;
import gtunes.application.gtunes.ui.detail.GenreDetail;
import gtunes.application.gtunes.ui.detail.StartKaraoke;
import gtunes.application.gtunes.ui.fragment.karaoke.KaraokeFragment;

public class LaguAdapter extends RecyclerView.Adapter<LaguAdapter.LaguViewHolder> {
    private List<Lagu> laguList;
    Context context;

    public LaguAdapter(List<Lagu> laguList, Context context) {
        this.laguList = laguList;
        this.context = context;
    }

    @Override
    public LaguAdapter.LaguViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lagu, parent, false);
        LaguAdapter.LaguViewHolder gvh = new LaguAdapter.LaguViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(LaguAdapter.LaguViewHolder holder, final int position) {
        holder.tv_judul.setText(laguList.get(position).getJudulLagu());
        holder.tv_artis.setText(laguList.get(position).getArtsLagu());
        holder.tv_menit.setText(laguList.get(position).getTime());

        holder.iv_karaoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), StartKaraoke.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return laguList.size();
    }

    public class LaguViewHolder extends RecyclerView.ViewHolder {
        TextView tv_judul, tv_artis, tv_menit;
        ImageView iv_karaoke;

        public LaguViewHolder(View view) {
            super(view);
            tv_judul = view.findViewById(R.id.tv_judul);
            tv_artis = view.findViewById(R.id.tv_artis);
            tv_menit = view.findViewById(R.id.mtv_menit);
            iv_karaoke = view.findViewById(R.id.iv_karaoke);

        }
    }*/

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pixplicity.fontview.FontAppCompatTextView;

import java.util.List;

import gtunes.application.gtunes.Database.Data_Profile;
import gtunes.application.gtunes.MainActivity;
import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.model.DataLagu;
import gtunes.application.gtunes.ui.dashboard.DashboardActivity;
import gtunes.application.gtunes.ui.detail.MusicDetail;
import gtunes.application.gtunes.ui.detail.StartKaraoke;

public class LaguAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener mOnLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    RecyclerView mRecyclerView;
    List<DataLagu> myArray;
    Context c;
    Data_Profile data_profile;

    //This is adapter for showing listing of songs

    public LaguAdapter(Context c, RecyclerView mRecyclerView, List<DataLagu> myArray) {

        this.mRecyclerView = mRecyclerView;
        this.myArray = myArray;
        this.c = c;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                //using for loadmore function
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return myArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(c).inflate(R.layout.item_lagu, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserViewHolder) {

            //Storing the array data into Berita Model
            final DataLagu dataLagu = myArray.get(position);
            UserViewHolder userViewHolder = (UserViewHolder) holder;
            data_profile = Data_Profile.findById(Data_Profile.class,1L);

            //Set the data into View
            userViewHolder.t_artis.setText("" + dataLagu.get_artist_name());
            userViewHolder.t_judul.setText("" + dataLagu.get_music_title());

            //called when user press the song
            userViewHolder.playLagu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //check, if the song is free or not. Zero means paid songs. And One meaning free song
                    if(dataLagu.get_music_free().equals("0"))
                    {
                        //Check if the user is login or not. False if user not login
                        //If user not login yet, it will redirect to login page
                        if(data_profile.login == false)
                        {
                            Toast.makeText(c,"Silakan login untuk memutar lagu ini",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(c,MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            c.startActivity(intent);
                        }
                        //If the package status is zero, meaning users doesn't have any package yet. So it will redirect automatically
                        //to buy package page. They can choose to select google payment or telcos
                        else if(data_profile.paket_status.equals("0"))
                        {
                            Toast.makeText(c,"Lagu berbayar. Silakan melakukan pembelian paket untuk memutar lagu ini",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(c,DashboardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("tab","paket");
                            c.startActivity(intent);
                        }
                        //if user is login and have package, it will redirect to music player page
                        else
                        {
                            Intent intent = new Intent(c,MusicDetail.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("title","" + dataLagu.get_music_title());
                            intent.putExtra("band","" + dataLagu.get_artist_name());
                            intent.putExtra("gambar","" + dataLagu.get_music_artwork());
                            intent.putExtra("url","" + dataLagu.get_music_url());
                            intent.putExtra("karaoke","" + dataLagu.get_music_karaoke_available());
                            intent.putExtra("url_karaoke","" + dataLagu.get_music_singsong_url());
                            intent.putExtra("lirik","" + dataLagu.get_music_lyric());
                            intent.putExtra("musicId","" + dataLagu.get_music_id());
                            intent.putExtra("tryme","" + dataLagu.get_tryme());
                            c.startActivity(intent);
                        }
                    }
                    //and if the song is free, it will redirect directly to music player page
                    else
                    {
                        Intent intent = new Intent(c,MusicDetail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("title","" + dataLagu.get_music_title());
                        intent.putExtra("band","" + dataLagu.get_artist_name());
                        intent.putExtra("gambar","" + dataLagu.get_music_artwork());
                        intent.putExtra("url","" + dataLagu.get_music_url());
                        intent.putExtra("karaoke","" + dataLagu.get_music_karaoke_available());
                        intent.putExtra("url_karaoke","" + dataLagu.get_music_singsong_url());
                        intent.putExtra("lirik","" + dataLagu.get_music_lyric());
                        intent.putExtra("musicId","" + dataLagu.get_music_id());
                        intent.putExtra("tryme","" + dataLagu.get_tryme());
                        c.startActivity(intent);
                    }

                }
            });

            //called when user press the song
            userViewHolder.playLagu2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //check, if the song is free or not. Zero means paid songs. And One meaning free song
                    if(dataLagu.get_music_free().equals("0"))
                    {
                        //Check if the user is login or not. False if user not login
                        //If user not login yet, it will redirect to login page
                        if(data_profile.login == false)
                        {
                            Toast.makeText(c,"Silakan login untuk memutar lagu ini",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(c,MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            c.startActivity(intent);
                        }
                        //If the package status is zero, meaning users doesn't have any package yet. So it will redirect automatically
                        //to buy package page. They can choose to select google payment or telcos
                        else if(data_profile.paket_status.equals("0"))
                        {
                            Toast.makeText(c,"Lagu berbayar. Silakan melakukan pembelian paket untuk memutar lagu ini",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(c,DashboardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("tab","paket");
                            c.startActivity(intent);
                        }
                        //if user is login and have package, it will redirect to music player page
                        else
                        {
                            Intent intent = new Intent(c,MusicDetail.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("title","" + dataLagu.get_music_title());
                            intent.putExtra("band","" + dataLagu.get_artist_name());
                            intent.putExtra("gambar","" + dataLagu.get_music_artwork());
                            intent.putExtra("url","" + dataLagu.get_music_url());
                            intent.putExtra("karaoke","" + dataLagu.get_music_karaoke_available());
                            intent.putExtra("url_karaoke","" + dataLagu.get_music_singsong_url());
                            intent.putExtra("lirik","" + dataLagu.get_music_lyric());
                            intent.putExtra("musicId","" + dataLagu.get_music_id());
                            intent.putExtra("tryme","" + dataLagu.get_tryme());
                            c.startActivity(intent);
                        }
                    }
                    //and if the song is free, it will redirect directly to music player page
                    else
                    {
                        Intent intent = new Intent(c,MusicDetail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("title","" + dataLagu.get_music_title());
                        intent.putExtra("band","" + dataLagu.get_artist_name());
                        intent.putExtra("gambar","" + dataLagu.get_music_artwork());
                        intent.putExtra("url","" + dataLagu.get_music_url());
                        intent.putExtra("karaoke","" + dataLagu.get_music_karaoke_available());
                        intent.putExtra("url_karaoke","" + dataLagu.get_music_singsong_url());
                        intent.putExtra("lirik","" + dataLagu.get_music_lyric());
                        intent.putExtra("musicId","" + dataLagu.get_music_id());
                        intent.putExtra("tryme","" + dataLagu.get_tryme());
                        c.startActivity(intent);
                    }
                }
            });

            //if the songs don't have the karaoke version, the karaoke button will be hidden
            if(dataLagu.get_music_karaoke_available().equals("0"))
            {
                userViewHolder.playKaraoke.setVisibility(View.GONE);
            }

            //it will called when user press the karaoke button
            userViewHolder.playKaraoke.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //check, if the song is free or not. Zero means paid songs. And One meaning free song
                    if(dataLagu.get_music_free().equals("0"))
                    {
                        //Check if the user is login or not. False if user not login
                        //If user not login yet, it will redirect to login page
                        if(data_profile.login == false)
                        {
                            Toast.makeText(c,"Silakan login untuk memutar lagu ini",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(c,MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            c.startActivity(intent);
                        }
                        //If the package status is zero, meaning users doesn't have any package yet. So it will redirect automatically
                        //to buy package page. They can choose to select google payment or telcos
                        else if(data_profile.paket_status.equals("0"))
                        {
                            Toast.makeText(c,"Lagu berbayar. Silakan melakukan pembelian paket untuk memutar lagu ini",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(c,DashboardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("tab","paket");
                            c.startActivity(intent);
                        }
                        //if user is login and have package, it will redirect to karaoke page
                        else
                        {
                            Intent intent = new Intent(c,StartKaraoke.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("title","" + dataLagu.get_music_title());
                            intent.putExtra("band","" + dataLagu.get_artist_name());
                            intent.putExtra("gambar","" + dataLagu.get_music_artwork());
                            intent.putExtra("url_karaoke","" + dataLagu.get_music_singsong_url());
                            intent.putExtra("lirik","" + dataLagu.get_music_lyric());
                            intent.putExtra("musicId","" + dataLagu.get_music_id());
                            intent.putExtra("tryme","" + dataLagu.get_tryme());
                            c.startActivity(intent);
                        }
                    }
                    //and if the song is free, it will redirect directly to karaoke page
                    else
                    {
                        Intent intent = new Intent(c,StartKaraoke.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("title","" + dataLagu.get_music_title());
                        intent.putExtra("band","" + dataLagu.get_artist_name());
                        intent.putExtra("gambar","" + dataLagu.get_music_artwork());
                        intent.putExtra("url_karaoke","" + dataLagu.get_music_singsong_url());
                        intent.putExtra("lirik","" + dataLagu.get_music_lyric());
                        intent.putExtra("musicId","" + dataLagu.get_music_id());
                        intent.putExtra("tryme","" + dataLagu.get_tryme());
                        c.startActivity(intent);
                    }

                }
            });
        }


        else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return myArray == null ? 0 : myArray.size();
    }

    public void setLoaded() {
        isLoading = false;
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        public FontAppCompatTextView t_judul,t_artis;
        public ImageView playLagu,playKaraoke;
        public LinearLayout playLagu2;

        public UserViewHolder(View view) {
            super(view);
            c = itemView.getContext();

            //Initializing view
            t_artis = view.findViewById(R.id.tv_artis);
            t_judul = view.findViewById(R.id.tv_judul);
            playLagu = view.findViewById(R.id.play);
            playLagu2 = view.findViewById(R.id.play2);
            playKaraoke = view.findViewById(R.id.iv_karaoke);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            //showing progressbar when using loadmore
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

}
