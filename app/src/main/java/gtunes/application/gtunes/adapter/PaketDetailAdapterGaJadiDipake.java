package gtunes.application.gtunes.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.vending.billing.IInAppBillingService;
import com.pixplicity.fontview.FontAppCompatTextView;

import java.util.List;

import gtunes.application.gtunes.R;
import gtunes.application.gtunes.Utilities.OnLoadMoreListener;
import gtunes.application.gtunes.model.DataTelcos;
import gtunes.application.gtunes.ui.detail.PaketWebview;

public class PaketDetailAdapterGaJadiDipake extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener mOnLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    RecyclerView mRecyclerView;
    List<DataTelcos> myArray;
    Context c;

    String IN_APP_CODE = "";

    public PaketDetailAdapterGaJadiDipake(Context c, RecyclerView mRecyclerView, List<DataTelcos> myArray) {

        this.mRecyclerView = mRecyclerView;
        this.myArray = myArray;
        this.c = c;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return myArray.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(c).inflate(R.layout.item_pilih_paket, parent, false);
            return new PaketDetailAdapterGaJadiDipake.UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(c).inflate(R.layout.progressbar, parent, false);
            return new PaketDetailAdapterGaJadiDipake.LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PaketDetailAdapterGaJadiDipake.UserViewHolder) {

            final DataTelcos dataTelcos = myArray.get(position);
            PaketDetailAdapterGaJadiDipake.UserViewHolder userViewHolder = (PaketDetailAdapterGaJadiDipake.UserViewHolder) holder;

            userViewHolder.t_judul.setText(""+dataTelcos.get_telco());
            userViewHolder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(dataTelcos.get_type().equals("google"))
                    {
                        googlePayment(""+dataTelcos.get_paket_code());
                    }
                    else
                    {
                        Intent intent = new Intent(c,PaketWebview.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("title","" + ""+dataTelcos.get_telco());
                        intent.putExtra("url","" + ""+dataTelcos.get_url());
                        c.startActivity(intent);
                    }

                }
            });

        }

        else if (holder instanceof PaketDetailAdapterGaJadiDipake.LoadingViewHolder) {
            PaketDetailAdapterGaJadiDipake.LoadingViewHolder loadingViewHolder = (PaketDetailAdapterGaJadiDipake.LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return myArray == null ? 0 : myArray.size();
    }

    public void setLoaded() {
        isLoading = false;
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        public FontAppCompatTextView t_judul;
        public LinearLayout parent;

        public UserViewHolder(View view) {
            super(view);
            c = itemView.getContext();

            parent = view.findViewById(R.id.parent);
            t_judul = view.findViewById(R.id.nama_paket);
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }


    public void googlePayment(String paket_code)
    {

    }

}
