package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetVideo {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private List<DataVideo> listVideo = null;


    public ResponseGetVideo(String code, List<DataVideo> listVideo){
        this.code = code;
        this.listVideo = listVideo;
    }

    public ResponseGetVideo() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public List<DataVideo> getListVideo() {
        return listVideo;
    }

    public void setListVideo(List<DataVideo> listVideo) {
        this.listVideo = listVideo;
    }
}
