package gtunes.application.gtunes.model;

public class VideosClip {

    public int avatar,imgVideos;
    public String textJudul;

    public VideosClip(int avatar, int imgVideos, String textJudul) {
        this.avatar = avatar;
        this.imgVideos = imgVideos;
        this.textJudul = textJudul;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public int getImgVideos() {
        return imgVideos;
    }

    public void setImgVideos(int imgVideos) {
        this.imgVideos = imgVideos;
    }

    public String getTextJudul() {
        return textJudul;
    }

    public void setTextJudul(String textJudul) {
        this.textJudul = textJudul;
    }

    @Override
    public String toString() {
        return "VideosClip{" +
                "avatar=" + avatar +
                ", imgVideos=" + imgVideos +
                ", textJudul='" + textJudul + '\'' +
                '}';
    }
}
