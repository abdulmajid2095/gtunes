package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetArtistDetail2 {

    @SerializedName("songs")
    @Expose
    private List<DataLagu> listSong = null;


    public ResponseGetArtistDetail2(List<DataLagu> listSong){
        this.listSong = listSong;
    }

    public ResponseGetArtistDetail2() {

    }

    public List<DataLagu> getListSong() {
        return listSong;
    }

    public void setListSong(List<DataLagu> listSong) {
        this.listSong = listSong;
    }

}
