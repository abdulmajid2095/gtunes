package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendEditProfile {
    @SerializedName("customer_email")
    @Expose
    private String email;

    @SerializedName("customer_name")
    @Expose
    private String name;

    @SerializedName("customer_phone")
    @Expose
    private String phone;


    public SendEditProfile(String email, String name, String phone){
        this.email = email;
        this.name = name;
        this.phone = phone;
    }

    public SendEditProfile() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
