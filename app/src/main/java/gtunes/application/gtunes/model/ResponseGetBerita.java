package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetBerita {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private List<DataBerita> listNews = null;


    public ResponseGetBerita(String code, List<DataBerita> listNews){
        this.code = code;
        this.listNews = listNews;
    }

    public ResponseGetBerita() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public List<DataBerita> getListNews() {
        return listNews;
    }

    public void setListNews(List<DataBerita> listNews) {
        this.listNews = listNews;
    }
}
