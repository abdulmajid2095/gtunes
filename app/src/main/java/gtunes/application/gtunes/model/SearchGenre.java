package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchGenre {
    @SerializedName("judul")
    @Expose
    private String judul;

    public SearchGenre(String judul){
        this.judul = judul;
    }

    public SearchGenre() {

    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

}
