package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;

//It is needed when request to server using retrofit library, to carry some data needed

public class DataBerita {
    @SerializedName("whats_new_id")
    private String whats_new_id;

    @SerializedName("whats_new_title")
    private String whats_new_title;

    @SerializedName("whats_new_short_desc")
    private String whats_new_short_desc;

    @SerializedName("whats_new_desc")
    private String whats_new_desc;

    @SerializedName("whats_new_image")
    private String whats_new_image;

    @SerializedName("whats_new_active_status")
    private String whats_new_active_status;

    @SerializedName("whats_new_meta_description")
    private String whats_new_meta_description;

    @SerializedName("whats_new_meta_keywords")
    private String whats_new_meta_keywords;

    @SerializedName("whats_new_hits")
    private String whats_new_hits;

    @SerializedName("whats_new_ticket")
    private String whats_new_ticket;

    @SerializedName("whats_new_create_date")
    private String whats_new_create_date;

    @SerializedName("whats_new_create_by")
    private String whats_new_create_by;

    @SerializedName("whats_new_update_date")
    private String whats_new_update_date;

    @SerializedName("whats_new_update_by")
    private String whats_new_update_by;

    @SerializedName("dateHuman")
    private String dateHuman;

    public DataBerita(String whats_new_id, String whats_new_title, String whats_new_short_desc, String whats_new_desc, String whats_new_image,
                      String whats_new_active_status, String whats_new_meta_description, String whats_new_meta_keywords, String whats_new_hits, String whats_new_ticket,
                      String whats_new_create_date, String whats_new_create_by, String whats_new_update_date, String whats_new_update_by, String dateHuman) {

        this.whats_new_id = whats_new_id;
        this.whats_new_title = whats_new_title;
        this.whats_new_short_desc = whats_new_short_desc;
        this.whats_new_desc = whats_new_desc;
        this.whats_new_image = whats_new_image;
        this.whats_new_active_status = whats_new_active_status;
        this.whats_new_meta_description = whats_new_meta_description;
        this.whats_new_meta_keywords = whats_new_meta_keywords;
        this.whats_new_hits = whats_new_hits;
        this.whats_new_ticket = whats_new_ticket;
        this.whats_new_create_date = whats_new_create_date;
        this.whats_new_create_by = whats_new_create_by;
        this.whats_new_update_date = whats_new_update_date;
        this.whats_new_update_by = whats_new_update_by;
        this.dateHuman = dateHuman;
    }

    public String get_whats_new_id() {
        return whats_new_id;
    }

    public String get_whats_new_title() {
        return whats_new_title;
    }

    public String get_whats_new_short_desc() {
        return whats_new_short_desc;
    }

    public String get_whats_new_desc() {
        return whats_new_desc;
    }

    public String get_whats_new_image() {
        return whats_new_image;
    }

    public String get_whats_new_active_status() {
        return whats_new_active_status;
    }

    public String get_whats_new_meta_description() {
        return whats_new_meta_description;
    }

    public String get_whats_new_meta_keywords() {
        return whats_new_meta_keywords;
    }

    public String get_whats_new_hits() {
        return whats_new_hits;
    }

    public String get_whats_new_ticket() {
        return whats_new_ticket;
    }

    public String get_whats_new_create_date() {
        return whats_new_create_date;
    }

    public String get_whats_new_create_by() {
        return whats_new_create_by;
    }

    public String get_whats_new_update_date() {
        return whats_new_update_date;
    }

    public String get_whats_new_update_by() {
        return whats_new_update_by;
    }

    public String get_dateHuman() {
        return dateHuman;
    }
}
