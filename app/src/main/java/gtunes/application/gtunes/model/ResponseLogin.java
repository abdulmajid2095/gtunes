package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseLogin {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private DataData dataData = null;

    @SerializedName("message")
    @Expose
    private String message;


    public ResponseLogin(String code, DataData dataData, String message){
        this.code = code;
        this.dataData = dataData;
        this.message = message;
    }

    public ResponseLogin() {

    }

    public String get_code() {
        return code;
    }

    public DataData get_dataData() {
        return dataData;
    }

    public String get_message() {
        return message;
    }
}
