package gtunes.application.gtunes.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetLaguGenre2 {

    @SerializedName("songs")
    @Expose
    private List<DataLagu> listSong = null;


    public ResponseGetLaguGenre2(List<DataLagu> listSong){
        this.listSong = listSong;
    }

    public ResponseGetLaguGenre2() {

    }

    public List<DataLagu> getListSong() {
        return listSong;
    }

    public void setListSong(List<DataLagu> listSong) {
        this.listSong = listSong;
    }

}
