package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPaket {
    @SerializedName("paket_id")
    private String paket_id;

    @SerializedName("paket_code")
    private String paket_code;

    @SerializedName("paket_tarif")
    private String paket_tarif;

    @SerializedName("paket_hari")
    private String paket_hari;

    public DataPaket(String paket_id, String paket_code, String paket_tarif, String paket_hari) {

        this.paket_id = paket_id;
        this.paket_code = paket_code;
        this.paket_tarif = paket_tarif;
        this.paket_hari = paket_hari;
    }

    public String get_paket_id() {
        return paket_id;
    }

    public String get_paket_code() {
        return paket_code;
    }

    public String get_paket_tarif() {
        return paket_tarif;
    }

    public String get_paket_hari() {
        return paket_hari;
    }

}
