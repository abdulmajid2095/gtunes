package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseGetVersionCode {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private DataVersionCode dataVersionCode = null;

    public ResponseGetVersionCode(String code, DataVersionCode dataVersionCode){
        this.code = code;
        this.dataVersionCode = dataVersionCode;
    }

    public ResponseGetVersionCode() {

    }

    public String get_code() {
        return code;
    }

    public DataVersionCode get_dataVersionCode() {
        return dataVersionCode;
    }
}
