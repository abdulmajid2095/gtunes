package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseUploadImage {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private DataData dataData = null;

    @SerializedName("message")
    @Expose
    private String message;


    public ResponseUploadImage(String code, DataData dataData, String message){
        this.code = code;
        this.dataData = dataData;
        this.message = message;
    }

    public ResponseUploadImage() {

    }

    public String get_code() {
        return code;
    }

    public DataData get_dataData() {
        return dataData;
    }

    public String get_message() {
        return message;
    }
}
