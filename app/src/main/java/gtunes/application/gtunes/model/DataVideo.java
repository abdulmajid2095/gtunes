package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;

public class DataVideo {
    @SerializedName("video_clip_id")
    private String video_clip_id;

    @SerializedName("video_clip_title")
    private String video_clip_title;

    @SerializedName("video_clip_image")
    private String video_clip_image;

    @SerializedName("video_clip_url")
    private String video_clip_url;

    @SerializedName("video_clip_desc")
    private String video_clip_desc;


    public DataVideo(String video_clip_id, String video_clip_title, String video_clip_image, String video_clip_url, String video_clip_desc) {

        this.video_clip_id = video_clip_id;
        this.video_clip_title = video_clip_title;
        this.video_clip_image = video_clip_image;
        this.video_clip_url = video_clip_url;
        this.video_clip_desc = video_clip_desc;
    }

    public String get_video_clip_id() {
        return video_clip_id;
    }

    public String get_video_clip_title() {
        return video_clip_title;
    }

    public String get_video_clip_image() {
        return video_clip_image;
    }

    public String get_video_clip_url() {
        return video_clip_url;
    }

    public String get_video_clip_desc() {
        return video_clip_desc;
    }

}
