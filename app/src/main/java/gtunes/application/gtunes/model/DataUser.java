package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;
/*

public class DataUser {
    @SerializedName("customer_id")
    private String customer_id;

    @SerializedName("customer_name")
    private String customer_name;

    @SerializedName("customer_username")
    private String customer_username;

    @SerializedName("customer_email")
    private String customer_email;

    @SerializedName("customer_phone")
    private String customer_phone;

    @SerializedName("customer_avatar")
    private String customer_avatar;

    @SerializedName("customer_code")
    private String customer_code;

    @SerializedName("customer_balance")
    private String customer_balance;

    @SerializedName("customer_paket")
    private String customer_paket;

    @SerializedName("customer_paket_name")
    private String customer_paket_name;

    @SerializedName("customer_paket_status")
    private String customer_paket_status;

    @SerializedName("paket")
    private String customer_paket_day;

    public DataUser(String customer_id, String customer_name, String customer_username, String customer_email, String customer_phone,
                    String customer_avatar, String customer_code, String customer_balance, String customer_paket, String customer_paket_name,
                    String customer_paket_status, String customer_paket_day) {

        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_username = customer_username;
        this.customer_email = customer_email;
        this.customer_phone = customer_phone;
        this.customer_avatar = customer_avatar;
        this.customer_code = customer_code;
        this.customer_balance = customer_balance;
        this.customer_paket = customer_paket;
        this.customer_paket_name = customer_paket_name;
        this.customer_paket_status = customer_paket_status;
        this.customer_paket_day = customer_paket_day;
    }

    public String get_customer_id() {
        return customer_id;
    }

    public String get_customer_name() {
        return customer_name;
    }

    public String get_customer_username() {
        return customer_username;
    }

    public String get_customer_email() {
        return customer_email;
    }

    public String get_customer_phone() {
        return customer_phone;
    }

    public String get_customer_avatar() {
        return customer_avatar;
    }

    public String get_customer_code() {
        return customer_code;
    }

    public String get_customer_balance() {
        return customer_balance;
    }

    public String get_customer_paket() {
        return customer_paket;
    }

    public String get_customer_paket_name() {
        return customer_paket_name;
    }

    public String get_customer_paket_status() {
        return customer_paket_status;
    }

    public String get_customer_paket_day() {
        return customer_paket_day;
    }


}
*/


public class DataUser {
    @SerializedName("user_id")
    private String user_id;

    @SerializedName("phone")
    private String phone;

    @SerializedName("telco")
    private String telco;

    @SerializedName("email")
    private String email;

    @SerializedName("active_until")
    private String active_until;

    @SerializedName("paket_status")
    private String paket_status;


    public DataUser(String user_id, String phone, String telco, String email, String active_until,
                    String paket_status) {

        this.user_id = user_id;
        this.phone = phone;
        this.telco = telco;
        this.email = email;
        this.active_until = active_until;
        this.paket_status = paket_status;
    }

    public String get_user_id() {
        return user_id;
    }

    public String get_phone() {
        return phone;
    }

    public String get_telco() {
        return telco;
    }

    public String get_email() {
        return email;
    }

    public String get_active_until() {
        return active_until;
    }

    public String get_paket_status() {
        return paket_status;
    }
}
