package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetGenre {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private List<DataGenre> listGenre = null;


    public ResponseGetGenre(String code, List<DataGenre> listGenre){
        this.code = code;
        this.listGenre = listGenre;
    }

    public ResponseGetGenre() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public List<DataGenre> getListGenre() {
        return listGenre;
    }

    public void setListGenre(List<DataGenre> listGenre) {
        this.listGenre = listGenre;
    }
}
