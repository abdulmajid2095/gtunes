package gtunes.application.gtunes.model;


import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/*
public class DataData {
    @SerializedName("token")
    private String token;

    @SerializedName("user")
    private DataUser dataUser;

    @SerializedName("customer_avatar")
    private String customer_avatar;

    public DataData(String token, DataUser dataUser, String customer_avatar) {

        this.token = token;
        this.dataUser = dataUser;
        this.customer_avatar = customer_avatar;
    }

    public String get_token() {
        return token;
    }

    public DataUser get_dataUser() {
        return dataUser;
    }

    public String get_customer_avatar() {
        return customer_avatar;
    }

}
*/

public class DataData {
    @SerializedName("token")
    private String token;

    @SerializedName("user")
    private DataUser dataUser;

    public DataData(String token, DataUser dataUser) {

        this.token = token;
        this.dataUser = dataUser;
    }

    public String get_token() {
        return token;
    }

    public DataUser get_dataUser() {
        return dataUser;
    }

}
