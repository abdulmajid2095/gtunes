package gtunes.application.gtunes.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseGetLaguGenre {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private ResponseGetLaguGenre2 data;


    public ResponseGetLaguGenre(String code, ResponseGetLaguGenre2 data){
        this.code = code;
        this.data = data;
    }

    public ResponseGetLaguGenre() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public ResponseGetLaguGenre2 get_data() {
        return data;
    }

    public void set_data(ResponseGetLaguGenre2 data) {
        this.data = data;
    }

}
