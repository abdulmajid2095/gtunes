package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;

public class DataTelcos {
    @SerializedName("telco")
    private String telco;

    @SerializedName("url")
    private String url;

    @SerializedName("type")
    private String type;

    @SerializedName("paket_code")
    private String paket_code;


    public DataTelcos(String telco, String url, String type, String paket_code) {

        this.telco = telco;
        this.url = url;
        this.type = type;
        this.paket_code = paket_code;
    }

    public String get_telco() {
        return telco;
    }

    public String get_url() {
        return url;
    }

    public String get_type() {
        return type;
    }

    public String get_paket_code() {
        return paket_code;
    }


}
