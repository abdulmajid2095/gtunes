package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;

public class DataVersionCode {
    @SerializedName("version_code")
    private String version_code;

    public DataVersionCode(String version_code) {

        this.version_code = version_code;
    }

    public String get_version_code() {
        return version_code;
    }
}
