package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetArtistDetail {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private ResponseGetArtistDetail2 data;


    public ResponseGetArtistDetail(String code, ResponseGetArtistDetail2 data){
        this.code = code;
        this.data = data;
    }

    public ResponseGetArtistDetail() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public ResponseGetArtistDetail2 getData() {
        return data;
    }

    public void setData(ResponseGetArtistDetail2 listGenre) {
        this.data = data;
    }
}
