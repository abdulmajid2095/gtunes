package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetPaket {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private List<DataPaket> dataPaket = null;


    public ResponseGetPaket(String code, List<DataPaket> dataPaket){
        this.code = code;
        this.dataPaket = dataPaket;
    }

    public ResponseGetPaket() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public List<DataPaket> getDataPaket() {
        return dataPaket;
    }

}
