package gtunes.application.gtunes.model;

import com.google.gson.annotations.SerializedName;

public class GoogleBilling {

    @SerializedName("product_id")
    private String product_id;

    @SerializedName("purchase_token")
    private String purchase_token;

    @SerializedName("order_id")
    private String order_id;

    public GoogleBilling(String product_id, String purchase_token, String order_id) {
        this.product_id = product_id;
        this.purchase_token = purchase_token;
        this.order_id = order_id;
    }

    public String getproduct_id() {
        return product_id;
    }

    public void setproduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String setpurchase_token() {
        return purchase_token;
    }

    public void getpurchase_token(String purchase_token) {
        this.purchase_token = purchase_token;
    }

    public String setorder_id() {
        return order_id;
    }

    public void getorder_id(String order_id) {
        this.order_id = order_id;
    }

}
