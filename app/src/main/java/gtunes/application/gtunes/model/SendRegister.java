package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/*

public class SendRegister {
    @SerializedName("customer_email")
    @Expose
    private String email;

    @SerializedName("customer_name")
    @Expose
    private String name;

    @SerializedName("customer_password")
    @Expose
    private String password;

    @SerializedName("customer_phone")
    @Expose
    private String phone;

    @SerializedName("customer_username")
    @Expose
    private String username;


    public SendRegister(String email, String name, String password, String phone, String username){
        this.email = email;
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.username = username;
    }

    public SendRegister() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
*/


public class SendRegister {
    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("phone")
    @Expose
    private String phone;

    public SendRegister(String user_id, String password, String name, String phone){
        this.user_id = user_id;
        this.password = password;
        this.name = name;
        this.phone = phone;
    }

    public SendRegister() {

    }

    public String getuser_id() {
        return user_id;
    }

    public void setuser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getpassword() {
        return password;
    }

    public void setpassword(String password) {
        this.password = password;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
