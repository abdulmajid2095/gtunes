package gtunes.application.gtunes.model;

public class Paket {

    public String paket;

    public Paket(String paket) {
        this.paket = paket;
    }

    public String getPaket() {
        return paket;
    }

    public void setPaket(String paket) {
        this.paket = paket;
    }

    @Override
    public String toString() {
        return "Paket{" +
                "paket='" + paket + '\'' +
                '}';
    }
}
