package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;

public class DataLagu {

    @SerializedName("music_id")
    private String music_id;

    @SerializedName("music_title")
    private String music_title;

    @SerializedName("music_artist")
    private String music_artist;

    @SerializedName("music_url")
    private String music_url;

    @SerializedName("music_artwork")
    private String music_artwork;

    @SerializedName("artist_name")
    private String artist_name;

    @SerializedName("artist_id")
    private String artist_id;

    @SerializedName("music_singsong_url")
    private String music_singsong_url;

    @SerializedName("music_lyric")
    private String music_lyric;

    @SerializedName("music_free")
    private String music_free;

    @SerializedName("music_karaoke_available")
    private String music_karaoke_available;

    @SerializedName("tryme")
    private String tryme;


    public DataLagu(String music_id, String music_title, String music_artist, String music_url, String music_artwork, String artist_name, String artist_id,
                    String music_singsong_url, String music_lyric, String music_free, String music_karaoke_available, String tryme) {

        this.music_id = music_id;
        this.music_title = music_title;
        this.music_artist = music_artist;
        this.music_url = music_url;
        this.music_artwork = music_artwork;
        this.artist_name = artist_name;
        this.artist_id = artist_id;
        this.music_singsong_url = music_singsong_url;
        this.music_lyric = music_lyric;
        this.music_free = music_free;
        this.music_karaoke_available = music_karaoke_available;
        this.tryme = tryme;
    }

    public String get_music_id() {
        return music_id;
    }

    public String get_music_title() {
        return music_title;
    }

    public String get_music_artist() {
        return music_artist;
    }

    public String get_music_url() {
        return music_url;
    }

    public String get_music_artwork() {
        return music_artwork;
    }

    public String get_artist_name() {
        return artist_name;
    }

    public String get_artist_id() {
        return artist_id;
    }

    public String get_music_singsong_url() {
        return music_singsong_url;
    }

    public String get_music_lyric() {
        return music_lyric;
    }

    public String get_music_free() {
        return music_free;
    }

    public String get_music_karaoke_available() {
        return music_karaoke_available;
    }

    public String get_tryme() {
        return tryme;
    }

}
