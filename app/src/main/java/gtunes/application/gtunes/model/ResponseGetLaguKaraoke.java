package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetLaguKaraoke {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private List<DataLagu> data;


    public ResponseGetLaguKaraoke(String code, List<DataLagu> data){
        this.code = code;
        this.data = data;
    }

    public ResponseGetLaguKaraoke() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public List<DataLagu> get_data() {
        return data;
    }

    public void set_data(List<DataLagu> data) {
        this.data = data;
    }

}
