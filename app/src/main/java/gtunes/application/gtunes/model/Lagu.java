package gtunes.application.gtunes.model;

public class Lagu {
    public String judulLagu,artsLagu,time;

    public Lagu(String judulLagu, String artsLagu, String time) {
        this.judulLagu = judulLagu;
        this.artsLagu = artsLagu;
        this.time = time;
    }

    public String getJudulLagu() {
        return judulLagu;
    }

    public void setJudulLagu(String judulLagu) {
        this.judulLagu = judulLagu;
    }

    public String getArtsLagu() {
        return artsLagu;
    }

    public void setArtsLagu(String artsLagu) {
        this.artsLagu = artsLagu;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Lagu{" +
                "judulLagu='" + judulLagu + '\'' +
                ", artsLagu='" + artsLagu + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
