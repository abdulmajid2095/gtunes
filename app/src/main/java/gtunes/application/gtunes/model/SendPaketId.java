package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendPaketId {
    @SerializedName("paket_id")
    @Expose
    private String paket_id;



    public SendPaketId(String paket_id){
        this.paket_id = paket_id;
    }

    public SendPaketId() {

    }

    public String getPaket_id() {
        return paket_id;
    }

    public void setPaket_id(String customer_phone) {
        this.paket_id = paket_id;
    }

}
