package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;

//It is needed when request to server using retrofit library, to carry some data needed

public class DataArtis {
    @SerializedName("artist_id")
    private String artist_id;

    @SerializedName("artist_name")
    private String artist_name;

    @SerializedName("artist_image")
    private String artist_image;


    public DataArtis(String artist_id, String artist_name, String artist_image) {

        this.artist_id = artist_id;
        this.artist_name = artist_name;
        this.artist_image = artist_image;
    }

    public String get_artist_id() {
        return artist_id;
    }

    public String get_artist_name() {
        return artist_name;
    }

    public String get_artist_image() {
        return artist_image;
    }

}
