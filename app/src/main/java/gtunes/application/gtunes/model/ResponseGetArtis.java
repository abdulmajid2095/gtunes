package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetArtis {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private List<DataArtis> listArtis = null;


    public ResponseGetArtis(String code, List<DataArtis> listArtis){
        this.code = code;
        this.listArtis = listArtis;
    }

    public ResponseGetArtis() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public List<DataArtis> getListArtis() {
        return listArtis;
    }

    public void setListArtis(List<DataArtis> listArtis) {
        this.listArtis = listArtis;
    }
}
