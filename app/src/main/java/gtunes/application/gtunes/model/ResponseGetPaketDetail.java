package gtunes.application.gtunes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetPaketDetail {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("data")
    @Expose
    private List<DataTelcos> dataTelcos = null;


    public ResponseGetPaketDetail(String code, List<DataTelcos> dataTelcos){
        this.code = code;
        this.dataTelcos = dataTelcos;
    }

    public ResponseGetPaketDetail() {

    }

    public String get_code() {
        return code;
    }

    public void set_code(String code) {
        this.code = code;
    }

    public List<DataTelcos> getDataPaketDetail() {
        return dataTelcos;
    }

}
