package gtunes.application.gtunes.model;


import com.google.gson.annotations.SerializedName;

public class DataGenre {
    @SerializedName("genre_id")
    private String genre_id;

    @SerializedName("genre_name")
    private String genre_name;

    @SerializedName("genre_image")
    private String genre_image;


    public DataGenre(String genre_id, String genre_name, String genre_image) {

        this.genre_id = genre_id;
        this.genre_name = genre_name;
        this.genre_image = genre_image;
    }

    public String get_genre_id() {
        return genre_id;
    }

    public String get_genre_name() {
        return genre_name;
    }

    public String get_genre_image() {
        return genre_image;
    }

}
