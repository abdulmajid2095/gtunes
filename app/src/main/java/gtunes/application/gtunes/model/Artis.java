package gtunes.application.gtunes.model;

//UNUSED
public class Artis {

    public String namaArtis;
    public int imgArtis;

    public Artis(String namaArtis, int imgArtis) {
        this.namaArtis = namaArtis;
        this.imgArtis = imgArtis;
    }

    public String getNamaArtis() {
        return namaArtis;
    }

    public void setNamaArtis(String namaArtis) {
        this.namaArtis = namaArtis;
    }

    public int getImgArtis() {
        return imgArtis;
    }

    public void setImgArtis(int imgArtis) {
        this.imgArtis = imgArtis;
    }

    @Override
    public String toString() {
        return "Artis{" +
                "namaArtis='" + namaArtis + '\'' +
                ", imgArtis=" + imgArtis +
                '}';
    }
}
