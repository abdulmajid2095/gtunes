package gtunes.application.gtunes.model;

public class Genre {
    public String genre;
    public int imageGenre;

    public Genre(String genre, int imageGenre) {
        this.genre = genre;
        this.imageGenre = imageGenre;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getImageGenre() {
        return imageGenre;
    }

    public void setImageGenre(int imageGenre) {
        this.imageGenre = imageGenre;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "genre='" + genre + '\'' +
                ", imageGenre=" + imageGenre +
                '}';
    }
}
