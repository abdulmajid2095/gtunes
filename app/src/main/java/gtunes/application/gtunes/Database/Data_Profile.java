package gtunes.application.gtunes.Database;
/*

import com.orm.SugarRecord;

import java.util.Scanner;

public class Data_Profile extends SugarRecord{

     public String token, customer_id, customer_name, customer_email, customer_phone,
                    customer_avatar, customer_code, customer_paket, customer_paket_name,
                    customer_paket_status, customer_paket_day;
    public boolean login;

    public Data_Profile() {

    }

    public Data_Profile(String token, String customer_id, String customer_name, String customer_email, String customer_phone,
                    String customer_avatar, String customer_code, String customer_paket, String customer_paket_name,
                    String customer_paket_status, String customer_paket_day, Boolean login) {

        this.token = token;
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_email = customer_email;
        this.customer_phone = customer_phone;
        this.customer_avatar = customer_avatar;
        this.customer_code = customer_code;
        this.customer_paket = customer_paket;
        this.customer_paket_name = customer_paket_name;
        this.customer_paket_status = customer_paket_status;
        this.customer_paket_day = customer_paket_day;
        this.login = login;
    }
}*/


import com.orm.SugarRecord;

import java.util.Scanner;

//This file is for configuring local database using Sugar Library
public class Data_Profile extends SugarRecord{

    //Initializing data type needed
    public String token, user_id, phone, telco, email,
            active_until, paket_status, nama, avatar, loginUser, loginPassword;
    public boolean login;

    public Data_Profile() {

    }

    public Data_Profile(String token, String user_id, String phone, String telco, String email,
                        String active_until, String paket_status, boolean login, String nama, String avatar,
                        String loginUser, String loginPassword) {

        this.token = token;
        this.user_id = user_id;
        this.phone = phone;
        this.telco = telco;
        this.email = email;
        this.active_until = active_until;
        this.paket_status = paket_status;
        this.login = login;
        this.nama = nama;
        this.avatar = avatar;
        this.loginUser = loginUser;
        this.loginPassword = loginPassword;
    }
}